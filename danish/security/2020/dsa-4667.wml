#use wml::debian::translation-check translation="ae06255bde57d831150a61d8cba709fe69cdbd83" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Flere sårbarheder er opdaget i Linux-kernen, hvilke kunne føre til en 
rettighedsforøgelse, lammelsesangreb eller informationslækage.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-2732">CVE-2020-2732</a>

    <p>Paulo Bonzini opdagede at KVM-implementateringen for Intel-processorer 
    ikke på korrekt vis håndterede instruktionsemulering ved L2-gæster med 
    aktiveret indlejret virtualisering.  Dermed kunne det være muligt for en 
    L2-gæst at forårsage rettighedsforøgelse, lammelsesangreb eller 
    informationslækage i L1-gæsten.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8428">CVE-2020-8428</a>

    <p>Al Viro opdagede en sårbarhed i forbindelse med anvendelse efter 
    frigivelse i VFS-laget.  Dermed kunne lokale brugere forårsage et 
    lammelsesangreb (nedbrud) eller få adgang til følsomme oplysninger fra 
    kernehukommelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10942">CVE-2020-10942</a>

    <p>Man opdagede at driveren vhost_net validerede ikke på korrekt vis 
    sockettyper opsat som backend'er.  En lokal bruger med rettigheder til at 
    tilgå /dev/vhost-net kunne anvende dette til at forårsage stakkorruption 
    gennem fabrikerede systemkald, medførende lammelsesangreb (nedbrud) eller 
    muligvis rettighedsforøgelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11565">CVE-2020-11565</a>

    <p>Entropy Moe rapporterede at del hukommelse-filsystemet (tmpfs) håndterede 
    ikke på korrekt vis en <q>mpol</q>-mountvalgmulighed med angivelse af en tom 
    nodeliste, førende til en stakbaseret skrivning udenfor grænserne.  Hvis 
    brugernavnerum er aktiveret, kunne en lokal bruger udnytte dette til at 
    forårsage et lammelsesangreb (nedbrud) eller muligvis 
    rettighedsforøgelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11884">CVE-2020-11884</a>

    <p>Al Viro rapporterede om en kapløbstilstand i hukommelseshåndteringen for 
    IBM Z (s390x-arkitekturen), hvilket kunne medføre at kernen udførte kode fra 
    et brugeradresserum.  En lokal bruger kunne udnytte dette til 
    rettighedsforøgelse.</p></li>

</ul>

<p>I den stabile distribution (buster), er disse problemer rettet i
version 4.19.98-1+deb10u1.</p>

<p>Vi anbefaler at du opgraderer dine linux-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende linux, se
dens sikkerhedssporingssidede på:
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4667.data"
