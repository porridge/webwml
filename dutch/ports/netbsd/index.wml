#use wml::debian::template title="Debian GNU/NetBSD" BARETITLE="yes" NOHEADER="yes"
#use wml::fmt::verbatim
#include "$(ENGLISHDIR)/ports/netbsd/menu.inc"
#use wml::debian::translation-check translation="6a11f166d66c3d21d75c0b38c728ed3d3dd75ba8"
{#style#:
<style type="text/css">
    .update {
	color:		red;
	background:	inherit;
	font-weight:	bold;
    }
</style>
:##}

<define-tag update endtag=required>
  <p><span class="update">UPDATE: </span>
    %body
  </p>
</define-tag>


#############################################################################
<div class="important">
<p><strong>
Deze overdraagbaarheidsinspanning is al lang opgegeven. De informatie op deze
pagina is er enkel voor historische redenen.
</strong></p>
</div>


<h1>
Debian GNU/NetBSD
</h1>


<p>
Debian GNU/NetBSD is een streven om het Debian besturingssysteem geschikt
te maken voor de NetBSD-kernel en libc (niet te verwarren met de andere
overzettingen van Debian naar BSD, gebaseerd op glibc). De overzetting
bevindt zich momenteel in een vroege ontwikkelingsfase - niettemin kan
het systeem nu vanaf nul geïnstalleerd worden.
</p>

# dode link <p>
#<a href="http://www.srcf.ucam.org/debian-netbsd/floppies">\
#Experimentele installatiediskettes downloaden</a> (laatst
#bijgewerkt 6 oktober 2002)
#</p>

<p>
<a href="why">Waarom Debian GNU/NetBSD?</a>
</p>

<h2>
Hoe installeren
</h2>

<p>
Download de diskette-images vanaf bovenstaande link. Gebruik voor laptops
de laptop-images - gebruik voor alle andere machines de gewone images. Schrijf
deze images op diskettes. Start op vanaf de eerste diskette - er zal u gevraagd
worden om diskettes te wisselen. Volg de instructies die u worden gegeven
nadat het menusysteem verschijnt.
</p>

<h2>
Te doen
</h2>

<p>
Pakketten die geproduceerd moeten worden
</p>

<ul>
<li>
Voor alle libs in <kbd>/lib</kbd> en <kbd>/usr/lib</kbd> die momenteel niet
verpakt worden, moet dit nog gebeuren
</li>
<li>
  base-passwd is wanhopig ongelukkig
  <update>
    We hebben nu een in principe werkend base-passwd voor FreeBSD en
    NetBSD (rest nog een segmentatiefout). Met dank aan Nathan en
    Matthew.
  </update>
</li>
<li>er moeten equivalenten geproduceerd worden van console-tools/data
    <update>
      Er werden pakketten gemaakt die de basisfunctionaliteit leveren
    </update>
</li>
<li>
netbase moet opnieuw gebouwd worden. Dit is waarschijnlijk een van de
moeilijkere taken - we hebben broncode voor de BSD-versies van ifconfig e.a.,
maar de semantiek is iets anders. Als we bij de BSD-semantiek blijven, moeten we
alle scripts die een semantiek in Linux-stijl gebruiken, aanpakken. Volgt
de Hurd de semantiek in Linux-stijl en indien niet, hoe hebben zij het dan
aangepakt?
  <update>
     Marcus Brinkmann van het Hurd-team
     <a href="https://lists.debian.org/debian-bsd/2002/debian-bsd-200202/msg00087.html">\
     verduidelijkte</a> dit een beetje en schetste mogelijke oplossingen.
     De huidige aanpak is de NetBSD-hulpmiddelen te gebruiken en ifupdown
     aan te passen om de gebruiker eenzelfde interface te bieden.
  </update>
</li>
<li>procps (waarschijnlijk is het best om gewoon de BSD-versies te leveren)</li>
# dode link <li>sysklogd
#  (we kunnen in plaats daarvan waarschijnlijk <a
#  href="https://packages.debian.org/msyslog">msyslog</a> gebruiken)
#
#  <update>
#     <a href="https://packages.debian.org/msyslog">msyslog</a> werkt op
#     NetBSD (resten nog enkele minpuntjes in verband met bestandspaden)
#  </update>
#</li>
<li>sysvinit
  (BSD init ondersteunt geen runlevels. We kunnen het zonder te veel moeite
  herwerken om het te doen werken zoals Debian met één enkel runlevel)
  <update>
    sysvinit werkt, Matthew is erin geslaagd om op i386 Debian GNU/NetBSD
    op te starten! Er zijn nog wel wat problemen met opstartscripts,
    maar dit is een belangrijke stap naar een volledig werkend systeem.
  </update>
</li>
<li><a href="https://packages.debian.org/fakeroot">fakeroot</a>
  <update>
    Fakeroot werkt nu.
  </update>
</li>
<li>XFree86
    (Nathan is hier momenteel mee bezig, en ontdekte dat
    <a href="https://packages.debian.org/ed">ed</a> nodig is, wat een
    segmentatiefout geeft. Verschillende personen onderzoeken deze zaak.)

    <update>
        ed werkt als het gebouwd wordt met libed.a. Ook valt Joel te citeren:
	<q>X11 is in een bruikbare staat</q>! Het is niet behoorlijk verpakt,
	maar het werkt. Pakketten mogen binnenkort verwacht worden.
    </update>
</li>
<li>gcc-3.0
    (Noch gcc-3.0.1 noch gcc-current zijn momenteel in een bruikbare
    toestand voor BSD. Joel heeft een werkende versie van gcc-current
    en poste de <a
    href="https://lists.debian.org/debian-bsd/2002/debian-bsd-200202/msg00137.html">resultaten
    van de testsuite</a>.  libstdc++ is nog steeds erg ongelukkig.)

    <update>
        gcc-3.0.4 werd uitgebracht en <a
        href="http://gcc.gnu.org/gcc-3.0/features.html"> ondersteunt nu
        NetBSD ELF-systemen</a> (ten minste voor doelwit x86).
    </update>
</li>
# dode link <li>Hoe omgaan met architecturen?<br />
#    Er is momenteel een <a
#    href="https://lists.debian.org/debian-bsd/2002/debian-bsd-200202/msg00033.html">discussie</a>
#    gaande op de mailinglijst over Marcus Brinkmann's <a
#    href="http://master.debian.org/~brinkmd/arch-handling.txt">voorstel
#    om met architecturen om te gaan</a>.
#</li>
</ul>


<h2>
Bronnen
</h2>

<ul>
<li><a href="$(HOME)/">Debian</a></li>

<li><a href="http://www.netbsd.org/">NetBSD</a></li>

# dode link <li>
#<a href="http://www.srcf.ucam.org/debian-netbsd/">\
#Matthew's voor apt-get toegankelijk pakketarchief</a>
#</li>

# dode link<li>
#<a href="http://debian-bsd.lightbearer.com/">Joel's voor apt-get toegankelijk archief</a>
#met pakketten voor FreeBSD en NetBSD
#</li>

# dode link <li>
# <a href="ftp://trantor.utsl.org/pub/">Een op FreeBSD gebaseerde
# chroot-omgeving en enkele pakketten</a>
# </li>

# dode link <li>
#<a href="http://debian-bsd.lightbearer.com/debian-gnu-freebsd.tar.bz2">Nog
#een FreeBSD chroot-tarball</a> en <a
#href="http://debian-bsd.lightbearer.com/kernel-ext2.tar.gz">kernel
#met ext2-ondersteuning</A>, beide gebouwd door
#<a href="mailto:rmh@debian.org">Robert Millan</a>.
#</li>

# dode link <li>
# <a href="http://master.debian.org/~dexter/debian-freebsd/">Debian
# GNU/FreeBSD-pakketten</a> (zeer oud, gebaseerd op FreeBSD-3.3 en slink)
# </li>
</ul>

<p>
Er is een Debian GNU/*BSD mailinglijst. Stuur e-mail naar
<a href="mailto:debian-bsd-request@lists.debian.org?subject=subscribe">\
debian-bsd-request@lists.debian.org</a> met subscribe als onderwerp indien
u wilt intekenen. Archieven zijn te vinden op
<url "https://lists.debian.org/debian-bsd/" />.
</p>

<hr />
<p>
Om het Debian GNU/NetBSD-team te contacteren, moet u een e-mail zenden naar
<email "debian-bsd@lists.debian.org" />.
Commentaar, vragen of suggesties in verband met onze afdeling van de website
van Debian zijn ook welkom op dat adres.
</p>

## Local variables:
## sgml-default-doctype-name: "HTML"
## End:
