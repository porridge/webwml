# translation of webwml/english/po/doc.pot to Dutch
# Templates files for webwml modules
# Copyright (C) 2003,2004 Software in the Public Interest, Inc.
#
# Frans Pop <elendil@planet.nl>, 2008, 2010.
# Jeroen Schot <schot@a-eskwadraat.nl>, 2012.
# Frans Spiesschaert <Frans.Spiesschaert@yucom.be>, 2014, 2017, 2018, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: dutch/po/doc.nl.po\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2020-12-18 20:31+0100\n"
"Last-Translator: Frans Spiesschaert <Frans.Spiesschaert@yucom.be>\n"
"Language-Team: Debian Dutch l10n Team <debian-l10n-dutch@lists.debian.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 2.2.1\n"

#: ../../english/doc/books.data:35
msgid ""
"\n"
"  Debian 9 is the must-have handbook for learning Linux. Start on the\n"
"  beginners level and learn how to deploy the system with graphical\n"
"  interface and terminal.\n"
"  This book provides the basic knowledge to grow and become a 'junior'\n"
"  systems administrator. Start off with exploring the GNOME desktop\n"
"  interface and adjust it to your personal needs. Overcome your fear of\n"
"  using the Linux terminal and learn the most essential commands in\n"
"  administering Debian. Expand your knowledge of system services (systemd)\n"
"  and learn how to adapt them. Get more out of the software in Debian and\n"
"  outside of Debian. Manage your home-network with network-manager, etc.\n"
"  10 percent of the profits on this book will be donated to the Debian\n"
"  Project."
msgstr ""
"\n"
"  Debian 9 is een onmisbaar handboek om Linux onder de knie te krijgen.\n"
"  Begin bij het niveau voor beginners en leer hoe u het systeem kunt\n"
"  gebruiken met een grafische interface en aan de terminal.\n"
"  Dit boek biedt de basiskennis die u nodig heeft om door te kunnen\n"
"  groeien tot een beginnende systeembeheerder. Ga op weg met het\n"
"  verkennen van de GNOME desktopomgeving en pas deze aan volgens uw\n"
"  eigen behoeften. Overwin uw angst om de Linux terminal te gebruiken\n"
"  en leer de meest essentiële commando's voor het beheer van Debian.\n"
"  Verruim uw kennis over systeemdiensten (systemd)\n"
"  en leer hoe u ze kunt aanpassen. Haal meer uit de software\n"
"  in en buiten Debian. Beheer uw thuisnetwerk met network-manager, etc.\n"
"  10 percent van de opbrengst van dit boek wordt gedoneerd\n"
"  aan het Debian Project."

#: ../../english/doc/books.data:64 ../../english/doc/books.data:174
#: ../../english/doc/books.data:229
msgid ""
"Written by two Debian developers, this free book\n"
"  started as a translation of their French best-seller known as Cahier de\n"
"  l'admin Debian (published by Eyrolles). Accessible to all, this book\n"
"  teaches the essentials to anyone who wants to become an effective and\n"
"  independent Debian GNU/Linux administrator.\n"
"  It covers all the topics that a competent Linux administrator should\n"
"  master, from the installation and the update of the system, up to the\n"
"  creation of packages and the compilation of the kernel, but also\n"
"  monitoring, backup and migration, without forgetting advanced topics\n"
"  like SELinux setup to secure services, automated installations, or\n"
"  virtualization with Xen, KVM or LXC."
msgstr ""
"Dit vrij verkrijgbare boek, geschreven door twee Debian ontwikkelaars,\n"
"  was oorspronkelijk een vertaling van hun Franse bestseller, uitgebracht\n"
"  door Eyrolles onder de titel \"Cahier de l'admin Debian.\" Wie graag een\n"
"  efficiënte en onafhankelijke systeembeheerder van Debian Gnu/Linux\n"
"  computers wil worden kan via dit erg toegankelijke werk de nodige\n"
"  basiskennis verwerven.\n"
"  Het boek behandelt alle onderwerpen die een competente Linux\n"
"  systeembeheerder onder de knie moet hebben, vanaf het installeren en\n"
"  onderhouden van het systeem tot het maken van pakketten en het\n"
"  compileren van de kernel. Het besteedt voorts aandacht aan\n"
"  systeemopvolging, reservekopieën maken en de overstap wagen. Ook meer\n"
"  gevorderde aspecten komen aan bod, zoals SELinux instellen voor het\n"
"  beveiligen van diensten, automatische installaties en virtualisatie\n"
"  met behulp van Xen, KVM of LXC."

#: ../../english/doc/books.data:86
msgid ""
"The aim of this freely available book is to get you up to\n"
"  speed with Debian. It is comprehensive with basic support\n"
"  for the user who installs and maintains the system themselves (whether\n"
"  in the home, office, club, or school). Some Debian specific information "
"are very old.\n"
"  "
msgstr ""
"Het doel van dit vrij verkrijgbaar up-to-date boek is om op snelheid te\n"
"  komen met Debian. Het is zeer uitgebreid met basisondersteuning voor\n"
"  de gebruiker die het systeem zelf installeert en beheert (thuis, op\n"
"  kantoor, in een club of op school). Bepaalde\n"
"Debian-specifieke informatie is zeer oud."

#: ../../english/doc/books.data:108
msgid ""
"The first French book about Debian is already in its fifth edition. It\n"
"  covers all aspects of the administration of Debian from the installation\n"
"  to the configuration of network services.\n"
"  Written by two Debian developers, this book can be of interest to many\n"
"  people: the beginner wishing to discover Debian, the advanced user "
"looking\n"
"  for tips to enhance his mastership of the Debian tools and the\n"
"  administrator who wants to build a reliable network with Debian."
msgstr ""
"Het eerste Franstalige boek over Debian in alweer zijn vijfde editie. Het\n"
"  behandelt alle aspecten van het beheer van Debian van de installatie tot\n"
"  de configuratie van netwerkdiensten.\n"
"  Geschreven door twee Debian-ontwikkelaars, kan dit boek voor velen "
"interessant\n"
"  zijn: de beginner die Debian wilt ontdekken, de geavanceerde gebruiker\n"
"  die tips zoekt om het gebruik van de Debian-hulpmiddelen te verbeteren en "
"de\n"
"  beheerder die een betrouwbaar netwerk wilt bouwen met Debian."

#: ../../english/doc/books.data:128
msgid ""
"The book covers topics ranging from concepts of package\n"
"  management over the available tools and how they're used to concrete "
"problems\n"
"  which may occur in real life and how they can be solved. The book is "
"written\n"
"  in German, an English translation is planned. Format: e-book (Online, "
"HTML,\n"
"  PDF, ePub, Mobi), printed book planned.\n"
msgstr ""
"Dit boek behandelt een aantal onderwerpen, gaande van de begrippen van\n"
"  pakketbeheer over het gereedschap dat u ter beschikking heeft en hoe\n"
"  u dat kunt gebruiken bij het aanpakken en oplossen van de concrete\n"
"  problemen waarmee u in het echte leven te maken krijgt. Het boek\n"
"  werd geschreven in het Duits. Een vertaling naar het Engels staat op\n"
"  stapel. Formaat: e-boek (Online, HTML, PDF, ePub, Mobi).\n"
"  Een gedrukte versie wordt voorbereid.\n"

#: ../../english/doc/books.data:149
msgid ""
"This book teaches you how to install and configure the system and also how "
"to use Debian in a professional environment.  It shows the full potential of "
"the distribution (in its current version 8) and provides a practical manual "
"for all users who want to learn more about Debian and its range of services."
msgstr ""
"Dit boek leert u het installeren en configureren van het systeem en hoe "
"Debian te gebruiken in een professionele omgeving. Het toont alle "
"mogelijkheden van de distributie (in de huidige versie 8) en biedt een "
"praktische handleiding voor alle gebruikers die meer willen leren over "
"Debian en diens scala aan diensten."

#: ../../english/doc/books.data:203
msgid ""
"Written by two penetration researcher - Annihilator, Firstblood.\n"
"  This book teaches you how to build and configure Debian 8.x server system\n"
"  security hardening using Kali Linux and Debian simultaneously.\n"
"  DNS, FTP, SAMBA, DHCP, Apache2 webserver, etc.\n"
"  From the perspective that 'the origin of Kali Linux is Debian', it "
"explains\n"
"  how to enhance Debian's security by applying the method of penetration\n"
"  testing.\n"
"  This book covers various security issues like SSL cerificates, UFW "
"firewall,\n"
"  MySQL Vulnerability, commercial Symantec antivirus, including Snort\n"
"  intrusion detection system."
msgstr ""
"Geschreven door twee penetratieonderzoekers - Annihilator, Firstblood.\n"
"  Dit boek leert je een beveiligingsversterking te bouwen en te "
"configureren\n"
"  voor een Debian 8.x serversysteem door simultaan Kali Linux en Debian te "
"gebruiken.\n"
"  DNS, FTP, SAMBA, DHCP, webserver Apache2, enz.\n"
"  In het licht van het feit dat 'Kali Linux ontstaan is uit Debian',\n"
"  legt het uit hoe de beveiliging van Debian versterkt kan worden\n"
"  door het toepassen van de penetratietestmethode.\n"
"  Dit boek behandelt verschillende beveiligingsonderwerpen zoals SSL-"
"certificaten,\n"
"  UFW-firewall, MySQL-kwetsbaarheid, het commercieel antivirusprogramma\n"
"  van Symantec evenals het inbraakdetectiesysteem van Snort."

#: ../../english/doc/books.def:38
msgid "Author:"
msgstr "Auteur:"

#: ../../english/doc/books.def:41
msgid "Debian Release:"
msgstr "Debian Release:"

#: ../../english/doc/books.def:44
msgid "email:"
msgstr "E-mail:"

#: ../../english/doc/books.def:48
msgid "Available at:"
msgstr "Beschikbaar op:"

#: ../../english/doc/books.def:51
msgid "CD Included:"
msgstr "Inclusief CD:"

#: ../../english/doc/books.def:54
msgid "Publisher:"
msgstr "Uitgever:"

#: ../../english/doc/manuals.defs:28
msgid "Authors:"
msgstr "Auteurs:"

#: ../../english/doc/manuals.defs:35
msgid "Editors:"
msgstr "Redactie:"

#: ../../english/doc/manuals.defs:42
msgid "Maintainer:"
msgstr "Verantwoordelijke:"

#: ../../english/doc/manuals.defs:49
msgid "Status:"
msgstr "Status:"

#: ../../english/doc/manuals.defs:56
msgid "Availability:"
msgstr "Beschikbaarheid:"

#: ../../english/doc/manuals.defs:85
msgid "Latest version:"
msgstr "Meest recente versie:"

#: ../../english/doc/manuals.defs:101
msgid "(version <get-var version />)"
msgstr "(versie <get-var version />)"

#: ../../english/doc/manuals.defs:131 ../../english/releases/arches.data:38
msgid "plain text"
msgstr "platte tekst"

#: ../../english/doc/manuals.defs:147 ../../english/doc/manuals.defs:157
#: ../../english/doc/manuals.defs:165
msgid ""
"The latest <get-var srctype /> source is available through the <a href="
"\"https://packages.debian.org/git\">Git</a> repository."
msgstr ""
"De laatste <get-var srctype /> broncode is beschikbaar via de <a href="
"\"https://www.debian.org/git\">Git</a> opslagplaats."

#: ../../english/doc/manuals.defs:149 ../../english/doc/manuals.defs:159
#: ../../english/doc/manuals.defs:167
msgid "Web interface: "
msgstr "Web interface: "

#: ../../english/doc/manuals.defs:150 ../../english/doc/manuals.defs:160
#: ../../english/doc/manuals.defs:168
msgid "VCS interface: "
msgstr "VCS interface: "

#: ../../english/doc/manuals.defs:175 ../../english/doc/manuals.defs:179
msgid "Debian package"
msgstr "Debian-pakket"

#: ../../english/doc/manuals.defs:184 ../../english/doc/manuals.defs:188
msgid "Debian package (archived)"
msgstr "Debian-pakket (gearchiveerd)"

#: ../../english/releases/arches.data:36
msgid "HTML"
msgstr "HTML"

#: ../../english/releases/arches.data:37
msgid "PDF"
msgstr "PDF"
