#use wml::debian::template title="Onze filosofie: waarom we het doen en hoe we het doen"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="8d625100deb403dba223e3cecbac44a283fe02ff"

# translators: some text is taken from /intro/about.wml

<ul class="toc">
<li><a href="#what">WAT is Debian?</a>
<li><a href="#free">Alles is vrij beschikbaar?</a>
<li><a href="#how">Hoe werkt de gemeenschap als project?</a>
<li><a href="#history">Hoe begon het allemaal?</a>
</ul>

<h2><a name="what">WAT is Debian?</a></h2>

<p>Het <a href="$(HOME)/">Debian-project</a> is een vereniging van
individuen die zich als gemeenschappelijk doel hebben gesteld om een
<a href="free">vrij</a> besturingssysteem te maken. Dit door ons gecreëerd
besturingssysteem heet <strong>Debian</strong>.

<p>Een besturingssysteem is de verzameling basisprogramma's en hulpprogramma's
die zorgen dat uw computer werkt. De kern van een besturingssysteem is de
kernel. De kernel is het meest fundamentele programma op de computer en doet de
basishuishouding en laat u andere programma's starten.

<p>Debian-systemen gebruiken momenteel de
<a href="https://www.kernel.org/">Linux</a>-kernel of de
<a href="https://www.freebsd.org/">FreeBSD</a>-kernel. Linux is een stuk
software dat is gestart door
<a href="https://en.wikipedia.org/wiki/Linus_Torvalds">Linus Torvalds</a>
en wordt ondersteund door duizenden programmeurs over de hele wereld.
FreeBSD is een besturingssysteem met een kernel en andere software.

<p>Er wordt echter gewerkt aan het beschikbaar stellen van Debian voor andere
kernels, in de eerste plaats voor
<a href="https://www.gnu.org/software/hurd/hurd.html">de Hurd</a>.
De Hurd is een verzameling servers die bovenop een microkernel (zoals Mach)
draaien om verschillende functies te implementeren. De Hurd is door het
<a href="https://www.gnu.org/">GNU-project</a> geproduceerde vrije software.

<p>Een groot deel van de basishulpmiddelen die het besturingssysteem
vervolledigen, komen uit het <a href="https://www.gnu.org/">GNU-project</a>,
vandaar de benamingen GNU/Linux, GNU/kFreeBSD en GNU/Hurd.
Ook deze hulpmiddelen zijn vrij.

<p>Wat mensen willen is natuurlijk toepassingssoftware: programma's die hen
helpen om gedaan te krijgen wat ze willen doen, van het bewerken van documenten
over het beheren van een bedrijf tot het spelen van spelletjes of het schrijven
van meer software. Debian wordt geleverd met meer dan
<packages_in_stable> <a href="$(DISTRIB)/packages">pakketten</a>
(voorgecompileerde software die gebundeld is in een mooi formaat voor een
eenvoudige installatie op uw machine), een programma voor pakketbeheer (APT) en
andere hulpprogramma's die het mogelijk maken om duizenden pakketten op
duizenden computers net zo gemakkelijk te beheren als het installeren van één
enkele applicatie. En dat allemaal <a href="free">vrij</a>.
</p>

<p>Het is een beetje als een toren. Aan de basis is de kernel.
Daarbovenop komt al het basisgereedschap.
Vervolgens komt alle software die u op de computer gebruikt.
Bovenaan de toren bevindt zich Debian &mdash; alles zorgvuldig aan het
organiseren en aan het inpassen zodat het allemaal samenwerkt.

<h2>Alles is <a href="free" name="free">vrij</a> beschikbaar?</h2>

<p>Als we het woord "vrij" ("free" in het Engels, wat zowel voor vrij als voor
gratis/vrij beschikbaar kan gebruikt worden) gebruiken, bedoelen we de
<strong>vrijheid</strong> van software. U kunt meer lezen over
<a href="free">what we mean by "vrije software"</a> en
<a href="https://www.gnu.org/philosophy/free-sw">wat de Free Software
Foundation over dat onderwerp zegt</a>.

<p>Misschien vraagt u zich af: waarom investeren mensen uren van hun
eigen tijd in het schrijven en zorgvuldig verpakken van software, om deze
vervolgens weg te <em>geven</em>?  De antwoorden zijn zo verschillend
als de mensen die bijdragen leveren. Sommigen zijn graag behulpzaam. Velen
schrijven programma’s om meer te leren over computers. Steeds meer mensen
zoeken naar manieren om de opgeblazen prijs van software te vermijden.
Een groeiende groep helpt mee als dank voor al de prachtige vrije software die
ze van anderen hebben gekregen. Velen in de academische wereld maken vrije
software om het gebruik van de resultaten van hun onderzoek op grotere schaal
te bevorderen. Bedrijven helpen met het onderhouden van vrije software om
inspraak te hebben in de verdere ontwikkeling ervan -- er is geen snellere
manier om een nieuwe functie te verkrijgen dan die zelf te implementeren! En
natuurlijk zijn er velen onder ons die het gewoon leuk vinden.

<p>Debian is zo toegewijd aan vrije software dat het ons nuttig leek om deze toewijding te formaliseren in een schriftelijk document. Zo is ons
<a href="$(HOME)/social_contract">Sociale Contract</a> geboren.

<p>Hoewel Debian gelooft in vrije software, zijn er gevallen waarin mensen
niet-vrije software op hun computer willen of moeten gebruiken. Waar mogelijk
ondersteunt Debian dit. Er is een groeiend aantal pakketten die als enige
taak hebben om niet-vrije software te installeren op een Debian systeem.

<h2><a name="how">Hoe werkt de gemeenschap als project?</a></h2>

<p>Debian wordt gemaakt door bijna duizend actieve
ontwikkelaars, verspreid
<a href="$(DEVEL)/developers.loc">over de hele wereld</a>,
die als vrijwilliger meewerken in hun vrije tijd.
Slechts weinig ontwikkelaars hebben elkaar persoonlijk ontmoet.
Communicatie verloopt voornamelijk via e-mail (mailinglijsten op
lists.debian.org) en IRC (#debian kanaal op irc.debian.org).
</p>

<p>Het Debian Project heeft een zorgvuldig uitgebouwde
<a href="organization">organisatiestructuur</a>.
Voor meer informatie over hoe Debian er van binnenuit uitziet, kunt u eens
rondkijken in <a href="$(DEVEL)/">de hoek voor ontwikkelaars</a>.</p>

<p>
De belangrijkste documenten waarin wordt uitgelegd hoe de gemeenschap werkt,
zijn de volgende:
<ul>
<li><a href="$(DEVEL)/constitution">De statuten van Debian</a></li>
<li><a href="../social_contract">Het Sociaal Contract en de Richtlijnen voor
Vrije Software</a></li>
<li><a href="diversity">De Diversiteitsverklaring</a></li>
<li><a href="../code_of_conduct">De Gedragscode</a></li>
<li><a href="../doc/developers-reference/">Het Referentiehandboek voor
ontwikkelaars</a></li>
<li><a href="../doc/debian-policy/">Het Beleidshandboek van Debian</a></li>
</ul>

<h2><a name="history">Hoe is het allemaal begonnen?</a></h2>

<p>Het Debian project werd in augustus 1993 gestart door Ian Murdock,
als een nieuwe distributie die openlijk ontwikkeld zou worden, in de geest
van Linux en GNU. Het was de bedoeling om Debian voorzichtig en
zorgvuldig op te bouwen en het even goed te onderhouden en
ondersteunen. Het begon als een kleine, hechte groep ontwikkelaars van
Vrije Software en groeide geleidelijk uit tot een grote, goed
georganiseerde gemeenschap van ontwikkelaars en gebruikers. Zie
<a href="$(DOC)/manuals/project-history/">de gedetailleerde geschiedenis</a>.

<p>Omdat veel mensen er naar vragen: Debian wordt uitgesproken als
/&#712;de.bi.&#601;n/. De naam is afgeleid van de naam van de initiatiefnemer
van Debian, Ian Murdock, en van die van zijn vrouw, Debra.

