#use wml::debian::template title="BTS-systeem van Debian &mdash; info voor ontwikkelaars" NOHEADER=yes NOCOPYRIGHT=true
#include "$(ENGLISHDIR)/Bugs/pkgreport-opts.inc"
#use wml::debian::translation-check translation="204e82ebfe88f1243e0f7d9a424fd41a034b7f9b"

<h1>Informatie over het bugverwerkingssysteem voor pakketonderhouders en bugsorteerders</h1>

<p>Aanvankelijk wordt een bugrapport ingediend door een gebruiker
met een gewoon e-mailbericht naar <code>submit@bugs.debian.org</code>.
Dit moet een regel bevatten met <code>Package</code>
(zie <a href="Reporting">Instructies voor het rapporteren van een bug</a>
voor meer informatie). Dit krijgt dan een nummer; de indiener krijgt een
ontvangstbevestiging en het rapport wordt doorgestuurd naar
<code>debian-bugs-dist</code>. Indien de <code>Package</code>-regel
een pakket bevat met een gekende onderhouder, dan zal die ook een kopie
ontvangen.</p>

<p>Aan de <code>Onderwerp</code>-regel wordt
<code>Bug#</code><var>nnn</var><code>:</code> toegevoegd en
<code>Antwoorden-naar</code> wordt zo ingesteld dat dit veld zowel de
indiener van het rapport als <var>nnn</var><code>@bugs.debian.org</code>
bevat.</p>

<ul class="toc">
  <li><a href="#closing">Bugrapporten sluiten</a></li>
  <li><a href="#followup">Follow-up berichten</a></li>
  <li><a href="#severities">Graden van ernstigheid</a></li>
  <li><a href="#tags">Tags voor bugrapporten</a></li>
  <li><a href="#forward">Optekenen dat u een bugrapport hebt doorgestuurd</a></li>
  <li><a href="#owner">Veranderen van bugeigenaar</a></li>
  <li><a href="#maintincorrect">Onjuist vermelde pakketonderhouders</a></li>
  <li><a href="#requestserv">Heropenen, opnieuw toewijzen en manipuleren van bugs</a></li>
  <li><a href="#subscribe">Intekenen op bugs</a></li>
  <li><a href="#subjectscan">Min of meer verouderde functie voor het scannen van onderwerpen</a></li>
  <li><a href="#x-debian-pr">Achterhaalde functie <code>X-Debian-PR: quiet</code></a></li>
</ul>

<h2><a name="closing">Bugrapporten sluiten</a></h2>

<p>Bugrapporten in Debian moeten gesloten worden als het probleem
opgelost is. Problemen in pakketten kunnen enkel als opgelost beschouwd
worden, wanneer een pakket met de oplossing voor de bug het archief van
Debian binnenkomt.</p>

<p>Normaal zijn de enige mensen die een bugrapport mogen sluiten,
de indiener van het rapport en de onderhouder(s) van het pakket waartegen
de bug ingediend werd. Er bestaan uitzonderingen op deze regel,
bijvoorbeeld voor bugs die tegen een onbekend pakket ingediend werden of
tegen bepaalde generieke pseudopakketten. Een bug kan gesloten
worden door iedere medewerker indien de bug een <strong>verweesd</strong>
pakket betreft, of indien de onderhouder naliet het te sluiten. Het is
zeer belangrijk om de versie te vermelden waarin de bug opgelost werd.
Sluit bij twijfel geen bugs, maar vraag eerst om advies op de
mailinglijst debian-devel.</p>

<p>Bugrapporten moeten gesloten worden door een e-mail te sturen naar
<var>nnn</var><code>-done@bugs.debian.org</code>. De berichttekst moet
een uitleg bevatten over hoe de bug opgelost is.</p>

<p>Met de e-mails die u ontving van het bugvolgsysteem, is het enige wat
u moet doen om een bug te sluiten, zo'n bericht in uw e-mailprogramma
beantwoorden en het veld <code>Aan:</code> veranderen naar
<var>nnn</var><code>-done@bugs.debian.org</code> in plaats van
<var>nnn</var><code>@bugs.debian.org</code>
(<var>nnn</var><code>-close</code> is een alias voor
<var>nnn</var><code>-done</code>).</p>

<p>Geef, indien van toepassing, een <code>Version</code>regel op in de
<a href="Reporting#pseudoheader">pseudokop</a> van uw bericht bij het
sluiten van een bug, zodat het bugvolgsysteem weet welke uitgaven van het
pakket de oplossing bevatten.</p>

<p>De persoon die de bug sloot, de persoon die de bug indiende en de
mailinglijst <code>debian-bugs-closed</code> krijgen elk een melding
over de gewijzigde toestand van het rapport. De indiener en de mailinglijst
ontvangen ook de inhoud van het bericht dat verzonden werd naar
<var>nnn</var><code>-done</code>.</p>


<h2><a name="followup">Follow-up berichten</a></h2>

<p>Bij het doorsturen van het bugrapport zal het bugvolgsysteem het
adres van de indiener en het adres van de bug
(<var>nnn</var><code>@bugs.debian.org</code>) toevoegen aan het kopveld
<code>Antwoordadres:</code>. Merk op dat dit twee verschillende adressen
zijn.</p>

<p>
Elke ontwikkelaar die op een bugrapport wil antwoorden, moet gewoon het
bericht beantwoorden met inachtneming van het kopveld
<code>Antwoordadres:</code>. Daardoor wordt de bug <strong>niet</strong>
gesloten.</p>

<p>Gebruik <em>niet</em> de functies <q>Allen beantwoorden</q> of
<q>Opvolgingsbericht</q> van uw e-mailprogramma, tenzij u van plan bent de
ontvangers aanzienlijk aan te passen. Zorg er in het bijzonder voor om geen
opvolgingsberichten te sturen naar <code>submit@bugs.debian.org</code>.</p>

<p>
Berichten kunnen naar de volgende adressen worden verzonden om opgenomen
te worden in het bugvolgsysteem:
</p>

<ul>
<li>
<var>nnn</var><code>@bugs.debian.org</code> — zulke berichten worden ook
gestuurd naar de pakketonderhouder en doorgestuurd naar
<code>debian-bugs-dist</code>, maar <strong>niet</strong> naar de
indiener;
</li>
<li>
<var>nnn</var><code>-submitter@bugs.debian.org</code> — deze berichten
worden ook gestuurd naan de indiener en doorgestuurd naar
<code>debian-bugs-dist</code>, maar <strong>niet</strong> naar de
pakketonderhouder;
</li>
<li>
<var>nnn</var><code>-maintonly@bugs.debian.org</code> — deze berichten
worden enkel naar de pakketonderhouder gestuurd en <strong>niet</strong>
naar de indiener, noch naar <code>debian-bugs-dist</code>;
</li>
<li>
<var>nnn</var><code>-quiet@bugs.debian.org</code> —  deze berichten worden
enkel gearchiveerd in het bugvolgsysteem (net als alle hierboven vermelde),
maar worden naar <strong>niemand anders</strong> gestuurd.
</li>
</ul>

<p>Raadpleeg <a href="Reporting">Instructies voor het rapporteren van een bug</a>
voor meer informatie over kopvelden voor het onderdrukken van
ontvangstbevestigingsberichten en hoe u met het bugvolgsysteem Cc'tjes
kunt zenden.</p>


<h2><a name="severities">Graden van ernstigheid</a></h2>

<p>Het bugsysteem registreert een ernstigheidsniveau bij elk bugrapport.
Standaard is dit <code>normal</code>, maar dit kan vervangen worden
door in de pseudokop een regel met <code>Severity</code> op te nemen
bij het indienen van het bugrapport (zie de
<a href="Reporting#pseudoheader">Instructies voor het rapporteren van een bug</a>),
of door het commando <code>severity</code> te gebruiken met de
<a href="#requestserv">server voor controleaanvragen</a>.</p>

<p>De ernstigheidsniveaus zijn:</p>

<dl>
<dt><code>critical</code></dt>
<dd>maakt niet-gerelateerde software op het systeem (of het hele systeem)
onbruikbaar, of veroorzaakt ernstig gegevensverlies, of introduceert een
beveiligingslek op systemen waarop men het pakket installeert.</dd>

<dt><code>grave</code></dt>
<dd>maakt het pakket in kwestie onbruikbaar of zo goed als, of veroorzaakt
gegevensverlies, of introduceert een beveiligingslek waardoor toegang
verkregen kan worden tot de accounts van gebruikers die het pakket
gebruiken.</dd>

<dt><code>serious</code></dt>
<dd>is een <a href="$(DOC)/debian-policy/">ernstige schending van het Debian-beleid</a>
(is ruwweg een schending van een <q>must</q>- of een <q>required</q>-richtlijn),
of het maakt het pakket ongeschikt voor uitgave naar de mening van de pakketonderhouder of de releasemanager.</dd>

<dt><code>important</code></dt>
<dd>een bug die een groot effect heeft op de bruikbaarheid van een pakket,
zonder het voor iedereen volledig onbruikbaar te maken.</dd>

<dt><code>normal</code></dt>
<dd>de standaardwaarde, van toepassing op de meeste bugs.</dd>

<dt><code>minor</code></dt>
<dd>een probleem dat het nut van het pakket niet beïnvloedt en dat
vermoedelijk makkelijk op te lossen is.</dd>

<dt><code>wishlist</code></dt>
<dd>voor elk functieverzoek en ook voor alle bugs die zeer moeilijk te verhelpen zijn vanwege belangrijke overwegingen inzake productontwerp.</dd>
</dl>

<p>Bepaalde ernstigheidsgraden worden als
<em><a href="https://bugs.debian.org/release-critical/">release-kritiek</a></em> beschouwd,
wat betekent dat de bug een impact zal hebben op het uitbrengen van het
pakket in de volgende stabiele release van Debian. Momenteel zijn dit
<strong>critical</strong>, <strong>grave</strong> en <strong>serious</strong>. Voor de volledige en canonieke regels over welke problemen
deze ernstigheidskwalificaties verdienen, kunt u de lijst met
<a href="https://release.debian.org/testing/rc_policy.txt">release-kritieke
problemen voor de volgende release</a> raadplegen.</p>

<h2><a name="tags">Tags voor bugrapporten</a></h2>

<p>Elke bug kan nul of meer van een aantal mogelijke tags bevatten. Deze tags
worden weergegeven in de lijst met bugs wanneer u naar de pagina van een
pakket of naar het volledige buglogboek kijkt.</p>

<p>Tags kunnen ingesteld worden met een <code>Tags</code>-regel in de
pseudokop bij het indienen van het bugrapport (zie de
<a href="Reporting#pseudoheader">Instructies voor het rapporteren van een bug</a>),
of door het commando <code>tags</code> te gebruiken met de
<a href="#requestserv">server voor controleaanvragen</a>.
Scheid meerdere tags met komma's, spaties of beide.</p>

<p>De huidige bugtags zijn: <bts_tags>. Hier volgt wat detailinformatie
over de tags:</p>

<dl>

<dt><code>patch</code></dt>
  <dd>Het buglogboek bevat een patch of een andere eenvoudige werkwijze
  om de bug te repareren. Als er een patch is maar als dit het
  probleem niet adequaat oplost of andere problemen veroorzaakt, mag
  u deze tag niet gebruiken.</dd>

<dt><code>wontfix</code></dt>
  <dd>Deze bug zal niet gerepareerd worden. Mogelijk is dit omdat het
  een keuze betreft tussen twee willekeurige manieren om dingen te doen,
  waarbij de pakketonderhouder en de bugindiener een andere voorkeur
  hebben om dingen te doen. Een andere mogelijke reden is dat een
  gedragswijziging andere, ergere problemen zou veroorzaken voor anderen.
  Maar mogelijk is het omwille van nog andere redenen.</dd>

<dt><code>moreinfo</code></dt>
  <dd>Deze bug kan niet aangepakt worden totdat de indiener meer informatie
  verschaft. De bug zal gesloten worden als de indiener binnen een
  redelijke termijn (enkele maanden) geen extra informatie verschaft.
  Dit betreft bugs in de zin van <q>Het werkt niet</q>. Wat werkt niet?</dd>

<dt><code>unreproducible</code></dt>
  <dd>Deze bug kan op het systeem van de pakketonderhouder niet
  gereproduceerd worden. Hulp van anderen is nodig voor de diagnose van de
  oorzaak van het probleem.</dd>

<dt><code>help</code></dt>
<dd>De onderhouder vraagt hulp bij het oplossen van dit probleem.
  Mogelijk heeft de onderhouder niet de nodige vaardigheden om de
  bug te repareren en wenst deze medewerking, of mogelijk heeft
  de pakketonderhouder te veel werk en wil deze de taak delegeren.
  Deze bug kan ongeschikt zijn voor nieuwe medewerkers, tenzij ze
  ook gemarkeerd werd met de tag <code>newcomer</code>.</dd>

<dt><code>newcomer</code></dt>
  <dd>Voor deze bug bestaat een bekende oplossing, maar de
  pakketonderhouder vraagt dat iemand anders deze oplossing implementeert.
  Dit is een ideale taak voor nieuwe medewerkers, welke zich in Debian
  willen engageren of hun vaardigheden willen verbeteren.</dd>

<dt><code>pending</code></dt>
  <dd>Voor deze bug werd een oplossing gevonden en binnenkort zal er een
  upload gebeuren.</dd>

<dt><code>fixed</code></dt>
  <dd>Deze bug werd gerepareerd of omzeild (door een upload door iemand
  anders dan de pakketonderhouder, bijvoorbeeld), maar er is nog steeds
  een probleem dat opgelost moet worden. Deze tag vervangt de oude
  ernstigheidsgraad <q>fixed</q>.</dd>

<dt><code>security</code></dt>
  <dd>Deze bug beschrijft een beveiligingsprobleem in een pakket (bijv.
  slechte toegangsrechten die toegang mogelijk maken tot data welke niet
  toegankelijk zouden mogen zijn; bufferoverlopen, waardoor mensen een
  systeem kunnen controleren op manieren die ze niet zouden moeten kunnen;
  denial-of-serviceaanvallen die moeten worden verholpen, enz.). De
  meeste beveiligingsbugs moeten een ernstigheidsniveau critical (kritiek)
  of grave (zorgwekkend) krijgen.</dd>

<dt><code>upstream</code></dt>
  <dd>Deze bug heeft betrekking op het bovenstroomse deel van het pakket.</dd>

<dt><code>confirmed</code></dt>
  <dd>De pakketonderhouder heeft naar de bug gekeken, begrijpt het
  probleem en is het er grotendeels mee eens, maar moet het nog oplossen.
  (Het gebruik van deze tag is facultatief. Hij is hoofdzakelijk bedoeld
  voor onderhouders welke een groot aantal open bugs moeten beheren.)</dd>

<dt><code>fixed-upstream</code></dt>
  <dd>Deze bug werd door de bovenstroomse onderhouder opgelost, maar
  nog niet in het pakket (om welke reden dan ook: misschien is het te
  gecompliceerd om de oplossing over te zetten naar een eerdere versie of
  is het probleem te klein om de moeite waard te zijn).</dd>

<dt><code>fixed-in-experimental</code></dt>
  <dd>De bug werd gerepareerd in het pakket dat zich in de distributie
  experimental bevindt, maar nog niet in de distributie unstable.</dd>

<dt><code>d-i</code></dt>
  <dd>Deze bug is relevant voor de ontwikkeling van het
  installatieprogramma van Debian. Men verwacht dat u deze tag gebruikt
  als de bug de ontwikkeling van het installatieprogramma beïnvloedt,
  maar niet ingediend werd tegen een pakket dat rechtstreeks deel uitmaakt
  van het installatieprogramma zelf.</dd>

<dt><code>ipv6</code></dt>
  <dd>Deze bug beïnvloedt de ondersteuning voor het Internet Protocol
  versie 6.</dd>

<dt><code>lfs</code></dt>
  <dd>Deze bug beïnvloedt de ondersteuning voor grote bestanden (meer
  dan 2 gigabyte).</dd>

<dt><code>l10n</code></dt>
  <dd>Deze bug is relevant voor de lokalisatie van het pakket.</dd>

<dt><code>a11y</code></dt>
  <dd>Deze bug is relevant voor de toegankelijkheid van het pakket voor
  mensen met een beperking.</dd>

<dt><code>ftbfs</code></dt>
  <dd>Het bouwen van het pakket uit de broncode mislukt. Indien de bug
  toegewezen werd aan een broncodepakket, dan mislukt het bouwen van dat
  pakket. Indien de bug toegewezen werd aan een binair pakket, kunnen de
  getroffen broncodepakketten niet gebouwd worden. De tag kan toegepast
  worden op niet-standaard bouwomgevingen (bijv. met gebruik van
  Build-Depends uit experimental), maar in dergelijke gevallen moet de
  ernstigheidsgraad lager dan serious (ernstig), d.w.z.release-kritiek,
  zijn.</dd>

<dt><bts_release_tags></dt>
  <dd>Dit zijn release-tags, die twee effecten hebben. Wanneer ze
  ingesteld worden op een bug, kan de bug enkel die specifieke
  release beïnvloeden (al kan ze ook andere releases beïnvloeden indien
  andere release-tags ingesteld werden), maar voor het overige gelden
  de normale regels aangaande getroffen/opgelost/afwezig.
  De bug mag ook niet gearchiveerd worden totdat hij gerepareerd werd
  in de release.</dd>

<dt><bts_release_ignore_tags></dt>
  <dd>Deze release-kritieke bug moet genegeerd worden met het oog op
  de uitgave van die specifieke release. <strong>Deze tags mogen enkel
  gebruikt worden door de releasemanager(s); gebruik ze niet zelf
  zonder hun expliciete goedkeuring.</strong></dd>

</dl>

<p>Wat informatie over distributie-specifieke tags:
  de tags <q>-ignore</q> zorgen ervoor dat de bug genegeerd wordt
  met het oog op de doorstroming van het pakket naar de
  testing-distributie. De <q>release</q>-tags geven aan dat de bug
  in kwestie niet gearchiveerd mag worden totdat hij gerepareerd is
  in de reeks gespecificeerde releases. Deze tags geven ook aan dat een bug
  enkel als een bug beschouwd mag worden voor de reeks van gespecificeerde
  releases. [Met andere woorden, de bug is <strong>afwezig</strong> in elke
  release waarvoor de overeenkomstige release-tag <strong>niet</strong>
  ingesteld werd, wanneer release-tags gebruikt werden; voor het overige
  gelden de normale regels i.v.m. gevonden/gerepareerd.]
</p>

<p>
  Release-tags mogen <strong>niet</strong> gebruikt worden als een juiste
  toewijzing van de bug aan een pakketversie het gewenste effect kan
  hebben, omdat deze tags manuele interventie vereisen om ze toe te
  voegen en te verwijderen. Indien u erover twijfelt of een release-tag
  vereist is, vraag dan om advies aan de beheerders van het Debian BTS
  (<email "owner@bugs.debian.org">) of aan het releaseteam.
</p>

<h2><a name="forward">Optekenen dat u een bugrapport hebt doorgestuurd</a></h2>

<p>Wanneer een ontwikkelaar een bugrapport doorstuurt naar de ontwikkelaar
van het bovenstroomse pakket waarvan het Debian-pakket afgeleid is, moet
deze dit als volgt noteren in het bugvolgsysteem:</p>

<p>Zorg ervoor dat het veld <code>Aan:</code> van uw bericht naar de auteur
enkel het/de adres(sen) van de auteur(s) bevat; plaats de persoon die de
bug rapporteerde, <var>nnn</var><code>-forwarded@bugs.debian.org</code>
en <var>nnn</var><code>@bugs.debian.org</code> in het veld <code>Cc:</code>.</p>

<p>Vraag de auteur wanneer deze antwoordt om de <code>Cc:</code> naar
<var>nnn</var><code>-forwarded@bugs.debian.org</code> te behouden,
zodat het bugvolgsysteem dit antwoord kan opslaan bij het
originele rapport. Deze berichten worden enkel opgeslagen en niet
doorgestuurd; om zoals gewoonlijk een bericht te sturen, moet u het ook
naar <var>nnn</var><code>@bugs.debian.org</code> sturen.</p>

<p>Wanneer het bugvolgsysteem een bericht krijgt op
<var>nnn</var><code>-forwarded</code>, zal het de betrokken bug markeren
als doorgestuurd naar het/de adres(sen) in het <code>Aan:</code>-veld van
het bericht dat het ontvangt, mocht de bug nog niet als doorgestuurd
gemarkeerd zijn.</p>

<p>U kunt de informatie in verband met <q>doorgestuurd aan</q> ook
manipuleren door berichten te sturen naar
<a href="server-control"><code>control@bugs.debian.org</code></a>.</p>


<h2><a name="owner">Veranderen van bugeigenaar</a></h2>

<p>In de gevallen waarin de persoon die verantwoordelijk is voor
het repareren van een bug niet de aangewezen onderhouder is van het
betreffende pakket (bijvoorbeeld als het pakket door een team onderhouden
wordt), kan het nuttig zijn om dit feit te noteren in het bugvolgsysteem.
Om hierbij behulpzaam te zijn, kan elke bug facultatief een eigenaar
hebben.</p>

<p>De eigenaar kan ingesteld worden door bij het indienen van het
bugrapport een <code>Owner</code>-regel toe te voegen in de pseudokop
(zie de <a href="Reporting#pseudoheader">instructies voor het rapporteren van bugs</a>), of door de commando's <code>owner</code> en <code>noowner</code>
te gebruiken met de <a href="#requestserv">server voor controleaanvragen</a>.</p>


<h2><a name="maintincorrect">Onjuist vermelde pakketonderhouders</a></h2>

<p>Als de onderhouder van een pakket niet correct wordt vermeld, is dit
meestal omdat er recent een nieuwe onderhouder kwam en deze
nog geen nieuwe versie van het pakket geüpload heeft met een aangepast
veld <code>Maintainer</code> in het control-bestand. Dit zal rechtgezet
worden wanneer het pakket geüpload wordt; een andere mogelijkheid is dat
de beheerders van het archief handmatig het maintainer-element van het
pakket overschrijven, bijvoorbeeld als niet te verwachten valt dat het
pakket binnenkort opnieuw zal gebouwd en geüpload moeten worden. Contacteer
<code>override-change@debian.org</code> voor aanpassingen aan het
override-bestand.</p>


<h2><a name="requestserv">Heropenen, opnieuw toewijzen en manipuleren van bugs</a></h2>

<p>Het is mogelijk om bugrapporten opnieuw toe te wijzen aan andere
pakketten, om ten onrechte gesloten bugs opnieuw te openen, om
wijzigingen aan te brengen in de informatie over waarnaartoe
een bugrapport eventueel doorgestuurd werd, om de ernstigheid van een bug
of de titel van een rapport aan te passen, om de eigenaar van een bug
in te stellen, om bugrapporten samen te voegen of uit te splitsen en om
de versies van pakketten te noteren waarin bugs gevonden werden en
waarin deze opgelost werden. Dit gebeurt door het sturen van e-mail naar
<code>control@bugs.debian.org</code>.</p>

<p>De <a href="server-control">indeling van deze berichten</a> wordt
beschreven in een ander document, dat te vinden is op het World Wide Web of
in het bestand <code>bug-maint-mailcontrol.txt</code>. Een versie in
gewone tekst kan ook verkregen worden door het woord <code>help</code>
te mailen naar de server op het hierboven vermelde adres.</p>

<h2><a name="subscribe">Intekenen op bugs</a></h2>

<p>Het bugvolgsysteem laat ook toe dat indieners van bugs, ontwikkelaars
en andere geïnteresseerde derden intekenen op individuele bugs. Deze
functionaliteit kan gebruikt worden door diegenen die een bug in het oog
willen houden, zonder te hoeven intekenen op een pakket via de
<a href="https://tracker.debian.org">Pakkettracering van Debian</a>.
Alle berichten die op <var>nnn</var><code>@bugs.debian.org</code>
ontvangen worden, worden doorgestuurd naar de intekenaars.</p>

<p>Intekenen op een bug kan door een e-mail te sturen naar
<var>nnn</var><code>-subscribe@bugs.debian.org</code>. Het onderwerp-veld
en de tekst van het bericht worden genegeerd door het BTS. Na verwerking
van het bericht, krijgen gebruikers een bevestigingsbericht waarop ze
moeten antwoorden en pas nadien zullen de berichten in verband met deze
bug naar hen doorgestuurd worden.</p>

<p>Het is eveneens mogelijk om zich uit te schrijven op een bug. Zich
uitschrijven gebeurt door het sturen van een e-mail naar
<var>nnn</var><code>-unsubscribe@bugs.debian.org</code>. Ook nu worden
het onderwerp-veld en de tekst van het bericht genegeerd door het BTS.
Gebruikers krijgen dan een bevestigingsbericht waarop ze moeten antwoorden
als ze zich willen uitschrijven op de bug.</p>

<p>Standaard is het adres van de intekenaar, het adres dat te vinden is
in het <code>Van:</code>-kopveld van het bericht. Indien u voor een ander
adres wilt intekenen, zult u dit moeten coderen in het intekenbericht.
Dit gebeurt in de volgende vorm:
<var>nnn</var><code>-subscribe-</code>\
<var>localpart</var><code>=</code>\
<var>example.com</var><code>@bugs.debian.org</code>.
In dit voorbeeld zal aan <code>localpart@example.com</code> een
bevestigingsbericht over de intekening op bug <var>nnn</var> gestuurd
worden. Het <code>@</code>-teken moet gecodeerd worden door het te
vervangen door een <code>=</code>-teken. Op dezelfde wijze
zal een uitschrijving de volgende vorm aannemen:
<var>nnn</var><code>-unsubscribe-</code><var>localpart</var>\
<code>=</code><var>example.com</var><code>@bugs.debian.org</code>.
In beide gevallen worden het onderwerp en de tekst van de e-mail
doorgestuurd naar dit e-mailadres bij de vraag om bevestiging.</p>

<h2><a name="subjectscan">Min of meer verouderde functie voor het scannen van onderwerpen</a></h2>

<p>Berichten die toekomen op <code>submit</code> of <code>bugs</code>,
waarvan het Onderwerp-veld begint met <code>Bug#</code><var>nnn</var>,
worden behandeld als een bericht dat gezonden werd naar
<var>nnn</var><code>@bugs.debian.org</code>. Dit is zowel bedoeld voor
achterwaartse compatibiliteit met e-mail die doorgestuurd werd vanaf
de oude adressen als voor het onderscheppen van vervolgmail die per
vergissing gezonden werd naar <code>submit</code> (bijvoorbeeld bij het
gebruik van <q>Allen beantwoorden</q>).</p>

<p>Een soortgelijke regeling geldt voor <code>maintonly</code>,
<code>done</code>, <code>quiet</code> en <code>forwarded</code>,
waarbij ontvangen e-mail met een tag in het Onderwerp-veld, behandeld
wordt als bedoeld voor het overeenkomstige adres <var>nnn-iets</var><code>@bugs.debian.org</code>.</p>

<p>Ontvangen e-mail met een eenvoudige <code>forwarded</code> of
<code>done</code> &mdash; d.w.z. zonder bugrapportnummer in het adres &mdash; en
zonder bugnummer in het Onderwerp-veld, zal gearchiveerd worden onder
<q>junk</q> en zal gedurende enkele weken bewaard worden, maar voor het overige genegeerd worden.</p>


<h2><a name="x-debian-pr">Achterhaalde functie <code>X-Debian-PR: quiet</code></a></h2>

<p>Vroeger was het mogelijk om te voorkomen dat het bugvolgsysteem
berichten die het ontving op <code>debian-bugs</code>, naar ergens
doorstuurde door een regel <code>X-Debian-PR: quiet</code> te plaatsen
in de eigenlijke kop van het bericht.</p>

<p>Deze regel in de kop wordt nu genegeerd. Stuur in de plaats daarvan
uw bericht naar <code>quiet</code> of <var>nnn</var><code>-quiet</code> (of
<code>maintonly</code> of <var>nnn</var><code>-maintonly</code>).</p>

<hr />

#use "otherpages.inc"

#use "$(ENGLISHDIR)/Bugs/footer.inc"
