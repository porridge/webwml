#use wml::debian::template title="Les pages Debian en portugais" NOHEADER="yes"

#use wml::debian::translation-check translation="ca5f8ab5a2fa424c129c2277a06525c91a69a6aa" maintainer="Christian Couder"

# Translators:
# Christian Couder, 2001, 2002, 2004.
# Dabid Prévot, 2012.

<h1>Debian en portugais</h1>

<p>
Cette page vous donne des informations spécifiques sur notre effort
de traduction de Debian en portugais. Notre équipe est composée
uniquement de brésiliens, et, de ce fait, le portugais brésilien
domine fortement dans ces pages. Cette équipe, qui est formée à la
fois de développeurs Debian et d'utilisateurs, se rassemble dans un
projet appelé <a href="https://wiki.debian.org/Brasil">Debian Brasil</a>.
</p>
<p>
Pour savoir comment vous inscrire aux listes de diffusion pour les
utilisateurs de langue portugaise, et comment nous rejoindre sur notre
canal IRC, consultez nos sections <a
href="https://wiki.debian.org/Brasil/Listas">listes</a> et <a
href="https://wiki.debian.org/Brasil/IRC">IRC</a> des pages du projet Debian-BR.
</p>
<p>
Pour accéder à notre documentation en portugais, comme les manuels traduits et
ceux faits par les membres de l'équipe, consultez la <a
href="https://wiki.debian.org/Brasil/Documentos">section
sur la documentation</a> du site.
</p>
<p>
Pour avoir les dernières nouvelles sur le monde Debian en
portugais, consultez la <a href="$(HOME)/News/weekly/">
section des nouvelles</a>.
</p>
<p>
Pour en savoir plus sur les groupe d'utilisateurs et les activités faites,
consultez la section des <a href="https://wiki.debian.org/Brasil/GUD">groupes
d'utilisateurs Debian</a>.
</p>
<p>
Si vous voulez <strong>vraiment</strong> aider à développer encore
plus Debian, consultez les différentes façons possibles dans la section
<a href="https://wiki.debian.org/Brasil/ComoColaborar">Comment aider</a>
ou envoyez un <a href="debian-br-geral@alioth-lists.debian.net"> message</a>.
</p>

<p>
Même si nous avons «&nbsp;Brasil&nbsp;» dans le nom de notre projet,
nous avons plusieurs amis habitant au Portugal dans nos listes de
diffusion et sur nos canaux IRC, et tous les utilisateurs parlant
portugais qui souhaitent nous rejoindre sont les bienvenus.
</p>
<p>
Merci&nbsp;!
</p>
<p>
L'équipe Debian Brasil
&lt;<a href="mailto:debian-br-geral@alioth-lists.debian.net">debian-br-geral@alioth-lists.debian.net</a>&gt;.
</p>
