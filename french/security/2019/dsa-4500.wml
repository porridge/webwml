#use wml::debian::translation-check translation="3a434572e5a5011fce231866da5376d28082ebe6" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le navigateur web
Chromium.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5805">CVE-2019-5805</a>

<p>Un problème d'utilisation de mémoire après libération a été découvert
dans la bibliothèque pdfium.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5806">CVE-2019-5806</a>

<p>Wen Xu a découvert un problème de dépassement d'entier dans la
bibliothèque Angle.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5807">CVE-2019-5807</a>

<p>TimGMichaud a découvert un problème de corruption de mémoire dans la
bibliothèque JavaScript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5808">CVE-2019-5808</a>

<p>cloudfuzzer a découvert un problème d'utilisation de mémoire après
libération dans Blink et WebKit.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5809">CVE-2019-5809</a>

<p>Mark Brand a découvert un problème d'utilisation de mémoire après
libération dans Blink et WebKit.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5810">CVE-2019-5810</a>

<p>Mark Amery a découvert un problème de divulgation d'informations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5811">CVE-2019-5811</a>

<p>Jun Kokatsu a découvert un moyen de contourner la fonction de partage de
ressources entre origines multiples.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5813">CVE-2019-5813</a>

<p>Aleksandar Nikolic a découvert un problème de lecture hors limites dans
la bibliothèque JavaScript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5814">CVE-2019-5814</a>

<p>@AaylaSecura1138 a découvert un moyen de contourner la fonction de
partage de ressources entre origines multiples.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5815">CVE-2019-5815</a>

<p>Nicolas Grégoire a découvert un problème de dépassement de tampon dans
Blink et WebKit.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5818">CVE-2019-5818</a>

<p>Adrian Tolbaru a découvert un problème de valeur non initialisée.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5819">CVE-2019-5819</a>

<p>Svyat Mitin a découvert une erreur dans les outils de développement.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5820">CVE-2019-5820</a>

<p>pdknsk a découvert un problème de dépassement d'entier dans la
bibliothèque pdfium.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5821">CVE-2019-5821</a>

<p>pdknsk a découvert un autre problème de dépassement d'entier dans la
bibliothèque pdfium.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5822">CVE-2019-5822</a>

<p>Jun Kokatsu a découvert un moyen de contourner la fonction de partage de
ressources entre origines multiples.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5823">CVE-2019-5823</a>

<p>David Erceg a découvert une erreur de navigation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5824">CVE-2019-5824</a>

<p>leecraso et Guang Gong ont découvert une erreur dans le lecteur
multimédia.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5825">CVE-2019-5825</a>

<p>Genming Liu, Jianyu Chen, Zhen Feng et Jessica Liu ont découvert un
problème d'écriture hors limites dans la bibliothèque JavaScript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5826">CVE-2019-5826</a>

<p>Genming Liu, Jianyu Chen, Zhen Feng et Jessica Liu ont découvert un
problème d'utilisation de mémoire après libération.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5827">CVE-2019-5827</a>

<p>mlfbrown a découvert un problème de lecture hors limites dans la
bibliothèque sqlite.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5828">CVE-2019-5828</a>

<p>leecraso et Guang Gong ont découvert un problème d'utilisation de
mémoire après libération.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5829">CVE-2019-5829</a>

<p>Lucas Pinheiro a découvert un problème d'utilisation de mémoire après
libération.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5830">CVE-2019-5830</a>

<p>Andrew Krashichkov a découvert une erreur d'identification dans la
fonction de partage de ressources entre origines multiples.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5831">CVE-2019-5831</a>

<p>yngwei a découvert une erreur d'adressage dans la bibliothèque JavaScript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5832">CVE-2019-5832</a>

<p>Sergey Shekyan a découvert une erreur dans la fonction de partage de
ressources entre origines multiples.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5833">CVE-2019-5833</a>

<p>Khalil Zhani a découvert une erreur d'interface utilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5834">CVE-2019-5834</a>

<p>Khalil Zhani a découvert un problème d'usurpation d'URL.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5836">CVE-2019-5836</a>

<p>Omair a découvert un problème de dépassement de tampon dans la
bibliothèque Angle.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5837">CVE-2019-5837</a>

<p>Adam Iawniuk a découvert un problème de divulgation d'informations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5838">CVE-2019-5838</a>

<p>David Erceg a découvert une erreur dans les permissions d'extension.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5839">CVE-2019-5839</a>

<p>Masato Kinugawa a découvert des erreurs d'implémentation dans Blink et
WebKit.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5840">CVE-2019-5840</a>

<p>Eliya Stein et Jerome Dangu ont découvert un moyen de contourner le
bloqueur de vue additionnelle (« popup »).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5842">CVE-2019-5842</a>

<p>BUGFENSE a découvert un problème d'utilisation de mémoire après
libération dans Blink et WebKit.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5847">CVE-2019-5847</a>

<p>m3plex a découvert une erreur dans la bibliothèque JavaScript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5848">CVE-2019-5848</a>

<p>Mark Amery a découvert un problème de divulgation d'informations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5849">CVE-2019-5849</a>

<p>Zhen Zhou a découvert une lecture hors limites dans la bibliothèque
Skia.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5850">CVE-2019-5850</a>

<p>Brendon Tiszka a découvert un problèmes d'utilisation de mémoire après
libération dans le chargeur de page hors-ligne.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5851">CVE-2019-5851</a>

<p>Zhe Jin a découvert un problème de « use-after-poison » (« utilisation
après empoisonnement »).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5852">CVE-2019-5852</a>

<p>David Erceg a découvert un problème de divulgation d'informations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5853">CVE-2019-5853</a>

<p>Yngwei et sakura ont découvert un problème de corruption de mémoire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5854">CVE-2019-5854</a>

<p>Zhen Zhou a découvert un problème de dépassement d'entier dans la
bibliothèque pdfium.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5855">CVE-2019-5855</a>

<p>Zhen Zhou a découvert un problème de dépassement d'entier dans la bibliothèque pdfium.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5856">CVE-2019-5856</a>

<p>Yongke Wang a découvert une erreur liée aux permissions de l'URI
« filesystem: ».</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5857">CVE-2019-5857</a>

<p>cloudfuzzer a découvert un moyen pour planter Chromium.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5858">CVE-2019-5858</a>

<p>evil1m0 a découvert un problème de divulgation d'informations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5859">CVE-2019-5859</a>

<p>James Lee a découvert un moyen pour charger d'autres navigateurs.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5860">CVE-2019-5860</a>

<p>Un problème d'utilisation de mémoire après libération a été découvert
dans la bibliothèque JavaScript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5861">CVE-2019-5861</a>

<p>Robin Linus a découvert une erreur de détermination de localisation du
clic.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5862">CVE-2019-5862</a>

<p>Jun Kokatsu a découvert une erreur dans l'implémentation de AppCache.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5864">CVE-2019-5864</a>

<p>Devin Grindle a découvert une erreur dans la fonction de partage de
ressources entre origines multiples pour les extensions.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5865">CVE-2019-5865</a>

<p>Ivan Fratric a découvert un moyen de contourner la fonctionnalité
« isolation des sites ».</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5867">CVE-2019-5867</a>

<p>Lucas Pinheiro a découvert un problème de lecture hors limites dans la bibliothèque JavaScript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5868">CVE-2019-5868</a>

<p>banananapenguin a découvert un problème d'utilisation de mémoire après
libération dans la bibliothèque JavaScript v8.</p></li>

</ul>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés
dans la version 76.0.3809.100-1~deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets chromium.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de chromium, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/chromium">\
https://security-tracker.debian.org/tracker/chromium</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4500.data"
# $Id: $
