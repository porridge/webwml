#use wml::debian::translation-check translation="45c7a748884ba819e8e43a8bafa9c7b0a2629b5a" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le serveur DNS BIND :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5743">CVE-2018-5743</a>

<p>Les limites de connexion étaient appliquées incorrectement.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5745">CVE-2018-5745</a>

<p>La fonctionnalité « managed-keys » était vulnérable à un déni de service
par le déclenchement d'une assertion.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-6465">CVE-2019-6465</a>

<p>Les ACL pour transferts de zone étaient appliquées incorrectement pour
les zones chargées dynamiquement (DLZ).</p></li>

</ul>

<p>Pour la distribution stable (Stretch), ces problèmes ont été corrigés
dans la version 1:9.10.3.dfsg.P4-12.3+deb9u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets bind9.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de bind9, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/bind9">\
https://security-tracker.debian.org/tracker/bind9</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4440.data"
# $Id: $
