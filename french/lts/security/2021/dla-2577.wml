#use wml::debian::translation-check translation="7e0889f4402b54a4853f5bdc426897cf7b13834d" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes ont été découverts dans python-pysaml2, une
implémentation en pur Python de la norme version 2 de SAML.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-1000433">CVE-2017-1000433</a>

<p>pysaml2 accepte n’importe quel mot de passe lors d’une exécution avec les
optimisations de Python activées. Cela permet à des attaquants de se connecter
en tant que n’importe quel utilisateur sans connaître son mot de passe.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21239">CVE-2021-21239</a>

<p>pysaml2 est sujet à une vulnérabilité de vérification incorrecte de signature
de chiffrement. Les utilisateurs de pysaml2 qui utilisent par défaut le dorsal
 CryptoBackendXmlSec1 et qui ont besoin de vérifier des documents SAML signés
sont affectés. PySAML2 ne certifie pas qu’un document SAML signé le soit
correctement. Le dorsal par défaut CryptoBackendXmlSec1 utilise le binaire
xmlsec1 pour vérifier la signature de documents SAML signés, mais par défaut
xmlsec1 accepte n’importe quel type de clé trouvée dans le document fourni.
xmlsec1 doit être configuré explicitement pour utiliser uniquement des
certificats x509 pour le processus de vérification de la signature de document
SAML.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 3.0.0-5+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets python-pysaml2.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de python-pysaml2, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/python-pysaml2">\
https://security-tracker.debian.org/tracker/python-pysaml2</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2577.data"
# $Id: $
