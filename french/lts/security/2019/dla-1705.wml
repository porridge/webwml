#use wml::debian::translation-check translation="e93ca5fbfaed58a7d5dc6aef84b3b25a6d2651b6" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans SoX (Sound eXchange),
un programme de traitement de sons.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11332">CVE-2017-11332</a>

<p>La fonction startread (wav.c) est sujette à une vulnérabilité de division par
zéro lors du traitement d’un fichier WAV avec un compte de zéro canal. Ce
défaut peut être exploité par des attaquants distants utilisant un fichier WAV
contrefait pour réaliser un déni de service (plantage d'application).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11358">CVE-2017-11358</a>

<p>La fonction read_samples (hcom.c) est sujette à une vulnérabilité de lecture
de mémoire non valable lors du traitement de fichiers HCOM avec des
dictionnaires non valables. Ce défaut peut être exploité par des attaquants
distants utilisant un fichier HCOM contrefait pour réaliser un déni de service
(plantage d'application).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11359">CVE-2017-11359</a>

<p>La fonction wavwritehdr (wav.c) est sujette à une vulnérabilité de division
par zéro lors du traitement de fichiers WAV avec un compte de canaux non valable
au-dessus de 16 bits. Ce défaut peut être exploité par des attaquants distants
utilisant un fichier WAV contrefait pour réaliser un déni de service (plantage
d'application).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15371">CVE-2017-15371</a>

<p>La fonction sox_append_comment() (formats.c) est vulnérable à une assertion
accessible lors du traitement de fichiers FLAC avec des métadonnées déclarant
plus de commentaires que présents. Ce défaut peut être exploité par des
attaquants distants utilisant des données FLAC contrefaites pour réaliser un
déni de service (plantage d'application).</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans la version 14.4.1-5+deb8u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets sox.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1705.data"
# $Id: $
