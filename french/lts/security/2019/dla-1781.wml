#use wml::debian::translation-check translation="506daaf7068a6a520320de42a0bfbd10d4cb86d3" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été trouvées dans QEMU, un émulateur rapide de
processeur.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-11806">CVE-2018-11806</a>

<p>L’implémentation de réseautage SLiRP pourrait utiliser une mauvaise taille
lors de la réallocation de ses tampons. Cela pourrait être exploité par un
utilisateur privilégié sur un invité pour provoquer un déni de service ou
éventuellement l’exécution de code arbitraire dans le système hôte.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18849">CVE-2018-18849</a>

<p>L’émulation du contrôleur hôte de bus SCSI LSI53C895A était sujette à un
accès en mémoire hors limites qui pourrait être exploité par un utilisateur
malveillant de l’invité pour planter les processus de QEMU.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20815">CVE-2018-20815</a>

<p>Un dépassement de tampon basé sur le tas a été découvert dans la fonction
load_device_tree, qui pourrait être utilisé par un utilisateur malveillant pour
éventuellement exécuter du code arbitraire avec les privilèges du processus de
QEMU.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9824">CVE-2019-9824</a>

<p>William Bowling a découvert que l’implémentation de réseautage SLiRP ne
gérait pas correctement quelques messages. Cela pourrait être utilisé pour déclencher
une fuite de mémoire à l’aide de messages contrefaits.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1:2.1+dfsg-12+deb8u11.</p>
<p>Nous vous recommandons de mettre à jour vos paquets qemu.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1781.data"
# $Id: $
