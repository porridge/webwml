#use wml::debian::translation-check translation="b2eb73ccfe48a5f3cabfe3614ab068da445fd89d" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Les responsables d’ikiwiki ont découvert que le greffon aggregate n’utilisait
pas LWPx::ParanoidAgent. Sur les sites ou le greffon aggregate est activé, des
auteurs autorisés de wiki pourrait demander à ikiwiki d’accéder à des URI
potentiellement indésirables, même si LWPx::ParanoidAgent était installé :</p>

<p>fichiers locaux à l’aide de l’URI file: ;
autres schémas d’URI pouvant être détournés des attaquants, tels que gopher: ;
hôtes déterminant les adresses IP de boucle locale (127.x.x.x) ;
hôtes déterminant les adresses IP RFC 1918 (192.168.x.x, etc.)</p>

<p>Cela pourrait être utilisé par un attaquant pour publier des informations qui
ne devraient pas être accessibles, causant un déni de service en requérant
des URI <q>tarpit</q> longs à répondre, ou causant des effets de bord
indésirables si un serveur web local implémente des requêtes GET <q>non sûres</q>.
(<a href="https://security-tracker.debian.org/tracker/CVE-2019-9187">CVE-2019-9187</a>)</p>

<p>De plus, si liblwpx-paranoidagent-perl n’est pas installé, les greffons
blogspam, openid et pinger retomberaient sur LWP, qui est vulnérable à des
attaques similaires. Cela est peu probable d’être un problème en pratique pour
le greffon blogspam car l’URL demandé est sous le contrôle de l’administrateur
du wiki, mais le greffon openid peut demander des URL contrôlées par des
utilisateurs distants non authentifiés, et le greffon pinger peut demander des
URL contrôlées pas des auteurs autorisés de wiki.</p>

<p>Cela est corrigé dans ikiwiki 3.20190228 comme suit, avec les mêmes correctifs
rétroportés dans Debian 9 dans la version 3.20170111.1 :</p>

<ul>

<li>Les schémas d’URI autres que http: et https: ne sont pas acceptés, évitant
l’accès à file:, gopher:, etc.</li>

<li>Si un mandataire est configuré dans le fichier de configuration d’ikiwiki,
il est utilisé pour toutes les requêtes sortantes http: et https:. Dans ce cas,
le mandataire est responsable du blocage de toutes requêtes indésirables,
incluant les adresses de boucle locale ou RFC 1918.</li>

<li>Si un mandataire n’est pas configuré et liblwpx-paranoidagent-perl installé,
il sera utilisé. Cela empêche les adresses IP de boucle locale et RFC 1918, et
règle un délai pour éviter un déni de service à l’aide d’URI <q>tarpit</q>.</li>

<li>Sinon, l’agent utilisateur ordinaire LWP sera utilisé. Cela permet des
requêtes d’adresses IP de boucle locale et RFC 1918, et a un comportement moins
fort d’arrêt. Nous ne traitant pas cela comme une vulnérabilité : si ce
comportement n’est pas acceptable pour votre site, veuillez être sûr que
que LWPx::ParanoidAgent soit installé ou désactivez les greffons concernés.</li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ce problème a été corrigé dans
la version 3.20141016.4+deb8u1.</p>
<p>Nous vous recommandons de mettre à jour vos paquets ikiwiki. De plus, il est
aussi recommandé que liblwpx-paranoidagent-perl soit installé, qui est dans le
champ « recommandé » d’ikiwiki.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1716.data"
# $Id: $
