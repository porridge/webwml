#use wml::debian::translation-check translation="f319a3715452bd40d0c7b0a3defaded2765b324e" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été trouvés dans QEMU, un émulateur rapide de
processeur.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-12617">CVE-2018-12617</a>

<p>La fonction qmp_guest_file_read (qga/commands-posix.c) est sujette à un
dépassement d'entier et à une insuffisance subséquente d’allocation mémoire.
Cette faiblesse peut être exploitée par des attaquants distants pour provoquer
un déni de service (plantage d'application).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16872">CVE-2018-16872</a>

<p>Les fonctions usb_mtp_get_object, usb_mtp_get_partial_object et
usb_mtp_object_readdirs (hw/usb/dev-mtp.c) sont sujettes à une attaque par lien
symbolique. Un attaquant distant peut exploiter cette vulnérabilité pour
réaliser une divulgation d'informations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-6778">CVE-2019-6778</a>

<p>La fonction tcp_emu (slirp/tcp_subr.c) est sujette à un dépassement de tampon
basé sur le tas causé par un validation insuffisante de l’espace disponible dans
le tampon sc_rcv->sb_data. Un attaquant distant peut exploiter ce défaut pour
provoquer un déni de service, ou un autre impact non précisé.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans la version 1:2.1+dfsg-12+deb8u10.</p>

<p>Nous vous recommandons de mettre à jour vos paquets qemu.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1694.data"
# $Id: $
