#use wml::debian::translation-check translation="ce41d997301872adfc27a79ea546429856226b67" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans l'hyperviseur Xen. Le
projet « Common Vulnerabilities and Exposures » (CVE) identifie les
problèmes suivants.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7092">CVE-2016-7092</a> (XSA-185)

<p>Jérémie Boutoille de Quarkslab et Shangcong Luan de Alibaba ont
découvert un défaut dans le traitement des entrées de table des pages L3
permettant à un administrateur de client PV 32 bits malveillant d'augmenter
ses droits à ceux de l'hôte.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7094">CVE-2016-7094</a> (XSA-187)

<p>Des clients HVM x86 s'exécutant avec une pagination « shadow » utilisent
un sous-ensemble de l'émulateur x86 pour gérer le client écrivant ses
propres tables des pages. Andrew Cooper de Citrix a découvert qu'il y a des
situations, qu'un client peut provoquer, qui peuvent avoir pour conséquence
le dépassement de l'espace alloué pour un état interne. Un administrateur
malveillant de client HVM peut provoquer l'échec d'une vérification de
bogue par Xen, causant un déni de service à l'hôte.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 4.1.6.lts1-2. Pour Debian 8 <q>Jessie</q>, ces problèmes ont été
corrigés dans la version 4.4.1-9+deb8u7.</p>

<p>Nous vous recommandons de mettre à jour vos paquets xen.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-614.data"
# $Id: $
