#use wml::debian::translation-check translation="ce41d997301872adfc27a79ea546429856226b67" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6313">CVE-2016-6313</a>

<p>Felix Dörre et Vladimir Klebanov de l'Institut de Technologie de
Karlsruhe ont découvert un bogue dans les fonctions de mélange du
générateur de nombres aléatoires (random number generator – RNG) de
GnuPG. Un attaquant qui obtient 4640 bits du RNG peut prévoir de façon
banale les 160 bits suivants de la sortie.</p>

<p>Une première analyse de l'impact de ce bogue de GnuPG montre que les
clés RSA existantes ne sont pas affaiblies. En ce qui concerne les clés
DSA et Elgamal, il est aussi peu probable que la clé privée puisse être
prédite à partir d'autres informations publiques.</p></li>

<li>Contournement de vérification de clé GnuPG :

<p>Des faiblesses ont été découvertes dans la validation de signature de
GnuPG que des attaquants pourraient exploiter grâce à des clés publiques
contrefaites pour l'occasion et sous certaines conditions particulières
matérielles et logicielles. Même si le problème sous-jacent ne peut pas
trouver de solution uniquement logicielle, GnuPG a été renforcé en évitant
de dépendre de caches de trousseau de clés lors de la vérification des
clés. Les attaques potentielles spécifiques ne sont plus valables avec la
correction de GnuPG</p></li>

<li>Contournement de vérification de clé GnuPG :

<p>Des chercheurs de la Vrije Universiteit d'Amsterdam et de l'Université
catholique de Louvain ont découvert une méthode d'attaque, connue sous le
nom de Flip Feng Shui, qui concerne des défauts dans GnuPG. Ils ont
découvert que dans certaines conditions particulières matérielles et
logicielles, des attaquants pourraient contourner la validation de
signature GnuPG en utilisant des clés publiques contrefaites. Même si le
problème sous-jacent ne peut pas trouver de solution uniquement logicielle,
GnuPG a été rendu plus robuste pour éviter de dépendre de caches de
trousseau de clés lors de la vérification des clés.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 1.4.12-7+deb7u8.</p>

<p>Nous vous recommandons de mettre à jour vos paquets gnupg.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>


<p>Pour Debian 6 <q>Squeeze</q>, ces problèmes ont été corrigés dans la
version 1.4.12-7+deb7u8 de gnupg</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-602.data"
# $Id: $
