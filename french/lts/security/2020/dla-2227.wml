#use wml::debian::translation-check translation="f02672c2388720133385de3ccd9708ebc48ec10c" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités ont été découvertes dans BIND, une implémentation
de serveur DNS.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8616">CVE-2020-8616</a>

<p>Il a été découvert que BIND ne limitait pas suffisamment le nombre de
recherches réalisées lors du traitement de références. Un attaquant peut
exploiter ce défaut pour provoquer un déni de service (dégradation de
performance) ou utiliser le serveur de récursion pour une attaque par réflexion
avec un facteur d’amplification élevé.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8617">CVE-2020-8617</a>

<p>Il a été découvert qu’une erreur de logique dans le code qui contrôle la
validité TSIG peut être utilisée pour déclencher un échec d’assertion,
aboutissant à un déni de service.</p>


<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1:9.9.5.dfsg-9+deb8u19.</p>

<p>Nous vous recommandons de mettre à jour vos paquets bind9.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p></li>

</ul>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2227.data"
# $Id: $
