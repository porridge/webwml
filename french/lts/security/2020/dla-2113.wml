#use wml::debian::translation-check translation="60dcc8a658796f0184981676b8d0563217570883" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<ul>

<li><p><a href="https://security-tracker.debian.org/tracker/CVE-2020-8631">CVE-2020-8631</a></p>

<p>Dans cloud-init, le mot de passe aléatoire repose sur Mersenne Twister.
Cela facilite la prédiction des mots de passe par des attaquants, parce que
rand_str dans cloudinit/util.py appelle la fonction random.choice.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8632">CVE-2020-8632</a>

<p>Dans cloud-init, rand_user_password dans cloudinit/config/cc_set_passwords.py
possède une faible valeur pwlen par défaut. Cela facilite la découverte des
mots de passe par des attaquants.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 0.7.6~bzr976-2+deb8u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets cloud-init.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2113.data"
# $Id: $
