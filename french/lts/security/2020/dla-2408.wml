#use wml::debian::translation-check translation="38c3050f908eaae9b5b818aa7e3c111b6e82bfff" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes de sécurité ont été découverts dans Thunderbird qui
peuvent permettre l'exécution de code arbitraire ou un déni de service.</p>

<p>Debian suit les versions amont de Thunderbird. Le suivi des séries 68.x
est terminé, aussi, à partir de cette mise à jour, Debian suit les
versions 78.x.</p>

<p>Les séries 78.x interrompent la prise en charge de certains modules.
Aussi, à partir de la version 78, Thunderbird prend en charge OpenPGP de
façon native. Si vous utilisez actuellement le module Enigmail pour PGP,
veuillez consulter les fichiers NEWS et README.Debian.gz inclus pour des
informations sur la façon de faire migrer vos clés.</p>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 1:78.3.1-2~deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets thunderbird.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de thunderbird, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/thunderbird">https://security-tracker.debian.org/tracker/thunderbird</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2408.data"
# $Id: $
