#use wml::debian::translation-check translation="c25132d79dab5dda5298236044fc2bd05968ef47" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Cette publication corrige une vulnérabilité de sécurité dans le code
d’analyse de l’en-tête.</p>

<p>David Dennerline, de X-Force Researchers pour la sécurité d’IBM, et Régis
Leroy ont découvert des problèmes dans la manière dont Apache gère un grand
modèle de modèles d’espaces blancs inhabituels dans les requêtes HTTP. Dans
quelques configurations, cela pourrait conduire à des vulnérabilités de
découpage de réponse ou de pollution de cache. Pour corriger ces problèmes,
cette mise à jour rend httpd d’Apache plus strict dans la façon dont les
requêtes HTTP sont acceptées.</p>

<p>Si cela cause des problèmes dans des clients non conformes, quelques
vérifications peuvent être assouplies en ajoutant la nouvelle directive
<q>HttpProtocolOptions unsafe</q> à la configuration. Plus d’informations sont
disponibles sur</p>

<p><url "https://httpd.apache.org/docs/current/mod/core.html#httpprotocoloptions"></p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 2.2.22-13+deb7u8.</p>

<p>Nous vous recommandons de mettre à jour vos paquets apache2.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-841.data"
# $Id: $
