#use wml::debian::translation-check translation="adc5cbd36ecf754028e80bbdee567a58bca03b81" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités ont été découvertes dans l’hyperviseur Xen. Le
projet « Common vulnérabilités et Exposures » (CVE) identifie les problèmes
suivants.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9932">CVE-2016-9932</a> (XSA-200)

<p>L’émulation de CMPXCHG8B permet à des utilisateurs de système d’exploitation
cliente d’HVM  d’obtenir des informations sensibles de la mémoire de pile de
l’hôte.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7995">CVE-2017-7995</a>

<p>Description :
Xen vérifie les permissions d’accès à des gammes MMIO seulement après l’accès,
permettant des lectures d’espace mémoire de périphériques PCI d’hôte .</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-8903">CVE-2017-8903</a> (XSA-213)

<p>Xen gère incorrectement les tables des pages après un appel d’hyperviseur IRET
qui peut conduire à l’exécution de code arbitraire sur le système d’exploitation
client. La vulnérabilité est seulement exposée aux clients de paravirtualisation
de 64 bits.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-8904">CVE-2017-8904</a> (XSA-214)

<p>Xen gère incorrectement la propriété <q>contains segment descriptors</q> lors
de GNTTABOP_transfer. Cela pouvait permettre aux utilisateurs de
paravirtualisation de système d’exploitation invité d’exécuter du code arbitraire
sur le système d’exploitation hôte.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-8905">CVE-2017-8905</a> (XSA-215)

<p>Xen gère incorrectement la fonction de rappel à sécurité intégrée. Cela
pourrait permettre aux utilisateurs de paravirtualisation de système
d’exploitation invité d’exécuter du code arbitraire sur le système d’exploitation
hôte.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 4.1.6.lts1-8.</p>

<p>Nous vous recommandons de mettre à jour vos paquets xen.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-964.data"
# $Id: $
