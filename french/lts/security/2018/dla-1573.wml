#use wml::debian::translation-check translation="ab3be4ee01879fd4484c795bbaa824377c218575" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le micrologiciel pour les
puces wifi BCM43xx de Broadcom qui pourraient conduire à une augmentation de
droits ou une perte de confidentialité.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-0801">CVE-2016-0801</a>

<p>L’équipe de Broadgate a découvert des défauts dans le traitement de paquets
dans le micrologiciel wifi et les pilotes propriétaires de Broadcom. Cela
pourrait conduire à l’exécution de code à distance. Cependant, cette
vulnérabilité est considérée comme n’affectant pas les pilotes utilisés dans
Debian.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-0561">CVE-2017-0561</a>

<p>Gal Beniamini de Project Zero a découvert un défaut dans l’implémentation
TDLS du micrologiciel wifi de Broadcom. Cela pourrait être exploité par un
attaquant dans le même réseau WPA2 pour exécuter du code dans le microcontrôleur
wifi.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9417">CVE-2017-9417</a> /
#869639

<p>Nitay Artenstein de Exodus Intelligence a découvert un défaut dans
l’implémentation WMM du micrologiciel wifi de Broadcom. Cela pourrait être
exploité par un attaquant proche pour exécuter du code dans le microcontrôleur
wifi.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-13077">CVE-2017-13077</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2017-13078">CVE-2017-13078</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2017-13079">CVE-2017-13079</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2017-13080">CVE-2017-13080</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2017-13081">CVE-2017-13081</a>

<p>Mathy Vanhoef du groupe de recherche imec-DistriNet de KU Leuven a découvert
plusieurs vulnérabilités dans le protocole WPA utilisé pour l’authentification
dans les réseaux sans fil, contribuant à <q>KRACK</q>.</p>

<p>Un attaquant exploitant les vulnérabilités pourrait forcer le système
vulnérable à réutiliser des clefs de session chiffrée, permettant une série
d’attaques cryptographiques à l’encontre des algorithmes de chiffrement utilisés
dans WPA1 et WPA2.</p>

<p>Ces vulnérabilités sont corrigés seulement pour certaines puces wifi de
 Broadcom et peuvent être toujours présentes dans le micrologiciel d’autres
matériels wifi.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans la
version 20161130-4~deb8u1. Cette version ajoute aussi de nouveaux micrologiciels
et paquets à utiliser avec Linux 4.9, et réajoute des
firmware-{adi,ralink} comme paquets de transition.</p>

<p>Nous vous recommandons de mettre à jour vos paquets firmware-nonfree.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1573.data"
# $Id: $
