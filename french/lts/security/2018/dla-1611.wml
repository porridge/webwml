#use wml::debian::translation-check translation="ec63203ae7c88b70ad002f6a6d89bd22297d4fb8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs problèmes de sécurité ont été corrigés dans plusieurs
démultiplexeurs et décodeurs de la bibliothèque multimédia libav.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-9317">CVE-2014-9317</a>

<p>La fonction decode_ihdr_chunk dans libavcodec/pngdec.c permettait à des
attaquants distants de provoquer un déni de service (accès au tas hors limites)
et éventuellement d’avoir un autre impact non spécifié à l'aide d'un IDAT avant un
IHDR dans un fichier PNG. Le problème a été résolu par la vérification de
l’ordre IHDR/IDAT.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-6761">CVE-2015-6761</a>

<p>La fonction update_dimensions dans libavcodec/vp8.c dans libav dépend d’un
compte coefficient-partition pendant les opérations multiprocessus. Cela permettait
à des attaquants distants de provoquer un déni de service (situation de
compétition et corruption de mémoire) ou éventuellement d’avoir un impact non
précisé à l'aide d'un fichier WebM contrefait. Ce problème a été résolu en
utilisant num_coeff_partitions dans la configuration thread/buffer. La variable
n’est pas une constante et peut conduire à des situations de compétition.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-6818">CVE-2015-6818</a>

<p>La fonction decode_ihdr_chunk dans libavcodec/pngdec.c n’imposait pas
l’unicité du bloc IHDR (c'est-à-dire, en-tête d’image) dans une image PNG. Cela
permettait à des attaquants distants de provoquer un déni de service (accès
tableau hors limites) ou éventuellement d’avoir un impact non précisé à l'aide
d'une image contrefaite avec deux ou plus de ces blocs. Cela a été corrigé en
n’autorisant qu’un seul bloc IHDR. Plusieurs blocs IHDR sont interdits dans un
PNG.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-6820">CVE-2015-6820</a>

<p>La fonction ff_sbr_apply dans libavcodec/aacsbr.c ne vérifiait pas la
correspondance d’élément syntaxique de trame AAC avant de procéder à des calculs
de reconstruction de bande spectrale (SBR). Cela permettait à des attaquants
distants de provoquer un déni de service (accès tableau hors limites) ou
éventuellement d’avoir un impact non précisé à l’aide de données AAC contrefaites.
Cela a été corrigé en vérifiant que le type d’élément correspond avant
d’appliquer SBR.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-6821">CVE-2015-6821</a>

<p>La fonction ff_mpv_common_init dans libavcodec/mpegvideo.c ne respectait pas
correctement le contexte d’encodage. Cela permettait à des attaquants distants de
provoquer un déni de service (accès non valable à un pointeur) ou éventuellement
d’avoir un impact non précisé à l’aide de données MPEG contrefaites. Le problème a
été corrigé par le nettoyage des pointeurs dans ff_mpv_common_init(). Cela
assure que des pointeurs périmés ne fuient par aucun chemin.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-6822">CVE-2015-6822</a>

<p>La fonction destroy_buffers dans libavcodec/sanm.c ne respectait pas
correctement les valeurs de hauteur et largeur dans le contexte video. Cela
permettait à des attaquants distants de provoquer un déni de service (violation de
segmentation et plantage d'application) ou éventuellement d’avoir un impact non
précisé à l’aide de données vidéo LucasArts Smush contrefaites. La solution
était de réinitialiser les tailles dans destroy_buffers() dans avcodec/sanm.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-6823">CVE-2015-6823</a>

<p>Outre ce qui est mentionné dans le fichier debian/changelog, ce problème n’a
pas encore été résolu pour libav dans Debian Jessie LTS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-6824">CVE-2015-6824</a>

<p>Outre ce qui est mentionné dans le fichier debian/changelog, ce problème n’a
pas encore été résolu pour libav dans Debian Jessie LTS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-6825">CVE-2015-6825</a>

<p>La fonction ff_frame_thread_init dans libavcodec/pthread_frame.c ne gérait
pas correctement certains défauts d’allocation mémoire. Cela permettait à des
attaquants distants de provoquer un déni de service (accès non valable à pointeur)
ou éventuellement d’avoir un impact non précisé à l'aide d'un fichier contrefait,
comme démontré avec un fichier AVI. Le nettoyage de priv_data dans
avcodec/pthread_frame.c a résolu cela et évite maintenant les pointeurs périmés
dans les cas d’erreur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-6826">CVE-2015-6826</a>

<p>La fonction ff_rv34_decode_init_thread_copy dans libavcodec/rv34.c
n’initialisait pas certains membres de structure. Cela permettait à des
attaquants distants de provoquer un déni de service (accès non valable à pointeur)
ou éventuellement d’avoir un impact non précisé à l’aide (1) de RV30 contrefait ou
(2) des données RV40 RealVideo. Ce problème a été résolu en nettoyant les
pointeurs dans ff_rv34_decode_init_thread_copy() dans avcodec/rv34.c, évitant
les pointeurs périmés.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8216">CVE-2015-8216</a>

<p>La fonction ljpeg_decode_yuv_scan dans libavcodec/mjpegdec.c dans FFmpeg
omettait certaines vérifications de hauteur et largeur. Cela permettait à des
attaquants distants de provoquer un déni de service (accès hors limites à un tableau)
ou éventuellement d’avoir un impact non précisé à l’aide de données MJPEG
contrefaites. Les problèmes ont été corrigés en ajoutant une vérification pour
indexer vers avcodec/mjpegdec.c dans ljpeg_decode_yuv_scan() avant de l’utiliser,
ce qui corrige un accès hors tableau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8217">CVE-2015-8217</a>

<p>La fonction ff_hevc_parse_sps dans libavcodec/hevc_ps.c ne validait pas
l’indicateur de format Chroma. Cela permettait à des attaquants distants de
provoquer un déni de service (accès hors limites à un tableau) ou éventuellement
d’avoir un impact non précisé à l’aide des données High Efficiency Video Coding
(HEVC) contrefaites. Une vérification de chroma_format_idc dans avcodec/hevc_ps.c
a été ajoutée pour corriger cet accès hors tableau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8363">CVE-2015-8363</a>

<p>La fonction jpeg2000_read_main_headers dans libavcodec/jpeg2000dec.c
n’imposait pas l’unicité du marqueur SIZ dans une image JPEG 2000. Cela
permettait à des attaquants distants de provoquer un déni de service (accès hors
limites de mémoire de tas) ou éventuellement d’avoir un impact non précisé
à l'aide d'une image contrefaite avec deux ou plus de ces marqueurs. Dans
avcodec/jpeg2000dec.c, une vérification pour une duplication de marqueur SIZ
à été ajoutée pour corriger cela.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8364">CVE-2015-8364</a>

<p>Un dépassement d’entier dans la fonction ff_ivi_init_planes dans
libavcodec/ivi.c permettait à des attaquants distants de provoquer un déni de
service (accès hors limites de mémoire de tas) ou éventuellement d’avoir un
impact non précisé à l’aide de dimensions d’image contrefaites dans des données
Indeo Video Interactive. Une vérification de dimensions d’image a été ajoutée
dans le code (dans avcodec/ivi.c) qui corrige ce dépassement d’entier.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8661">CVE-2015-8661</a>

<p>La fonction h264_slice_header_init dans libavcodec/h264_slice.c ne validait
pas la relation entre le nombre de processus légers et le nombre de tranches.
Cela permettait à des attaquants distants de provoquer un déni de service (accès
hors limites à un tableau) ou éventuellement d’avoir un impact non précisé
à l’aide de données H.264 contrefaites. Dans avcodec/h264_slice.c
max_contexts devient maintenant limité quand slice_context_count est initialisé. Cela
évite un accès hors tableau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8662">CVE-2015-8662</a>

<p>La fonction ff_dwt_decode dans libavcodec/jpeg2000dwt.c ne validait pas le
nombre de niveaux de décomposition avant de procéder au décodage Discrete
Wavelet Transform. Cela permettait à des attaquants distants de provoquer un
déni de service (accès hors limites à un tableau) ou éventuellement d’avoir un impact
non précisé à l’aide de données JPEG 2000 contrefaites. Dans
avcodec/jpeg2000dwt.c, une vérification de ndeclevels a été ajoutée avant
d’appeler dwt_decode*(). Cela corrige l’accès hors tableau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8663">CVE-2015-8663</a>

<p>La fonction ff_get_buffer dans libavcodec/utils.c préservait les valeurs de
hauteur et largeur après un échec. Cela permettait à des attaquants distants de
provoquer un déni de service (accès hors limites à un tableau) ou éventuellement
d’avoir un impact non précisé à l'aide d'un fichier .mov contrefait. Désormais,
les dimensions sont nettoyées dans ff_get_buffer() lors d’un échec, ce qui
corrige la cause d’un accès hors tableau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10190">CVE-2016-10190</a>

<p>Un dépassement de tampon basé sur le tas dans libavformat/http.c permettait
aux serveurs web distants d’exécuter du code arbitraire à l'aide d'une taille de
bloc négative dans une réponse HTTP. Dans libavformat/http.c les variables
relatives à la longueur ou décalage sont désormais sans signe. Ce correctif
nécessitait l’inclusion de deux autres modifications portées à partir du Git
amont de ffmpeg (commits n°3668701f et n°362c17e6).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10191">CVE-2016-10191</a>

<p>Un autre dépassement de tampon basé sur le tas dans libavformat/rtmppkt.c
permettait à des attaquants distants d’exécuter du code arbitraire en exploitant
le défaut pour vérifier la non correspondance de tailles de paquets RTMP. En
vérifiant cette non correspondance, cet accès hors tableau a été résolu.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans la version 6:11.12-1~deb8u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libav.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1611.data"
# $Id: $
