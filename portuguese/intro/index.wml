#use wml::debian::template title="Introdução ao Debian"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="93f9b3008408920efa7a3791fb329de4c58aac6c"

<a id=community></a>
<h2>O Debian é uma comunidade de pessoas</h2>
<p>Milhares de voluntários(as) ao redor do mundo trabalham juntos(as)
priorizando o Software Livre e as necessidades dos(as) usuários(as).</p>

<ul>
  <li>
    <a href="people">As pessoas:</a>
    Quem somos e o que fazemos
  </li>
  <li>
    <a href="philosophy">Filosofia:</a>
    Porque fazemos e como fazemos
  </li>
  <li>
    <a href="../devel/join/">Envolva-se:</a>
    Você pode fazer parte!
  </li>
  <li>
    <a href="help">Como você pode ajudar o Debian?</a>
  </li>
  <li>
    <a href="../social_contract">Contrato social:</a>
    Nosso comprimisso ético
  </li>
  <li>
    <a href="diversity">Declaração de diversidade</a>
  </li>
  <li>
    <a href="../code_of_conduct">Código de conduta</a>
  </li>
  <li>
    <a href="../partners/">Parceiros:</a>
    Empresas e organizações que fornecem assistência contínua ao projeto
    Debian
  </li>
  <li>
    <a href="../donations">Doações</a>
  </li>
  <li>
    <a href="../legal/">Informações legais</a>
  </li>
  <li>
    <a href="../legal/privacy">Privacidade de dados</a>
  </li>
  <li>
    <a href="../contact">Contato</a>
  </li>
</ul>

<hr>

<a id=software></a>
<h2>O Debian é um sistema operacional livre</h2>
<p>Nós iniciamos pelo Linux e adicionamos dezenas de milhares de aplicativos
  para satisfazer as necessidades dos(as) usuários(as).</p>

<ul>
  <li>
    <a href="../distrib">Download:</a>
    Mais variantes de imagens do Debian
  </li>
  <li>
  <a href="why_debian">Porque Debian</a>
  </li>
  <li>
    <a href="../support">Suporte:</a>
    Obtendo ajuda
  </li>
  <li>
    <a href="../security">Segurança:</a>
    Última atualização <br>
    <:{ $MYLIST = get_recent_list('security/1m', '1', '$(ENGLISHDIR)', 'bydate', '(2000\d+\w+|dsa-\d+)');
        @MYLIST = split(/\n/, $MYLIST);
        $MYLIST[0] =~ s#security#../security#;
        print $MYLIST[0]; }:>
  </li>
  <li>
    <a href="../distrib/packages"> Pacotes de software:</a>
    Pesquise e navegue pela longa lista dos nossos softwares
  </li>
  <li>
    <a href="../doc"> Documentação</a>
  </li>
  <li>
    <a href="https://wiki.debian.org"> Wiki do Debian</a>
  </li>
  <li>
    <a href="../Bugs"> Relatórios de bug</a>
  </li>
  <li>
    <a href="https://lists.debian.org/">
    Listas de discussão</a>
  </li>
  <li>
    <a href="../blends"> Pure Blends:</a>
    Metapacotes para necessidades específicas
  </li>
  <li>
    <a href="../devel"> Canto dos(as) desenvolvedores(as):</a>
    Informações principalmente de interesse de desenvolvedores(as) Debian
  </li>
  <li>
    <a href="../ports"> Portes/Arquiteturas:</a>
    Arquiteturas de CPU que suportamos
  </li>
  <li>
    <a href="search">Informações sobre como usar o motor de pesquisa do Debian</a>.
  </li>
  <li>
    <a href="cn">Informações sobre páginas disponíveis em múltiplos idiomas</a>.
  </li>
</ul>
