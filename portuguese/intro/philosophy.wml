#use wml::debian::template title="Our philosophy: why we do it and how we do it"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="8d625100deb403dba223e3cecbac44a283fe02ff"

# tradutores(as): parte do texto foi retirado de /intro/about.wml

<ul class="toc">
<li><a href="#what">O QUE é o Debian?</a>
<li><a href="#free">É tudo livre?</a>
<li><a href="#how">Como a comunidade trabalha como um projeto?</a>
<li><a href="#history">Como tudo isso começou?</a>
</ul>

<h2><a name="what">O QUE é o Debian?</a></h2>

<p>O <a href="$(HOME)/">Projeto Debian</a> é uma associação de
indivíduos que têm como causa comum criar um sistema operacional
<a href="free">livre</a>. O sistema operacional que criamos é chamado
<strong>Debian</strong>.

<p>Um sistema operacional é o conjunto de programas básicos e utilitários
que fazem seu computador funcionar.
No núcleo do sistema operacional está o kernel.
O kernel é o programa mais fundamental no computador e faz todas
as operações mais básicas, permitindo que você execute outros programas.

<p>Os sistemas Debian atualmente usam o kernel
<a href="https://www.kernel.org/">Linux</a> ou o kernel
<a href="https://www.freebsd.org/">FreeBSD</a>.
O Linux é um software criado inicialmente por
<a href="https://en.wikipedia.org/wiki/Linus_Torvalds">Linus Torvalds</a>
com a ajuda de milhares de programadores(as) espalhados(as) por todo o mundo.
O FreeBSD é um sistema operacional que inclui um kernel e outros softwares.

<p>No entanto, existe um esforço para fornecer o Debian para outros
kernels, principalmente para o
<a href="https://www.gnu.org/software/hurd/hurd.html">Hurd</a>.
O Hurd é um conjunto de servidores que rodam em cima de um microkernel (como o
Mach) a fim de implementar diferentes funcionalidades. O Hurd é um software
livre produzido pelo <a href="https://www.gnu.org/">projeto GNU</a>.

<p>Uma grande parte das ferramentas básicas que formam o sistema operacional
origina-se do <a href="https://www.gnu.org/">projeto GNU</a>; daí os nomes:
GNU/Linux, GNU/kFreeBSD e GNU/Hurd.
Essas ferramentas também são livres.

<p>Claro, o que as pessoas querem são os aplicativos: programas para
ajudá-las a realizar suas tarefas, desde a edição de documentos até a
administração de negócios, passando por jogos e desenvolvimento de mais
softwares. O Debian vem com mais de <packages_in_stable> <A href="$(DISTRIB)/packages">pacotes</a>
(softwares pré-compilados e empacotados em um formato amigável, o que faz com
que sejam de fácil instalação em sua máquina), um gerenciador de pacotes (APT) e
outros utilitários que tornam possível gerenciar milhares de pacotes em milhares
de computadores de maneira tão fácil quanto instalar um único aplicativo. Todos
são <a href="free">livres</a>.
</p>

<p>Funciona como uma torre: na base está o kernel. Sobre ele estão
todas as ferramentas básicas. Acima delas estão todos os outros softwares que
você executa em seu computador.
No topo da torre está o Debian &mdash; cuidadosamente organizando e ajustando o
restante de modo que tudo funcione em conjunto.

<h2>É tudo <a href="free" name="free">livre?</a></h2>

<p>Quando usamos a palavra "livre" ("free", em inglês, também significa
"grátis"), estamos nos referindo
à <strong>liberdade</strong> de software. Você pode
ler mais sobre <a href="free">o que consideramos "software livre"</a> e
<a href="https://www.gnu.org/philosophy/free-sw">o que a Free Software
Foundation diz</a> sobre o assunto.

<p>Você deve estar pensando: por que as pessoas gastam tantas horas de seu tempo
para escrever software, empacotá-lo cuidadosamente e então
<EM>distribuí-lo</EM>? As respostas são tão variadas quanto as pessoas que
contribuem.
Algumas pessoas gostam de ajudar outras pessoas.
Muitas escrevem programas para aprender mais sobre computadores.
Mais e mais pessoas estão procurando maneiras de evitar o alto preço de
software.
Uma multidão contribui como forma de agradecimento por todos os excelentes
softwares livres que receberam dos(as) outros(as).
Muitas pessoas na academia criam softwares livres a fim de que os resultados de
suas pesquisas tenham um uso mais amplo.
Empresas ajudam a manter o software livre para que possam opinar na forma como
ele é desenvolvido -- não há maneira
mais rápida de obter um novo recurso do que implementá-lo você mesmo(a)!
Claro, muitos de nós apenas acha tudo isso muito divertido!

<p>O Debian é tão comprometido com o software livre que decidimos que
seria útil que esse compromisso fosse formalizado em um documento escrito.
Assim nasceu o nosso <a href="$(HOME)/social_contract">contrato social</a>.

<p>Embora o Debian acredite no software livre, há casos em que as pessoas
querem ou precisam colocar software não livre em suas máquinas. Sempre que
possível, o Debian dará suporte a esses(as) usuários(as). Há até mesmo um número
crescente de pacotes cujo único trabalho é instalar software não livre em
sistemas Debian.

<h2><a name="how">Como a comunidade trabalha como um projeto?</a></h2>

<p>O Debian é produzido por aproximadamente mil desenvolvedores(as)
ativos(as) espalhados(as)
<a href="$(DEVEL)/developers.loc">pelo mundo</a> que são voluntários(as)
em seu tempo livre.
Poucos(as) desenvolvedores(as) se conhecem pessoalmente. A comunicação é
realizada principalmente através de e-mail (listas de discussão
em lists.debian.org) e IRC (canal #debian-br em português, ou #debian em inglês,
no irc.debian.org).
</p>

<p>O Projeto Debian tem uma <a href="organization">estrutura organizada</a>
cuidadosamente. Para mais informações sobre como o Debian é internamente,
sinta-se livre para navegar pelo
<a href="$(DEVEL)/">canto dos(as) desenvolvedores(as)</a>.</p>

<p>
Os principais documentos que explicam como a comunidade funciona são:
<ul>
<li><a href="$(DEVEL)/constitution">A constituição Debian</a></li>
<li><a href="../social_contract">O contrato social e a definição Debian de Software Livre</a></li>
<li><a href="diversity">A declaração de diversidade</a></li>
<li><a href="../code_of_conduct">O código de Conduta</a></li>
<li><a href="../doc/developers-reference/">A referência dos(as) desenvolvedores(as)</a></li>
<li><a href="../doc/debian-policy/">As políticas Debian</a></li>
</ul>

<h2><a name="history">Como tudo isso começou?</a></h2>

<p>O Debian foi criado em agosto de 1993 por Ian Murdock, como uma nova
distribuição que seria feita abertamente, no espírito do Linux e do projeto GNU.
O Debian foi concebido para ser cuidadosamente e conscientemente organizado, e
para ser mantido e suportado com cuidado similar. Tudo começou como um pequeno
grupo de hackers de Software Livre que, gradativamente, cresceu e tornou-se uma
grande e organizada comunidade de desenvolvedores(as) e usuários(as). Veja
<a href="$(DOC)/manuals/project-history/">a história detalhada</a>.

<p>Já que muitas pessoas perguntaram, Debian é pronunciado /&#712;de.bi.&#601;n/.
O nome vem do nome de seu criador, Ian Murdock, e sua esposa, Debra.
