#use wml::debian::template title="Sites-espelho mundiais do Debian" BARETITLE=true
#use wml::debian::translation-check translation="ef7400a6ea2c98aed253709dd68a428201e21d88" maintainer="Paulo Vital"

<p>O Debian é distribuído (<em>espelhado</em>) em centenas de servidores na
internet. Utilizando um servidor próximo provavelmente irá acelerar o seu
download e também reduzir a carga em nossos servidores centrais e na internet
como um todo.</p>

<p>Os espelhos do Debian podem ser primários e secundários. As definições são
as seguintes:</p>

<p class="centerblock">
  Um site de <strong>espelho primário</strong> tem uma boa largura de banda e é
  sincronizado diretamente da rede proxy de sincronização interna do Debian.
  Alguns espelhos primários têm apelidos no formato
  <code>ftp.&lt;país&gt;.debian.org</code>, sendo mais fáceis de serem
  lembrados pelos(as) usuários(as).
  Eles geralmente oferecem todas as arquiteturas.
</p>

<p class="centerblock">
  Um site de <strong>espelho secundário</strong> pode ter restrições quanto ao
  que ele espelha (devido a restrições de espaço).
  Só porque um site é secundário, não necessariamente quer dizer que ele será
  mais lento ou mais desatualizado que um site primário. Na verdade, um
  espelho secundário que oferece sua arquitetura e está mais próximo a você
  como usuário(a) e, portanto, mais rápido, é quase sempre preferível a um
  primário que esteja mais distante.
</p>

<p>Utilize o site mais perto a você para baixar o mais rápido possível,
seja ele um site primário ou secundário.
O programa
<a href="https://packages.debian.org/stable/net/netselect">\
<em>netselect</em></a> pode ser usado para determinar o site com menor
latência; utilize um programa de download como o
<a href="https://packages.debian.org/stable/web/wget">\
<em>wget</em></a> ou
<a href="https://packages.debian.org/stable/net/rsync">\
<em>rsync</em></a> para determinar o site com maior taxa de transferência.
Note que a proximidade geográfica geralmente não é o fator mais importante para
determinar qual máquina lhe servirá melhor.</p>

<p>
Se o seu sistema se move muito, você poderá ser melhor servido por um
"espelho" que é apoiado por uma
<abbr title="Content Delivery Network">CDN</abbr> global.
O projeto Debian mantém o domínio
<code>deb.debian.org</code> para este propósito e você pode usá-lo no seu
apt sources.list &mdash; consulte
<a href="http://deb.debian.org/">a página de serviços para detalhes</a>.

<p>A cópia oficial da seguinte lista pode sempre ser encontrada em:
<url "https://www.debian.org/mirror/list">.
Qualquer coisa a mais que você queira saber sobre espelhos do Debian, acesse:
<url "https://www.debian.org/mirror/">.
</p>

<h2 class="center">Espelhos primários do Debian</h2>

<table border="0" class="center">
<tr>
  <th>País</th>
  <th>Site</th>
  <th>Arquiteturas</th>
</tr>
#include "$(ENGLISHDIR)/mirror/list-primary.inc"
</table>

<h2 class="center">Espelhos secundários do Debian</h2>

<table border="0" class="center">
<tr>
  <th>Nome do host</th>
  <th>HTTP</th>
  <th>Arquiteturas</th>
</tr>
#include "$(ENGLISHDIR)/mirror/list-secondary.inc"
</table>

#include "$(ENGLISHDIR)/mirror/list-footer.inc"
