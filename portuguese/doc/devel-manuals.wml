#use wml::debian::ddp title="Manuais de desenvolvedores(as) Debian"
#include "$(ENGLISHDIR)/doc/manuals.defs"
#include "$(ENGLISHDIR)/doc/devel-manuals.defs"
#use wml::debian::translation-check translation="78f856f0bff4125a68adb590b66952d97f183227"

<document "Manual de políticas Debian" "policy">

<div class="centerblock">
<p>
  Este manual descreve os requisitos da política para a distribuição
  Debian GNU/Linux. Isto inclui a estrutura e os conteúdos do repositório
  Debian, várias questões de design do sistema operacional e os
  requerimentos técnicos que cada pacote deve atender para ser
  incluído na distribuição.

<doctable>
  <authors "Ian Jackson, Christian Schwarz, David A. Morris">
  <maintainer "The Debian Policy group">
  <status>
  pronto
  </status>
  <availability>
  <inpackage "debian-policy">
  <inddpvcs-debian-policy>
  <p><a href="https://bugs.debian.org/debian-policy">Propostas de emendas</a> para as políticas

  <p>Documentações suplementares das políticas:</p>
  <ul>
    <li><a href="packaging-manuals/fhs/fhs-3.0.html">Hierarquia padrão do sistema de arquivos</a>
    [<a href="packaging-manuals/fhs/fhs-3.0.pdf">pdf</a>]
    [<a href="packaging-manuals/fhs/fhs-3.0.txt">texto puro</a>]</li>
    <li><a href="debian-policy/upgrading-checklist.html">Lista de verificação de atualização</a></li>
    <li><a href="packaging-manuals/virtual-package-names-list.yaml">Listas de nomes de pacotes virtuais</a></li>
    <li><a href="packaging-manuals/menu-policy/">Política de menus</a>
    [<a href="packaging-manuals/menu-policy/menu-policy.txt.gz">texto puro</a></li>
    <li><a href="packaging-manuals/perl-policy/">Política Pearl</a>
    [<a href="packaging-manuals/perl-policy/perl-policy.txt.gz">texto puro</a>]</li>
    <li><a href="packaging-manuals/debconf_specification.html">especificação debconf</a></li>
    <li><a href="packaging-manuals/debian-emacs-policy">Política Emacsen</a></li>
    <li><a href="packaging-manuals/java-policy/">Política Java</a></li>
    <li><a href="packaging-manuals/python-policy/">Política Python</a></li>
    <li><a href="packaging-manuals/copyright-format/1.0/">especificação de formato de direitos autorais</a></li>
  </ul>
  </availability>
</doctable>
</div>

<hr>

<document "Referência para desenvolvedores(as) Debian" "devref">

<div class="centerblock">
<p>
  Este manual descreve procedimentos e recursos para mantenedores(as) Debian.
  Descreve como se tornar um(a) novo(a) desenvolvedor(a), o procedimento de
  upload, como lidar com o nosso sistema de acompanhamento de bugs, as listas
  de discussão, servidores de Internet, etc.

  <p>Este manual foi criado como um <em>manual de referência</em> para todos(as)
os(as) desenvolvedores(as), novatos(as) ou veteranos(as).

<doctable>
  <authors "Ian Jackson, Christian Schwarz, Lucas Nussbaum, Rapha&euml;l Hertzog, Adam Di Carlo, Andreas Barth">
  <maintainer "Lucas Nussbaum, Hideki Yamane, Holger Levsen">
  <status>
  pronto
  </status>
  <availability>
  <inpackage "developers-reference">
  <inddpvcs-developers-reference>
  </availability>
</doctable>
</div>

<hr>

<document "Guia para mantenedores(as) Debian" "debmake-doc">

<div class="centerblock">
<p>
Este tutorial descreve a construção de um pacote Debian
para usuários(as) comuns e futuros(as) desenvolvedores(as) usando o comando
<code>debmake</code>.
</p>
<p>
O manual é focado no estilo moderno de empacotamento e vem com muitos exemplos
simples.
</p>
<ul>
<li>Empacotamento script shell POSIX</li>
<li>Empacotamento de script Python3</li>
<li>C com Makefile / Autotools / CMake</li>
<li>Pacotes binários múltiplos com biblioteca compartilhada etc.</li>
</ul>
<p>
Este “Guia para mantenedores(as) Debian” pode ser considerado como o sucessor do
"guia Debian para novos(as) mantenedores(as)".
</p>

<doctable>
  <authors "Osamu Aoki">
  <maintainer "Osamu Aoki">
  <status>
  pronto
  </status>
  <availability>
  <inpackage "debmake-doc">
  <inddpvcs-debmake-doc>
  </availability>
</doctable>
</div>

<hr>

<document "Guia de novos(as) mantenedores(as) Debian" "maint-guide">

<div class="centerblock">
<p>
  Este documento tentará descrever a construção de um pacote Debian GNU/Linux
  para o(a) usuário(a) comum do Debian (e desenvolvedor(a) aspirante) em
  linguagem comum, e bem coberto com exemplos.

  <p>Diferentemente das tentativas anteriores, esta é baseada no <code>debhelper</code>
  e nas novas ferramentas disponíveis para os(as) mantenedores(as). O autor está
  fazendo todos os esforços para incorporar e unificar esforços anteriores.

<doctable>
  <authors "Josip Rodin, Osamu Aoki">
  <maintainer "Osamu Aoki">
  <status>
  está para ser substituído pelo “guia para mantenedores(as) Debian” (debmake-doc)
  </status>
  <availability>
  <inpackage "maint-guide">
  <inddpvcs-maint-guide>
  </availability>
</doctable>
</div>

<hr>

<document "Introdução ao empacotamento Debian" "packaging-tutorial">

<div class="centerblock">
<p>
Este tutorial é uma introdução ao empacotamento Debian.
Ele ensina potenciais desenvolvedores(as)
como modificar pacotes existentes, como criar seus próprios pacotes,
e como interagir com a comunidade Debian.
Além do tutorial principal,
ele inclui três sessões práticas sobre a modificação do pacote <code>grep</code>,
e o empacotamento do jogo <code>gnujump</code> e de uma biblioteca Java.
</p>

<doctable>
  <authors "Lucas Nussbaum">
  <maintainer "Lucas Nussbaum">
  <status>
  pronto
  </status>
  <availability>
  <inpackage "packaging-tutorial">
  <inddpvcs-packaging-tutorial>
  </availability>
</doctable>
</div>

<hr>

<document "Sistema de menus do Debian" "menu">

<div class="centerblock">
<p>
  Este manual descreve o sistema de menus do Debian e o pacote
  <strong>menu</strong>.

  <p>O pacote menu foi inspirado no programa install-fvwm2-menu do
  antigo pacote fvwm2. No entanto, o pacote menu tenta fornecer uma interface
  mais geral para construção de menus. Com o comando update-menus deste
  pacote, nenhum pacote precisa ser modificado novamente para cada gerenciador
  de janelas X, e fornece uma interface unificada para programas orientados a
  texto e X.

<doctable>
  <authors "Joost Witteveen, Joey Hess, Christian Schwarz">
  <maintainer "Joost Witteveen">
  <status>
  pronto
  </status>
  <availability>
  <inpackage "menu">
  <a href="packaging-manuals/menu.html/">HTML on-line</a>
  </availability>
</doctable>
</div>

<hr>

<document "Mecanismos do instalador Debian" "d-i-internals">

<div class="centerblock">
<p>
  Este documento tem como objetivo tornar o instalador Debian mais acessível a
  novos(as) desenvolvedores(as) e como local central para documentar informações
  técnicas.

<doctable>
  <authors "Frans Pop">
  <maintainer "Debian Installer team">
  <status>
  pronto
  </status>
  <availability>
  <p><a href="https://d-i.debian.org/doc/internals/">HTML on-line</a></p>
  <p><a href="https://salsa.debian.org/installer-team/debian-installer/tree/master/doc/devel/internals">Fonte XML do DocBook online</a></p>
  </availability>
</doctable>
</div>

<hr>

<document "Documentação dbconfig-common" "dbconfig-common">

<div class="centerblock">
<p>
  Este documento é destinado a mantenedores(as) de pacotes que mantêm pacotes
  que requerem um banco de dados funcional. Em vez de implementar a lógica
  necessária eles mesmos, eles podem confiar no dbconfig-common para fazer as
  perguntas certas ao instalar, atualizar, reconfigurar e desinstalar, e criar e
  preencher os banco de dados.

<doctable>
  <authors "Sean Finney and Paul Gevers">
  <maintainer "Paul Gevers">
  <status>
  pronto
  </status>
  <availability>
  <inpackage "dbconfig-common">
  <inddpvcs-dbconfig-common>
  Adicionalmente, o <a href="/doc/manuals/dbconfig-common/dbconfig-common-design.html">documento de design</a> está disponível.
  </availability>
</doctable>
</div>

<hr>

<document "dbapp-policy" "dbapp-policy">

<div class="centerblock">
<p>
  Uma proposta de política para pacotes que dependem de um banco de dados
  funcional.

<doctable>
  <authors "Sean Finney">
  <maintainer "Paul Gevers">
  <status>
  esboço
  </status>
  <availability>
  <inpackage "dbconfig-common">
  <inddpvcs-dbapp-policy>
  </availability>
</doctable>
</div>
