msgid ""
msgstr ""
"Project-Id-Version: debian-webwml\n"
"PO-Revision-Date: 2016-05-16 11:22+0300\n"
"Last-Translator: Tommi Vainikainen <thv+debian@iki.fi>\n"
"Language-Team: Finnish <debian-l10n-finnish@lists.debian.org>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/consultants/consultant.defs:6
msgid "Name:"
msgstr "Nimi:"

#: ../../english/consultants/consultant.defs:9
msgid "Company:"
msgstr "Yritys:"

#: ../../english/consultants/consultant.defs:12
msgid "Address:"
msgstr "Osoite:"

#: ../../english/consultants/consultant.defs:15
msgid "Contact:"
msgstr "Yhteystiedot:"

#: ../../english/consultants/consultant.defs:19
msgid "Phone:"
msgstr "Puhelin:"

#: ../../english/consultants/consultant.defs:22
msgid "Fax:"
msgstr "Faksi:"

#: ../../english/consultants/consultant.defs:25
msgid "URL:"
msgstr "WWW-sivut:"

#: ../../english/consultants/consultant.defs:29
msgid "or"
msgstr "tai"

#: ../../english/consultants/consultant.defs:34
msgid "Email:"
msgstr "Sähköposti:"

#: ../../english/consultants/consultant.defs:52
msgid "Rates:"
msgstr "Hinnat:"

#: ../../english/consultants/consultant.defs:55
msgid "Additional Information"
msgstr "Lisätietoja"

#: ../../english/consultants/consultant.defs:58
msgid "Willing to Relocate"
msgstr "Valmis muuttamaan"

#: ../../english/consultants/consultant.defs:61
msgid ""
"<total_consultant> Debian consultants listed in <total_country> countries "
"worldwide."
msgstr ""
"Listattu yhteensä <total_consultant> Debian-konsulttia <total_country> "
"maassa maailmanlaajuisesti."

#: ../../english/template/debian/consultant.wml:6
msgid "List of Consultants"
msgstr "Konsulttilista"

#: ../../english/template/debian/consultant.wml:9
msgid "Back to the <a href=\"./\">Debian consultants page</a>."
msgstr "Takaisin <a href=\"./\">Debian-konsulttisivulle</a>."
