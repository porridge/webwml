msgid ""
msgstr ""
"Project-Id-Version: organization.po\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2016-12-22 02:31+0900\n"
"Last-Translator: Nobuhiro Iwamatsu <iwamatsu@debian.org>\n"
"Language-Team: Japanese <debian-www@debian.or.jp>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/intro/organization.data:15
msgid "delegation mail"
msgstr "委任のメール"

#: ../../english/intro/organization.data:16
msgid "appointment mail"
msgstr "指名のメール"

#. One male delegate
#. Pronoun tags with delegate combinations
#: ../../english/intro/organization.data:18
#: ../../english/intro/organization.data:22
msgid "<void id=\"male\"/>delegate"
msgstr "委任"

#. One female delegate
#: ../../english/intro/organization.data:20
#: ../../english/intro/organization.data:23
msgid "<void id=\"female\"/>delegate"
msgstr "委任"

#: ../../english/intro/organization.data:22
#: ../../english/intro/organization.data:25
#, fuzzy
#| msgid "<void id=\"female\"/>delegate"
msgid "<void id=\"he_him\"/>he/him"
msgstr "委任"

#: ../../english/intro/organization.data:23
#: ../../english/intro/organization.data:26
#, fuzzy
#| msgid "<void id=\"female\"/>delegate"
msgid "<void id=\"she_her\"/>she/her"
msgstr "委任"

#: ../../english/intro/organization.data:24
#, fuzzy
#| msgid "<void id=\"female\"/>delegate"
msgid "<void id=\"gender_neutral\"/>delegate"
msgstr "委任"

#: ../../english/intro/organization.data:24
#: ../../english/intro/organization.data:27
#, fuzzy
#| msgid "<void id=\"female\"/>delegate"
msgid "<void id=\"they_them\"/>they/them"
msgstr "委任"

#: ../../english/intro/organization.data:30
#: ../../english/intro/organization.data:32
msgid "current"
msgstr "現職"

#: ../../english/intro/organization.data:34
#: ../../english/intro/organization.data:36
msgid "member"
msgstr "構成員"

#: ../../english/intro/organization.data:39
msgid "manager"
msgstr "マネージャー"

#: ../../english/intro/organization.data:41
msgid "SRM"
msgstr "SRM"

#: ../../english/intro/organization.data:41
msgid "Stable Release Manager"
msgstr "安定版リリース管理者"

#: ../../english/intro/organization.data:43
msgid "wizard"
msgstr "ウィザード"

#. we only use the chair tag once, for techctte, I wonder why it's here.
#: ../../english/intro/organization.data:45
#, fuzzy
msgid "chair"
msgstr "委員長"

#: ../../english/intro/organization.data:48
msgid "assistant"
msgstr "補佐"

#: ../../english/intro/organization.data:50
msgid "secretary"
msgstr "書記"

#: ../../english/intro/organization.data:52
msgid "representative"
msgstr ""

#: ../../english/intro/organization.data:54
msgid "role"
msgstr ""

#: ../../english/intro/organization.data:62
msgid ""
"In the following list, <q>current</q> is used for positions that are\n"
"transitional (elected or appointed with a certain expiration date)."
msgstr ""

#: ../../english/intro/organization.data:70
#: ../../english/intro/organization.data:82
msgid "Officers"
msgstr "執行部"

#: ../../english/intro/organization.data:71
#: ../../english/intro/organization.data:108
msgid "Distribution"
msgstr "ディストリビューション"

#: ../../english/intro/organization.data:72
#: ../../english/intro/organization.data:203
msgid "Communication and Outreach"
msgstr ""

#: ../../english/intro/organization.data:74
#: ../../english/intro/organization.data:206
msgid "Data Protection team"
msgstr ""

#: ../../english/intro/organization.data:75
#: ../../english/intro/organization.data:211
msgid "Publicity team"
msgstr "広報チーム"

#: ../../english/intro/organization.data:77
#: ../../english/intro/organization.data:284
msgid "Membership in other organizations"
msgstr ""

#: ../../english/intro/organization.data:78
#: ../../english/intro/organization.data:312
msgid "Support and Infrastructure"
msgstr "サポートと設備"

#: ../../english/intro/organization.data:85
msgid "Leader"
msgstr "リーダー"

#: ../../english/intro/organization.data:87
msgid "Technical Committee"
msgstr "技術委員会"

#: ../../english/intro/organization.data:103
msgid "Secretary"
msgstr "書記"

#: ../../english/intro/organization.data:111
msgid "Development Projects"
msgstr "開発プロジェクト"

#: ../../english/intro/organization.data:112
msgid "FTP Archives"
msgstr "FTP アーカイブ"

#: ../../english/intro/organization.data:114
msgid "FTP Masters"
msgstr "FTP マスター"

#: ../../english/intro/organization.data:120
msgid "FTP Assistants"
msgstr "FTP アシスタント"

#: ../../english/intro/organization.data:126
msgid "FTP Wizards"
msgstr "FTP ウィザード"

#: ../../english/intro/organization.data:130
msgid "Backports"
msgstr "Backports"

#: ../../english/intro/organization.data:132
msgid "Backports Team"
msgstr "Backports チーム"

#: ../../english/intro/organization.data:136
msgid "Release Management"
msgstr "リリース管理"

#: ../../english/intro/organization.data:138
msgid "Release Team"
msgstr "リリースチーム"

#: ../../english/intro/organization.data:147
msgid "Quality Assurance"
msgstr "品質保証"

#: ../../english/intro/organization.data:148
msgid "Installation System Team"
msgstr "インストールシステムチーム"

#: ../../english/intro/organization.data:149
msgid "Debian Live Team"
msgstr ""

#: ../../english/intro/organization.data:150
msgid "Release Notes"
msgstr "リリースノート"

#: ../../english/intro/organization.data:152
#, fuzzy
#| msgid "CD Images"
msgid "CD/DVD/USB Images"
msgstr "CD イメージ"

#: ../../english/intro/organization.data:154
msgid "Production"
msgstr "製品"

#: ../../english/intro/organization.data:161
msgid "Testing"
msgstr "テスト"

#: ../../english/intro/organization.data:163
msgid "Cloud Team"
msgstr ""

#: ../../english/intro/organization.data:167
msgid "Autobuilding infrastructure"
msgstr "オートビルドインフラ"

#: ../../english/intro/organization.data:169
msgid "Wanna-build team"
msgstr "ビルド要求チーム"

#: ../../english/intro/organization.data:176
msgid "Buildd administration"
msgstr "ビルドデーモン管理"

#: ../../english/intro/organization.data:193
msgid "Documentation"
msgstr "ドキュメンテーション"

#: ../../english/intro/organization.data:198
msgid "Work-Needing and Prospective Packages list"
msgstr "作業の必要なパッケージ一覧"

#: ../../english/intro/organization.data:214
msgid "Press Contact"
msgstr "広報窓口"

#: ../../english/intro/organization.data:216
msgid "Web Pages"
msgstr "ウェブページ"

#: ../../english/intro/organization.data:228
msgid "Planet Debian"
msgstr "Planet Debian"

#: ../../english/intro/organization.data:233
msgid "Outreach"
msgstr "教育"

#: ../../english/intro/organization.data:238
msgid "Debian Women Project"
msgstr "Debian Women プロジェクト"

#: ../../english/intro/organization.data:246
msgid "Community"
msgstr ""

#: ../../english/intro/organization.data:255
msgid ""
"To send a private message to all the members of the Community Team, use the "
"GPG key <a href=\"community-team-pubkey.txt"
"\">817DAE61E2FE4CA28E1B7762A89C4D0527C4C869</a>."
msgstr ""

#: ../../english/intro/organization.data:257
msgid "Events"
msgstr "イベント"

#: ../../english/intro/organization.data:264
#, fuzzy
msgid "DebConf Committee"
msgstr "技術委員会"

#: ../../english/intro/organization.data:271
msgid "Partner Program"
msgstr "パートナープログラム"

#: ../../english/intro/organization.data:275
msgid "Hardware Donations Coordination"
msgstr "ハードウェア寄付コーディネーション"

#: ../../english/intro/organization.data:290
msgid "GNOME Foundation"
msgstr ""

#: ../../english/intro/organization.data:292
msgid "Linux Professional Institute"
msgstr ""

#: ../../english/intro/organization.data:294
msgid "Linux Magazine"
msgstr ""

#: ../../english/intro/organization.data:296
msgid "Linux Standards Base"
msgstr ""

#: ../../english/intro/organization.data:298
msgid "Free Standards Group"
msgstr ""

#: ../../english/intro/organization.data:299
msgid "SchoolForge"
msgstr ""

#: ../../english/intro/organization.data:302
msgid ""
"OASIS: Organization\n"
"      for the Advancement of Structured Information Standards"
msgstr ""

#: ../../english/intro/organization.data:305
msgid ""
"OVAL: Open Vulnerability\n"
"      Assessment Language"
msgstr ""

#: ../../english/intro/organization.data:308
msgid "Open Source Initiative"
msgstr ""

#: ../../english/intro/organization.data:315
msgid "Bug Tracking System"
msgstr "バグ追跡システム (BTS)"

#: ../../english/intro/organization.data:320
msgid "Mailing Lists Administration and Mailing List Archives"
msgstr "メーリングリスト管理とメーリングリストアーカイブ"

#: ../../english/intro/organization.data:329
msgid "New Members Front Desk"
msgstr "新規メンバー受付"

#: ../../english/intro/organization.data:335
msgid "Debian Account Managers"
msgstr "Debian アカウント管理者"

#: ../../english/intro/organization.data:339
msgid ""
"To send a private message to all DAMs, use the GPG key "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."
msgstr ""
"(全) DAM に私信のメールを送る際には 57731224A9762EA155AB2A530CA8D15BB24D96F2 "
"の GPG 鍵を使ってください。"

#: ../../english/intro/organization.data:340
msgid "Keyring Maintainers (PGP and GPG)"
msgstr "公開鍵管理者 (PGP および GPG)"

#: ../../english/intro/organization.data:344
msgid "Security Team"
msgstr "セキュリティチーム"

#: ../../english/intro/organization.data:355
msgid "Policy"
msgstr "ポリシー"

#: ../../english/intro/organization.data:358
msgid "System Administration"
msgstr "システム管理"

#: ../../english/intro/organization.data:359
msgid ""
"This is the address to use when encountering problems on one of Debian's "
"machines, including password problems or you need a package installed."
msgstr ""
"こちらの電子メールアドレスは、パスワードに関する問題や、新たなパッケージイン"
"ストールの依頼など、Debianマシンのいずれかに何か問題のある際に用いる連絡先で"
"す。"

#: ../../english/intro/organization.data:369
msgid ""
"If you have hardware problems with Debian machines, please see <a href="
"\"https://db.debian.org/machines.cgi\">Debian Machines</a> page, it should "
"contain per-machine administrator information."
msgstr ""
"Debian マシンに問題があれば、<a href=\"https://db.debian.org/machines.cgi"
"\">Debian マシン</a>のページを参照してください。そこにはマシンごとの管理情報"
"があります。"

#: ../../english/intro/organization.data:370
msgid "LDAP Developer Directory Administrator"
msgstr "LDAP 開発者ディレクトリ管理者"

#: ../../english/intro/organization.data:371
msgid "Mirrors"
msgstr "ミラー"

#: ../../english/intro/organization.data:378
msgid "DNS Maintainer"
msgstr "DNS 管理"

#: ../../english/intro/organization.data:379
msgid "Package Tracking System"
msgstr "パッケージ追跡システム"

#: ../../english/intro/organization.data:381
msgid "Treasurer"
msgstr ""

#: ../../english/intro/organization.data:388
msgid ""
"<a name=\"trademark\" href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgstr "<a name=\"trademark\" href=\"m4_HOME/trademark\">商標</a>利用申請"

#: ../../english/intro/organization.data:392
#, fuzzy
msgid "Salsa administrators"
msgstr "Alioth 管理者"

#~ msgid "Anti-harassment"
#~ msgstr "嫌がらせ反対"

#~ msgid "Debian Pure Blends"
#~ msgstr "Debian Pure Blends"

#~ msgid "Individual Packages"
#~ msgstr "個々のパッケージ"

#~ msgid "Debian for children from 1 to 99"
#~ msgstr "1歳から99歳までのこどものための Debian"

#~ msgid "Debian for medical practice and research"
#~ msgstr "臨床医学と医学研究のための Debian"

#~ msgid "Debian for education"
#~ msgstr "教育のための Debian"

#~ msgid "Debian in legal offices"
#~ msgstr "法律事務所のための Debian"

#~ msgid "Debian for people with disabilities"
#~ msgstr "障害者のための Debian"

#~ msgid "Debian for science and related research"
#~ msgstr "科学研究のための Debian"

#, fuzzy
#~| msgid "Debian for education"
#~ msgid "Debian for astronomy"
#~ msgstr "教育のための Debian"

#~ msgid "Live System Team"
#~ msgstr "ライブシステムチーム"

#~ msgid "Auditor"
#~ msgstr "監事"

#~ msgid "Publicity"
#~ msgstr "広報"

#~ msgid "Debian Maintainer (DM) Keyring Maintainers"
#~ msgstr "Debian メンテナ (DM) 公開鍵管理者"

#~ msgid "DebConf chairs"
#~ msgstr "DebConf 役員"

#~ msgid "Alioth administrators"
#~ msgstr "Alioth 管理者"

#~ msgid "User support"
#~ msgstr "ユーザサポート"

#~ msgid "Embedded systems"
#~ msgstr "組み込みシステム"

#~ msgid "Firewalls"
#~ msgstr "ファイアウォール"

#~ msgid "Laptops"
#~ msgstr "ラップトップ"

#~ msgid "Special Configurations"
#~ msgstr "特殊な設定"

#~ msgid "Ports"
#~ msgstr "移植"

#~ msgid "CD Vendors Page"
#~ msgstr "CD ベンダページ"

#~ msgid "Consultants Page"
#~ msgstr "コンサルタントページ"
