msgid ""
msgstr ""
"Project-Id-Version: templates.po\n"
"PO-Revision-Date: 2016-04-24 17:00+0900\n"
"Last-Translator: Nobuhiro IMAI <nov@yo.rim.or.jp>\n"
"Language-Team: Japanese <debian-doc@debian.or.jp>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/template/debian/votebar.wml:13
msgid "Date"
msgstr "日時"

#: ../../english/template/debian/votebar.wml:16
msgid "Time Line"
msgstr "スケジュール"

#: ../../english/template/debian/votebar.wml:19
msgid "Summary"
msgstr "要約"

#: ../../english/template/debian/votebar.wml:22
msgid "Nominations"
msgstr "候補者"

#: ../../english/template/debian/votebar.wml:25
#, fuzzy
msgid "Withdrawals"
msgstr "撤回された案件"

#: ../../english/template/debian/votebar.wml:28
msgid "Debate"
msgstr "討論会"

#: ../../english/template/debian/votebar.wml:31
msgid "Platforms"
msgstr "選挙公約"

#: ../../english/template/debian/votebar.wml:34
msgid "Proposer"
msgstr "提案者"

#: ../../english/template/debian/votebar.wml:37
msgid "Proposal A Proposer"
msgstr "提案 A の提案者"

#: ../../english/template/debian/votebar.wml:40
msgid "Proposal B Proposer"
msgstr "提案 B の提案者"

#: ../../english/template/debian/votebar.wml:43
msgid "Proposal C Proposer"
msgstr "提案 C の提案者"

#: ../../english/template/debian/votebar.wml:46
msgid "Proposal D Proposer"
msgstr "提案 D の提案者"

#: ../../english/template/debian/votebar.wml:49
msgid "Proposal E Proposer"
msgstr "提案 E の提案者"

#: ../../english/template/debian/votebar.wml:52
msgid "Proposal F Proposer"
msgstr "提案 F の提案者"

#: ../../english/template/debian/votebar.wml:55
#, fuzzy
msgid "Proposal G Proposer"
msgstr "提案 A の提案者"

#: ../../english/template/debian/votebar.wml:58
#, fuzzy
msgid "Proposal H Proposer"
msgstr "提案 A の提案者"

#: ../../english/template/debian/votebar.wml:61
msgid "Seconds"
msgstr "支持者"

#: ../../english/template/debian/votebar.wml:64
msgid "Proposal A Seconds"
msgstr "提案 A の支持者"

#: ../../english/template/debian/votebar.wml:67
msgid "Proposal B Seconds"
msgstr "提案 B の支持者"

#: ../../english/template/debian/votebar.wml:70
msgid "Proposal C Seconds"
msgstr "提案 C の支持者"

#: ../../english/template/debian/votebar.wml:73
msgid "Proposal D Seconds"
msgstr "提案 D の支持者"

#: ../../english/template/debian/votebar.wml:76
msgid "Proposal E Seconds"
msgstr "提案 E の支持者"

#: ../../english/template/debian/votebar.wml:79
msgid "Proposal F Seconds"
msgstr "提案 F の支持者"

#: ../../english/template/debian/votebar.wml:82
#, fuzzy
msgid "Proposal G Seconds"
msgstr "提案 A の支持者"

#: ../../english/template/debian/votebar.wml:85
#, fuzzy
msgid "Proposal H Seconds"
msgstr "提案 A の支持者"

#: ../../english/template/debian/votebar.wml:88
msgid "Opposition"
msgstr "反対者"

#: ../../english/template/debian/votebar.wml:91
msgid "Text"
msgstr "本文"

#: ../../english/template/debian/votebar.wml:94
msgid "Proposal A"
msgstr "提案 A"

#: ../../english/template/debian/votebar.wml:97
msgid "Proposal B"
msgstr "提案 B"

#: ../../english/template/debian/votebar.wml:100
msgid "Proposal C"
msgstr "提案 C"

#: ../../english/template/debian/votebar.wml:103
msgid "Proposal D"
msgstr "提案 D"

#: ../../english/template/debian/votebar.wml:106
msgid "Proposal E"
msgstr "提案 E"

#: ../../english/template/debian/votebar.wml:109
msgid "Proposal F"
msgstr "提案 F"

#: ../../english/template/debian/votebar.wml:112
#, fuzzy
msgid "Proposal G"
msgstr "提案 A"

#: ../../english/template/debian/votebar.wml:115
#, fuzzy
msgid "Proposal H"
msgstr "提案 A"

#: ../../english/template/debian/votebar.wml:118
msgid "Choices"
msgstr "選択肢"

#: ../../english/template/debian/votebar.wml:121
msgid "Amendment Proposer"
msgstr "修正提案者"

#: ../../english/template/debian/votebar.wml:124
msgid "Amendment Seconds"
msgstr "修正支持者"

#: ../../english/template/debian/votebar.wml:127
msgid "Amendment Text"
msgstr "修正文"

#: ../../english/template/debian/votebar.wml:130
msgid "Amendment Proposer A"
msgstr "修正提案者 A"

#: ../../english/template/debian/votebar.wml:133
msgid "Amendment Seconds A"
msgstr "修正支持者 A"

#: ../../english/template/debian/votebar.wml:136
msgid "Amendment Text A"
msgstr "修正文 A"

#: ../../english/template/debian/votebar.wml:139
msgid "Amendment Proposer B"
msgstr "修正提案者 B"

#: ../../english/template/debian/votebar.wml:142
msgid "Amendment Seconds B"
msgstr "修正支持者 B"

#: ../../english/template/debian/votebar.wml:145
msgid "Amendment Text B"
msgstr "修正文 B"

#: ../../english/template/debian/votebar.wml:148
msgid "Amendment Proposer C"
msgstr "修正提案者 C"

#: ../../english/template/debian/votebar.wml:151
msgid "Amendment Seconds C"
msgstr "修正支持者 C"

#: ../../english/template/debian/votebar.wml:154
msgid "Amendment Text C"
msgstr "修正文 C"

#: ../../english/template/debian/votebar.wml:157
msgid "Amendments"
msgstr "修正"

#: ../../english/template/debian/votebar.wml:160
msgid "Proceedings"
msgstr "進行状態"

#: ../../english/template/debian/votebar.wml:163
msgid "Majority Requirement"
msgstr "多数の要請"

#: ../../english/template/debian/votebar.wml:166
msgid "Data and Statistics"
msgstr "データと統計"

#: ../../english/template/debian/votebar.wml:169
msgid "Quorum"
msgstr "定足数"

#: ../../english/template/debian/votebar.wml:172
msgid "Minimum Discussion"
msgstr "最短の討論"

#: ../../english/template/debian/votebar.wml:175
msgid "Ballot"
msgstr "投票用紙"

#: ../../english/template/debian/votebar.wml:178
msgid "Forum"
msgstr "フォーラム"

#: ../../english/template/debian/votebar.wml:181
msgid "Outcome"
msgstr "結果"

#: ../../english/template/debian/votebar.wml:185
msgid "Waiting&nbsp;for&nbsp;Sponsors"
msgstr "スポンサー募集中"

#: ../../english/template/debian/votebar.wml:188
msgid "In&nbsp;Discussion"
msgstr "議論中"

#: ../../english/template/debian/votebar.wml:191
msgid "Voting&nbsp;Open"
msgstr "投票受付中"

#: ../../english/template/debian/votebar.wml:194
msgid "Decided"
msgstr "決定事項"

#: ../../english/template/debian/votebar.wml:197
msgid "Withdrawn"
msgstr "撤回された案件"

#: ../../english/template/debian/votebar.wml:200
msgid "Other"
msgstr "その他"

#: ../../english/template/debian/votebar.wml:204
msgid "Home&nbsp;Vote&nbsp;Page"
msgstr "投票情報ホームページ"

#: ../../english/template/debian/votebar.wml:207
msgid "How&nbsp;To"
msgstr "利用方法"

#: ../../english/template/debian/votebar.wml:210
msgid "Submit&nbsp;a&nbsp;Proposal"
msgstr "提案の提出"

#: ../../english/template/debian/votebar.wml:213
msgid "Amend&nbsp;a&nbsp;Proposal"
msgstr "提案の修正"

#: ../../english/template/debian/votebar.wml:216
msgid "Follow&nbsp;a&nbsp;Proposal"
msgstr "提案への意見"

#: ../../english/template/debian/votebar.wml:219
msgid "Read&nbsp;a&nbsp;Result"
msgstr "結果の閲覧"

#: ../../english/template/debian/votebar.wml:222
msgid "Vote"
msgstr "投票"
