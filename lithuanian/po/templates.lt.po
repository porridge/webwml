#
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2009-02-28 18:36+0300\n"
"Last-Translator: Aleksandr Charkov <alcharkov@gmail.com>\n"
"Language-Team: unknown\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/search.xml.in:7
#, fuzzy
msgid "Debian website"
msgstr "Debian Saugumas"

#: ../../english/search.xml.in:9
msgid "Search the Debian website."
msgstr ""

#: ../../english/template/debian/basic.wml:19
#: ../../english/template/debian/navbar.wml:11
#, fuzzy
msgid "Debian"
msgstr "Pagalba Debian"

#: ../../english/template/debian/basic.wml:48
msgid "Debian website search"
msgstr ""

#: ../../english/template/debian/common_translation.wml:4
msgid "Yes"
msgstr "Taip"

#: ../../english/template/debian/common_translation.wml:7
msgid "No"
msgstr "Ne"

#: ../../english/template/debian/common_translation.wml:10
msgid "Debian Project"
msgstr "Debian Projektas"

#: ../../english/template/debian/common_translation.wml:13
#, fuzzy
msgid ""
"Debian is an operating system and a distribution of Free Software. It is "
"maintained and updated through the work of many users who volunteer their "
"time and effort."
msgstr ""
"Debian GNU/Linux yra nemokama GNU/Linux operacinės sistemos distribucija. Ją "
"prižiūri ir atnaujina daug vartotojų, paaukojančių savo laiką ir pastangas."

#: ../../english/template/debian/common_translation.wml:16
msgid "debian, GNU, linux, unix, open source, free, DFSG"
msgstr "debian, GNU, linux, unix, open source, atviras kodas, laisvas, DFSG"

#: ../../english/template/debian/common_translation.wml:19
msgid "Back to the <a href=\"m4_HOME/\">Debian Project homepage</a>."
msgstr "<a href=\"m4_HOME/\">Debian Projekto tinklapis</a>."

#: ../../english/template/debian/common_translation.wml:22
#: ../../english/template/debian/links.tags.wml:149
msgid "Home"
msgstr "Pradžia"

#: ../../english/template/debian/common_translation.wml:25
msgid "Skip Quicknav"
msgstr "Praleisti Quicknav"

#: ../../english/template/debian/common_translation.wml:28
msgid "About"
msgstr "Apie"

#: ../../english/template/debian/common_translation.wml:31
msgid "About Debian"
msgstr "Apie Debian"

#: ../../english/template/debian/common_translation.wml:34
msgid "Contact Us"
msgstr "Susisiekite su mumis"

#: ../../english/template/debian/common_translation.wml:37
#, fuzzy
msgid "Legal Info"
msgstr "Leidimo informacija"

#: ../../english/template/debian/common_translation.wml:40
msgid "Data Privacy"
msgstr ""

#: ../../english/template/debian/common_translation.wml:43
msgid "Donations"
msgstr "Parama"

#: ../../english/template/debian/common_translation.wml:46
msgid "Events"
msgstr "Įvykiai"

#: ../../english/template/debian/common_translation.wml:49
msgid "News"
msgstr "Naujienos"

#: ../../english/template/debian/common_translation.wml:52
msgid "Distribution"
msgstr "Distribucija"

#: ../../english/template/debian/common_translation.wml:55
msgid "Support"
msgstr "Techninė pagalba"

#: ../../english/template/debian/common_translation.wml:58
msgid "Pure Blends"
msgstr ""

#: ../../english/template/debian/common_translation.wml:61
#: ../../english/template/debian/links.tags.wml:46
msgid "Developers' Corner"
msgstr "Plėtotojų kampelis"

#: ../../english/template/debian/common_translation.wml:64
msgid "Documentation"
msgstr "Dokumentacija"

#: ../../english/template/debian/common_translation.wml:67
msgid "Security Information"
msgstr "Saugumo informacija"

#: ../../english/template/debian/common_translation.wml:70
msgid "Search"
msgstr "Paieška"

#: ../../english/template/debian/common_translation.wml:73
msgid "none"
msgstr "nėra"

#: ../../english/template/debian/common_translation.wml:76
msgid "Go"
msgstr "Pirmyn"

#: ../../english/template/debian/common_translation.wml:79
msgid "worldwide"
msgstr "Pasauliniu mastu"

#: ../../english/template/debian/common_translation.wml:82
msgid "Site map"
msgstr "Turinys"

#: ../../english/template/debian/common_translation.wml:85
msgid "Miscellaneous"
msgstr "Kita"

#: ../../english/template/debian/common_translation.wml:88
#: ../../english/template/debian/links.tags.wml:104
msgid "Getting Debian"
msgstr "Kaip gauti Debian"

#: ../../english/template/debian/common_translation.wml:91
#, fuzzy
msgid "The Debian Blog"
msgstr "Knygos apie Debian"

#: ../../english/template/debian/common_translation.wml:94
#, fuzzy
#| msgid "Debian Project News"
msgid "Debian Micronews"
msgstr "Debian Projekto Naujienos"

#: ../../english/template/debian/common_translation.wml:97
#, fuzzy
#| msgid "Debian Project"
msgid "Debian Planet"
msgstr "Debian Projektas"

#: ../../english/template/debian/common_translation.wml:100
#, fuzzy
msgid "Last Updated"
msgstr "Paskutinė redagavimo data"

#: ../../english/template/debian/ddp.wml:6
msgid ""
"Please send all comments, criticisms and suggestions about these web pages "
"to our <a href=\"mailto:debian-doc@lists.debian.org\">mailing list</a>."
msgstr ""
"Prašome siųsti visus komentarus, kritiką ir pasiūlymus apie šiuos tinklapius "
"mūsų <a href=\"mailto:debian-doc@lists.debian.org\">pašto konferencijai</a>."

#: ../../english/template/debian/fixes_link.wml:11
msgid "not needed"
msgstr "nereikalinga"

#: ../../english/template/debian/fixes_link.wml:14
msgid "not available"
msgstr "nėra"

#: ../../english/template/debian/fixes_link.wml:17
msgid "N/A"
msgstr "nepritaikoma"

#: ../../english/template/debian/fixes_link.wml:20
msgid "in release 1.1"
msgstr "1.1 versijoje"

#: ../../english/template/debian/fixes_link.wml:23
msgid "in release 1.3"
msgstr "1.3 versijoje"

#: ../../english/template/debian/fixes_link.wml:26
msgid "in release 2.0"
msgstr "2.0 versijoje"

#: ../../english/template/debian/fixes_link.wml:29
msgid "in release 2.1"
msgstr "2.1 versijoje"

#: ../../english/template/debian/fixes_link.wml:32
msgid "in release 2.2"
msgstr "2.2 versijoje"

#. TRANSLATORS: Please make clear in the translation of the following
#. item that mail sent to the debian-www list *must* be in English. Also,
#. you can add some information of your own translation mailing list
#. (i.e. debian-l10n-XXXXXX@lists.debian.org) for reporting things in
#. your language.
#: ../../english/template/debian/footer.wml:89
#, fuzzy
msgid ""
"To report a problem with the web site, please e-mail our publicly archived "
"mailing list <a href=\"mailto:debian-www@lists.debian.org\">debian-www@lists."
"debian.org</a> in English.  For other contact information, see the Debian <a "
"href=\"m4_HOME/contact\">contact page</a>. Web site source code is <a href="
"\"https://salsa.debian.org/webmaster-team/webwml\">available</a>."
msgstr ""
"Problemas, susijusias su tinklalapiu siųskite el. paštu <a href=\"mailto:"
"debian-www@lists.debian.org\">debian-www@lists.debian.org</a>. Norint gauti "
"kitą kontaktinę informaciją, žiūrėkite Debian <a href=\"m4_HOME/contact\"> "
"kontaktų puslapį</a>."

#: ../../english/template/debian/footer.wml:92
msgid "Last Modified"
msgstr "Paskutinė redagavimo data"

#: ../../english/template/debian/footer.wml:95
msgid "Last Built"
msgstr ""

#: ../../english/template/debian/footer.wml:98
msgid "Copyright"
msgstr "Autorinės teisės"

#: ../../english/template/debian/footer.wml:101
msgid "<a href=\"https://www.spi-inc.org/\">SPI</a> and others;"
msgstr ""

#: ../../english/template/debian/footer.wml:104
msgid "See <a href=\"m4_HOME/license\" rel=\"copyright\">license terms</a>"
msgstr ""
"Žiūrėkite <a href=\"m4_HOME/license\" rel=\"copyright\">licencijavimo "
"tvarką</a>"

#: ../../english/template/debian/footer.wml:107
msgid ""
"Debian is a registered <a href=\"m4_HOME/trademark\">trademark</a> of "
"Software in the Public Interest, Inc."
msgstr ""
"Debian tai registruotas Programinės Įrangos Žmonių Labui, Inc. <a href="
"\"m4_HOME/trademark\">prekybinis ženklas</a> ."

#: ../../english/template/debian/languages.wml:196
#: ../../english/template/debian/languages.wml:232
msgid "This page is also available in the following languages:"
msgstr "Šis puslapis išverstas į šias kalbas:"

#: ../../english/template/debian/languages.wml:265
msgid "How to set <a href=m4_HOME/intro/cn>the default document language</a>"
msgstr ""
"Kaip nustatyti <a href=m4_HOME/intro/cn>standartinę dokumento kalbą</a>"

#: ../../english/template/debian/languages.wml:323
msgid "Browser default"
msgstr ""

#: ../../english/template/debian/languages.wml:323
msgid "Unset the language override cookie"
msgstr ""

#: ../../english/template/debian/links.tags.wml:4
msgid "Debian International"
msgstr "Debian Pasaulyje"

#: ../../english/template/debian/links.tags.wml:7
msgid "Partners"
msgstr "Partneriai"

#: ../../english/template/debian/links.tags.wml:10
msgid "Debian Weekly News"
msgstr "Savaitinės Debian Naujienos"

#: ../../english/template/debian/links.tags.wml:13
msgid "Weekly News"
msgstr "Savaitinės naujienos"

#: ../../english/template/debian/links.tags.wml:16
msgid "Debian Project News"
msgstr "Debian Projekto Naujienos"

#: ../../english/template/debian/links.tags.wml:19
msgid "Project News"
msgstr "Projekto naujienos"

#: ../../english/template/debian/links.tags.wml:22
msgid "Release Info"
msgstr "Leidimo informacija"

#: ../../english/template/debian/links.tags.wml:25
msgid "Debian Packages"
msgstr "Debian paketai"

#: ../../english/template/debian/links.tags.wml:28
msgid "Download"
msgstr "Parsisiųsti"

#: ../../english/template/debian/links.tags.wml:31
msgid "Debian&nbsp;on&nbsp;CD"
msgstr "Debian&nbsp;kompaktiniame&nbsp;diske"

#: ../../english/template/debian/links.tags.wml:34
msgid "Debian Books"
msgstr "Knygos apie Debian"

#: ../../english/template/debian/links.tags.wml:37
#, fuzzy
msgid "Debian Wiki"
msgstr "Pagalba Debian"

#: ../../english/template/debian/links.tags.wml:40
msgid "Mailing List Archives"
msgstr "Pašto konferencijų archyvai"

#: ../../english/template/debian/links.tags.wml:43
msgid "Mailing Lists"
msgstr "Pašto konferencijos"

#: ../../english/template/debian/links.tags.wml:49
msgid "Social Contract"
msgstr "Visuomeninė sutartis"

#: ../../english/template/debian/links.tags.wml:52
msgid "Code of Conduct"
msgstr ""

#: ../../english/template/debian/links.tags.wml:55
msgid "Debian 5.0 - The universal operating system"
msgstr ""

#: ../../english/template/debian/links.tags.wml:58
msgid "Site map for Debian web pages"
msgstr "Debian interneto puslapių turinys"

#: ../../english/template/debian/links.tags.wml:61
msgid "Developer Database"
msgstr "Kūrėjų duomenų bazė"

#: ../../english/template/debian/links.tags.wml:64
#, fuzzy
msgid "Debian FAQ"
msgstr "Knygos apie Debian"

#: ../../english/template/debian/links.tags.wml:67
msgid "Debian Policy Manual"
msgstr "Debian Normų vadovas"

#: ../../english/template/debian/links.tags.wml:70
msgid "Developers' Reference"
msgstr "Programuotojų Žinynas"

#: ../../english/template/debian/links.tags.wml:73
msgid "New Maintainers' Guide"
msgstr "Naujojo prižiūrėtojo vadovas"

#: ../../english/template/debian/links.tags.wml:76
msgid "Release Critical Bugs"
msgstr "Kritinės išleidimo klaidos"

#: ../../english/template/debian/links.tags.wml:79
msgid "Lintian Reports"
msgstr "Lintian pranešimai"

#: ../../english/template/debian/links.tags.wml:83
msgid "Archives for users' mailing lists"
msgstr "Vartotojų pašto konferencijos archyvas"

#: ../../english/template/debian/links.tags.wml:86
msgid "Archives for developers' mailing lists"
msgstr "Plėtotojų pašto konferencijos archyvas"

#: ../../english/template/debian/links.tags.wml:89
msgid "Archives for i18n/l10n mailing lists"
msgstr "i18n/l10n pašto konferencijos archyvas"

#: ../../english/template/debian/links.tags.wml:92
msgid "Archives for ports' mailing lists"
msgstr "Architektūros migracijų pašto konferencijos archyvas"

#: ../../english/template/debian/links.tags.wml:95
msgid "Archives for mailing lists of the Bug tracking system"
msgstr "Klaidų stebėjimo sistemos archyvai pašto konferencijai"

#: ../../english/template/debian/links.tags.wml:98
msgid "Archives for miscellaneous mailing lists"
msgstr "Kitų pašto konferencijų archyvai"

#: ../../english/template/debian/links.tags.wml:101
msgid "Free Software"
msgstr "Laisva programinė įranga"

#: ../../english/template/debian/links.tags.wml:107
msgid "Development"
msgstr "Vystymas"

#: ../../english/template/debian/links.tags.wml:110
msgid "Help Debian"
msgstr "Pagalba Debian"

#: ../../english/template/debian/links.tags.wml:113
msgid "Bug reports"
msgstr "Klaidų pranešimai"

#: ../../english/template/debian/links.tags.wml:116
msgid "Ports/Architectures"
msgstr "Migracija/Architektūros"

#: ../../english/template/debian/links.tags.wml:119
msgid "Installation manual"
msgstr "Įdiegimo instrukcija"

#: ../../english/template/debian/links.tags.wml:122
msgid "CD vendors"
msgstr "CD pardavėjai"

#: ../../english/template/debian/links.tags.wml:125
msgid "CD/USB ISO images"
msgstr "CD/USB ISO atvaizdai"

#: ../../english/template/debian/links.tags.wml:128
msgid "Network install"
msgstr "įdiegti per tinklą"

#: ../../english/template/debian/links.tags.wml:131
msgid "Pre-installed"
msgstr "Jau įdiegta"

#: ../../english/template/debian/links.tags.wml:134
msgid "Debian-Edu project"
msgstr "Debian-Edu Projektas"

#: ../../english/template/debian/links.tags.wml:137
msgid "Alioth &ndash; Debian GForge"
msgstr "Alioth &ndash; Debian GForge"

#: ../../english/template/debian/links.tags.wml:140
msgid "Quality Assurance"
msgstr "Kokybės Užtikrinimas"

#: ../../english/template/debian/links.tags.wml:143
msgid "Package Tracking System"
msgstr "Paketo Stebėjimo Sistema"

#: ../../english/template/debian/links.tags.wml:146
msgid "Debian Developer's Packages Overview"
msgstr "Debian plėtotuojų paketų apžiūra"

#: ../../english/template/debian/navbar.wml:10
#, fuzzy
msgid "Debian Home"
msgstr "Debian Projektas"

#: ../../english/template/debian/recent_list.wml:7
msgid "No items for this year."
msgstr "Šiais metais nieko nėra."

#: ../../english/template/debian/recent_list.wml:11
msgid "proposed"
msgstr "siūloma"

#: ../../english/template/debian/recent_list.wml:15
msgid "in discussion"
msgstr "diskusijose"

#: ../../english/template/debian/recent_list.wml:19
msgid "voting open"
msgstr "balsavimas atidarytas"

#: ../../english/template/debian/recent_list.wml:23
msgid "finished"
msgstr "pabaigta"

#: ../../english/template/debian/recent_list.wml:26
msgid "withdrawn"
msgstr "atšaukta"

#: ../../english/template/debian/recent_list.wml:30
msgid "Future events"
msgstr "Ateityje"

#: ../../english/template/debian/recent_list.wml:33
msgid "Past events"
msgstr "Praeityje"

#: ../../english/template/debian/recent_list.wml:37
msgid "(new revision)"
msgstr "(nauja revizija)"

#: ../../english/template/debian/recent_list.wml:329
msgid "Report"
msgstr "Pranešimas"

#: ../../english/template/debian/redirect.wml:6
msgid "Page redirected to <newpage/>"
msgstr ""

#: ../../english/template/debian/redirect.wml:12
msgid ""
"This page has been renamed to <url <newpage/>>, please update your links."
msgstr ""

#. given a manual name and an architecture, join them
#. if you need to reorder the two, use "%2$s ... %1$s", cf. printf(3)
#: ../../english/template/debian/release.wml:7
msgid "<void id=\"doc_for_arch\" />%s for %s"
msgstr "<void id=\"doc_for_arch\" />%s for %s"

#: ../../english/template/debian/translation-check.wml:37
msgid ""
"<em>Note:</em> The <a href=\"$link\">original document</a> is newer than "
"this translation."
msgstr ""
"<em>Pastaba:</em> <a href=\"$link\">Originalus dokumentas</a> yra naujesnis "
"vertimas nei šis."

#: ../../english/template/debian/translation-check.wml:43
msgid ""
"Warning! This translation is too out of date, please see the <a href=\"$link"
"\">original</a>."
msgstr ""
"Dėmesio! Šis vertimas yra pasenęs, prašome žiūrėti <a href=\"$link"
"\">originalą</a>."

#: ../../english/template/debian/translation-check.wml:49
msgid ""
"<em>Note:</em> The original document of this translation no longer exists."
msgstr "<em>Pastaba:</em> Šio vertimo originalus dokumentas nebeegzistuoja."

#: ../../english/template/debian/translation-check.wml:56
msgid "Wrong translation version!"
msgstr ""

#: ../../english/template/debian/url.wml:4
msgid "URL"
msgstr "URL"

#: ../../english/template/debian/users.wml:12
msgid "Back to the <a href=\"../\">Who's using Debian? page</a>."
msgstr "Grįžti į <a href=\"./\">Kas naudoja Debian?</a>."

#~ msgid "More information"
#~ msgstr "Daugiau informacijos"

#~ msgid "Upcoming Attractions"
#~ msgstr "AteinanÄios atrakcijos"

#~ msgid "link may no longer be valid"
#~ msgstr "nuoroda greiÄiausiai nebe egzistuoja"

#~ msgid "When"
#~ msgstr "Kada"

#~ msgid "Where"
#~ msgstr "Kur"

#~ msgid "More Info"
#~ msgstr "Daugiau informacijos"

#~ msgid "Debian Involvement"
#~ msgstr "Debian dalyvavimas"

#~ msgid "Main Coordinator"
#~ msgstr "Pagrindinis koordinatorius"

#~ msgid "<th>Project</th><th>Coordinator</th>"
#~ msgstr "<th>Projekto</th><th>koordinatorius</th>"

#~ msgid "Related Links"
#~ msgstr "Susijusios nuorodos"

#~ msgid "Latest News"
#~ msgstr "PaskutinÄs naujienos"

#, fuzzy
#~ msgid ""
#~ "Back to: other <a href=\"./\">Debian news</a> || <a href=\"m4_HOME/"
#~ "\">Debian Project homepage</a>."
#~ msgstr "<a href=\"m4_HOME/\">Debian Projekto tinklapis</a>."

#, fuzzy
#~ msgid ""
#~ "To receive this newsletter bi-weekly in your mailbox, <a href=\"http://"
#~ "lists.debian.org/debian-news/\">subscribe to the debian-news mailing "
#~ "list</a>."
#~ msgstr ""
#~ "NorÄdami kas savaitÄ gauti naujienas, <a href=\"m4_HOME/MailingLists/"
#~ "subscribe#debian-news\">uÅ¾siregistruokite</a> debian-news paÅ”to "
#~ "konferencijoje. "

#~ msgid "<a href=\"../../\">Back issues</a> of this newsletter are available."
#~ msgstr "<a href=\"../../\">AnkstesnÄs naujienos</a>."

#, fuzzy
#~ msgid ""
#~ "<void id=\"singular\" />Debian Project News is edited by <a href=\"mailto:"
#~ "debian-publicity@lists.debian.org\">%s</a>."
#~ msgstr ""
#~ "Debian SavaitinÄs Naujienos redaguojamos <a href=\"mailto:dwn@debian.org"
#~ "\">Joey Hess</a>."

#, fuzzy
#~ msgid ""
#~ "<void id=\"plural\" />Debian Project News is edited by <a href=\"mailto:"
#~ "debian-publicity@lists.debian.org\">%s</a>."
#~ msgstr ""
#~ "Debian SavaitinÄs Naujienos redaguojamos <a href=\"mailto:dwn@debian.org"
#~ "\">Joey Hess</a>."

#, fuzzy
#~ msgid ""
#~ "<void id=\"singular\" />This issue of Debian Project News was edited by "
#~ "<a href=\"mailto:debian-publicity@lists.debian.org\">%s</a>."
#~ msgstr ""
#~ "Debian SavaitinÄs Naujienos redaguojamos <a href=\"mailto:dwn@debian.org"
#~ "\">Joey Hess</a>."

#, fuzzy
#~ msgid ""
#~ "<void id=\"plural\" />This issue of Debian Project News was edited by <a "
#~ "href=\"mailto:debian-publicity@lists.debian.org\">%s</a>."
#~ msgstr ""
#~ "Debian SavaitinÄs Naujienos redaguojamos <a href=\"mailto:dwn@debian.org"
#~ "\">Joey Hess</a>."

#, fuzzy
#~ msgid "Back to the <a href=\"./\">Debian speakers page</a>."
#~ msgstr "GrÄÆÅ”ti ÄÆ <a href=\"./\">Debian konsultantÅ³ puslapÄÆ</a>."

#, fuzzy
#~ msgid ""
#~ "To receive this newsletter weekly in your mailbox, <a href=\"http://lists."
#~ "debian.org/debian-news/\">subscribe to the debian-news mailing list</a>."
#~ msgstr ""
#~ "NorÄdami kas savaitÄ gauti naujienas, <a href=\"m4_HOME/MailingLists/"
#~ "subscribe#debian-news\">uÅ¾siregistruokite</a> debian-news paÅ”to "
#~ "konferencijoje. "

#, fuzzy
#~ msgid ""
#~ "<void id=\"singular\" />Debian Weekly News is edited by <a href=\"mailto:"
#~ "dwn@debian.org\">%s</a>."
#~ msgstr ""
#~ "Debian SavaitinÄs Naujienos redaguojamos <a href=\"mailto:dwn@debian.org"
#~ "\">Joey Hess</a>."

#, fuzzy
#~ msgid ""
#~ "<void id=\"plural\" />Debian Weekly News is edited by <a href=\"mailto:"
#~ "dwn@debian.org\">%s</a>."
#~ msgstr ""
#~ "Debian SavaitinÄs Naujienos redaguojamos <a href=\"mailto:dwn@debian.org"
#~ "\">Joey Hess</a>."

#, fuzzy
#~ msgid ""
#~ "<void id=\"singular\" />This issue of Debian Weekly News was edited by <a "
#~ "href=\"mailto:dwn@debian.org\">%s</a>."
#~ msgstr ""
#~ "Debian SavaitinÄs Naujienos redaguojamos <a href=\"mailto:dwn@debian.org"
#~ "\">Joey Hess</a>."

#, fuzzy
#~ msgid ""
#~ "<void id=\"plural\" />This issue of Debian Weekly News was edited by <a "
#~ "href=\"mailto:dwn@debian.org\">%s</a>."
#~ msgstr ""
#~ "Debian SavaitinÄs Naujienos redaguojamos <a href=\"mailto:dwn@debian.org"
#~ "\">Joey Hess</a>."

#, fuzzy
#~ msgid "No requests for adoption"
#~ msgstr "Daugiau informacijos"

#, fuzzy
#~ msgid "No Requested packages"
#~ msgstr "Paliesti paketai"

#, fuzzy
#~ msgid "<void id=\"dc_pik\" />Pseudo Image Kit"
#~ msgstr "kita"

#, fuzzy
#~ msgid "License Information"
#~ msgstr "Daugiau informacijos"

#, fuzzy
#~ msgid "Discussion"
#~ msgstr "Vyksta&nbsp;diskusijos"

#, fuzzy
#~ msgid "non-free"
#~ msgstr "NÄra"

#~ msgid "%0 (dead link)"
#~ msgstr "%0 (nuoroda neegzistuoja)"

#~ msgid "buy pre-made images"
#~ msgstr "nusipirkti paruoÅ”tas cd kopijas"

#~ msgid "download with pik"
#~ msgstr "parsisiÅ³sti su pik"

#~ msgid "pik"
#~ msgstr "P.I.K"

#~ msgid ""
#~ "See the Debian <a href=\"m4_HOME/contact\">contact page</a> for "
#~ "information on contacting us."
#~ msgstr ""
#~ "NorÄdami su mumis susisiekti aplankykite <a href=\"m4_HOME/contact"
#~ "\">kontaktÅ³ puslapÄÆ</a>."

#~ msgid "discussed"
#~ msgstr "aptariama"

#~ msgid "Debian Security Advisory"
#~ msgstr "Debian saugumo patarimas"

#~ msgid "Debian Security Advisories"
#~ msgstr "Debian saugumo patarimai"

#~ msgid "Vulnerable"
#~ msgstr "PaÅ¾eidÅ¾iama"

#~ msgid "Fixed in"
#~ msgstr "Pataisyta"

#~ msgid "Source:"
#~ msgstr "IÅ eities tekstai"

#~ msgid "Architecture-independent component:"
#~ msgstr "Komponenetas nepriklausantis nuo architektÅ«ros"

#, fuzzy
#~ msgid ""
#~ "MD5 checksums of the listed files are available in the <a href=\"<get-var "
#~ "url />\">original advisory</a>."
#~ msgstr ""
#~ "PateiktÅ³ bylÅ³ kontrolinÄs sumos yra <a href=\"%attributes"
#~ "\">originaliame praneÅ”ime</a>."

#~ msgid "No other external database security references currently available."
#~ msgstr ""
#~ "JokiÅ³ iÅ”oriniÅ³ saugumo duomenÅ³ baziÅ³ Å”altiniÅ³ Å”iuo metÅ³ nÄra"

#~ msgid "CERT's vulnerabilities, advisories and incident notes:"
#~ msgstr "CERT paÅ¾eidimai, patarimai ir atsitikimÅ³ pasakojimai:"

#~ msgid "In the Bugtraq database (at SecurityFocus):"
#~ msgstr "Bugtraq duomenÅ³ bazÄje (pas SecurityFocus):"

#~ msgid "In Mitre's CVE dictionary:"
#~ msgstr "Mitre CVE Å¾odyne:"

#~ msgid "Security database references"
#~ msgstr "Saugumo duomenÅ³ bazÄs sÄsajos"

#~ msgid ""
#~ "Debian Weekly News is edited by <a href=\"mailto:dwn@debian.org\">Joe "
#~ "'Zonker' Brockmeier, Jean-Christophe Helary and Tollef Fog Heen</a>."
#~ msgstr ""
#~ "Debian Savaitines Naujienas redaguoja <a href=\"mailto:dwn@debian.org"
#~ "\">Joe 'Zonker' Brockmeier, Jean-Christophe Helary ir Tollef Fog Heen</a>."

#~ msgid ""
#~ "Debian Weekly News is edited by <a href=\"mailto:dwn@debian.org\">Joe "
#~ "'Zonker' Brockmeier and Martin 'Joey' Schulze</a>."
#~ msgstr ""
#~ "Debian Savaitines Naujienas redaguoja <a href=\"mailto:dwn@debian.org"
#~ "\">Joe 'Zonker' Brockmeier ir Martin 'Joey' Schulze</a>."

#, fuzzy
#~ msgid ""
#~ "Debian Weekly News is edited by <a href=\"mailto:dwn@debian.org"
#~ "\">Yooseong Yang and Martin 'Joey' Schulze</a>."
#~ msgstr ""
#~ "Debian Savaitines Naujienas redaguoja <a href=\"mailto:dwn@debian.org"
#~ "\">Joe 'Zonker' Brockmeier ir Martin 'Joey' Schulze</a>."

#, fuzzy
#~ msgid ""
#~ "Debian Weekly News is edited by <a href=\"mailto:dwn@debian.org\">Martin "
#~ "'Joey' Schulze</a>."
#~ msgstr ""
#~ "Debian SavaitinÄs Naujienos redaguojamos <a href=\"mailto:dwn@debian.org"
#~ "\">Joey Hess</a>."

#~ msgid "Vote"
#~ msgstr "Balsuoti"

#~ msgid "Read&nbsp;a&nbsp;Result"
#~ msgstr "Skaityti&nbsp;rezultatÄ"

#~ msgid "Follow&nbsp;a&nbsp;Proposal"
#~ msgstr "Sekti&nbsp;siÅ«lymÄ"

#~ msgid "Amend&nbsp;a&nbsp;Proposal"
#~ msgstr "Keisti&nbsp;siÅ«lymÄ"

#~ msgid "Submit&nbsp;a&nbsp;Proposal"
#~ msgstr "Pateikti&nbsp;siÅ«lymÄ"

#~ msgid "How&nbsp;To"
#~ msgstr "Kaip"

#~ msgid "Home&nbsp;Vote&nbsp;Page"
#~ msgstr "Pradinis&nbsp;balsavimo&nbsp;puslapis"

#~ msgid "Other"
#~ msgstr "Kita"

#~ msgid "Withdrawn"
#~ msgstr "AtÅ”aukta"

#~ msgid "Decided"
#~ msgstr "NusprÄsta"

#~ msgid "Voting&nbsp;Open"
#~ msgstr "Atviras&nbsp;balsavimas"

#~ msgid "In&nbsp;Discussion"
#~ msgstr "Vyksta&nbsp;diskusijos"

#~ msgid "Waiting&nbsp;for&nbsp;Sponsors"
#~ msgstr "Laukia&nbsp;sponsoriÅ³"

#, fuzzy
#~ msgid "Minimum Discussion"
#~ msgstr "Vyksta&nbsp;diskusijos"

#, fuzzy
#~ msgid "Proposer"
#~ msgstr "siÅ«loma"

#, fuzzy
#~ msgid "Debate"
#~ msgstr "Debian CD komanda"

#, fuzzy
#~ msgid "Nominations"
#~ msgstr "Parama"

#~ msgid "<void id=\"misc-bottom\" />misc"
#~ msgstr "kita"

#~ msgid "net_install"
#~ msgstr "ÄÆdiegimas per tinklÄ"

#~ msgid "buy"
#~ msgstr "pirk"

#~ msgid "http_ftp"
#~ msgstr "http/ftp"

#~ msgid "<void id=\"faq-bottom\" />faq"
#~ msgstr "DUK"

#~ msgid "debian_on_cd"
#~ msgstr "debian kompakte"

#~ msgid "Debian CD team"
#~ msgstr "Debian CD komanda"

#, fuzzy
#~ msgid "<void id=\"dc_rsyncmirrors\" />Rsync Mirrors"
#~ msgstr "kita"

#, fuzzy
#~ msgid "<void id=\"dc_mirroring\" />Mirroring"
#~ msgstr "kita"

#, fuzzy
#~ msgid "<void id=\"dc_artwork\" />Artwork"
#~ msgstr "DUK"

#, fuzzy
#~ msgid "<void id=\"dc_misc\" />Misc"
#~ msgstr "kita"

#, fuzzy
#~ msgid "<void id=\"dc_download\" />Download"
#~ msgstr "DUK"

#, fuzzy
#~ msgid "Network Install"
#~ msgstr "ÄÆdiegti per tinklÄ"

#, fuzzy
#~ msgid "Download via HTTP/FTP"
#~ msgstr "parsisiÅ³sti http/ftp protokolais"

#, fuzzy
#~ msgid "Download with Jigdo"
#~ msgstr "parsisiÅ³sti su jigdo"

#~ msgid "Back to the <a href=\"./\">Debian consultants page</a>."
#~ msgstr "GrÄÆÅ”ti ÄÆ <a href=\"./\">Debian konsultantÅ³ puslapÄÆ</a>."

#~ msgid "List of Consultants"
#~ msgstr "KonsultantÅ³ sÄraÅ”as"

#~ msgid "Rating:"
#~ msgstr "Reitingas:"

#~ msgid "Nobody"
#~ msgstr "Niekieno"

#~ msgid "Taken by:"
#~ msgstr "Perimta:"

#~ msgid "More information:"
#~ msgstr "Daugiau informacijos:"

#~ msgid "Select a server near you"
#~ msgstr "Pasirinkite tarnybinę stotį, esančią arčiausiai jūsų"

#~ msgid "Visit the site sponsor"
#~ msgstr "Aplankykite puslapio rėmėją"
