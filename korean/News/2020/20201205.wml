#use wml::debian::translation-check translation="5c427e44dc4a1503d5262b5edfba60b490ea0ab1" maintainer="Sebul" mindelta="-1"
<define-tag pagetitle>데비안 10 업데이트: 10.7 릴리스</define-tag>
<define-tag release_date>2020-12-05</define-tag>
#use wml::debian::news

<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.7</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>데비안 프로젝트는 안정 배포판 데비안 <release> (코드명 <q><codename></q>)의 7번째 업데이트를 알리게 되어 기쁩니다. 
이 포인트 릴리스는 심각한 문제 조치 및 보안 이슈 수정을 주로 포함합니다.
보안 권고는 이미 개별적으로 알렸으며 가능한 곳에서 참조됩니다.
</p>

<p>포인트 릴리스는 데비안 <release> 새 버전을 만드는 것이 아니며 포함된 일부 패키지만 업데이트함을 주의하세요.
옛 <q><codename></q> 미디어를 버릴 필요 없습니다. 설치 후, 패키지는 최신 데이안 미러에서 현재 버전으로 업그레이드 할 수 있습니다.
</p>

<p>security.debian.org의 업데이트를 자주 설치하는 사람은 패키지를 많이 업데이트하지 않아도 되며,
해당 업데이트는 대부분 포인트 릴리스에 들어 있습니다.</p>

<p>새 설치 위치는 정규 위치에 곧 공개될 겁니다.</p>

<p>패키지 관리 시스템이 수많은 데비안 HTTP 미러 중 하나를 가리키게 해서 기존 설치를 이 개정판으로 업그레이드할 수 있습니다.
포괄적인 미러 서버 목록은 아래에 있습니다:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>여러가지 버그 수정</h2>

<p>이 안정 업데이트는 몇 중요한 수정을 아래 패키지에 추가합니다:</p>

<table border=0>
<tr><th>패키지</th>               <th>이유</th></tr>
<correction base-files "Update for the point release">
<correction choose-mirror "Update mirror list">
<correction cups "Fix 'printer-alert' invalid free">
<correction dav4tbsync "New upstream release, compatible with newer Thunderbird versions">
<correction debian-installer "Use 4.19.0-13 Linux kernel ABI; add grub2 to Built-Using">
<correction debian-installer-netboot-images "Rebuild against proposed-updates">
<correction distro-info-data "Add Ubuntu 21.04, Hirsute Hippo">
<correction dpdk "New upstream stable release; fix remote code execution issue [CVE-2020-14374], TOCTOU issues [CVE-2020-14375], buffer overflow [CVE-2020-14376], buffer over read [CVE-2020-14377] and integer underflow [CVE-2020-14377]; fix armhf build with NEON">
<correction eas4tbsync "New upstream release, compatible with newer Thunderbird versions">
<correction edk2 "Fix integer overflow in DxeImageVerificationHandler [CVE-2019-14562]">
<correction efivar "Add support for nvme-fabrics and nvme-subsystem devices; fix uninitialized variable in parse_acpi_root, avoiding possible segfault">
<correction enigmail "Introduce migration assistant to Thunderbird's built-in GPG support">
<correction espeak "Fix using espeak with mbrola-fr4 when mbrola-fr1 is not installed">
<correction fastd "Fix memory leak when receiving too many invalid packets [CVE-2020-27638]">
<correction fish "Ensure TTY options are restored on exit">
<correction freecol "Fix XML External Entity vulnerability [CVE-2018-1000825]">
<correction gajim-omemo "Use 12-byte IV, for better compatibility with iOS clients">
<correction glances "Listen only on localhost by default">
<correction iptables-persistent "Don't force-load kernel modules; improve rule flushing logic">
<correction lacme "Use upstream certificate chain instead of an hardcoded one, easing support for new Let's Encrypt root and intermediate certificates">
<correction libdatetime-timezone-perl "Update included data to tzdata 2020d">
<correction libimobiledevice "Add partial support for iOS 14">
<correction libjpeg-turbo "Fix denial of service [CVE-2018-1152], buffer over read [CVE-2018-14498], possible remote code execution [CVE-2019-2201], buffer over read [CVE-2020-13790]">
<correction libxml2 "Fix denial of service [CVE-2017-18258], NULL pointer dereference [CVE-2018-14404], infinite loop [CVE-2018-14567], memory leak [CVE-2019-19956 CVE-2019-20388], infinite loop [CVE-2020-7595]">
<correction linux "New upstream stable release">
<correction linux-latest "Update for 4.19.0-13 kernel ABI">
<correction linux-signed-amd64 "New upstream stable release">
<correction linux-signed-arm64 "New upstream stable release">
<correction linux-signed-i386 "New upstream stable release">
<correction lmod "Change architecture to <q>any</q> - required due to LUA_PATH and LUA_CPATH being determined at build time">
<correction mariadb-10.3 "New upstream stable release; security fixes [CVE-2020-14765 CVE-2020-14776 CVE-2020-14789 CVE-2020-14812 CVE-2020-28912]">
<correction mutt "Ensure IMAP connection is closed after a connection error [CVE-2020-28896]">
<correction neomutt "Ensure IMAP connection is closed after a connection error [CVE-2020-28896]">
<correction node-object-path "Fix prototype pollution in set() [CVE-2020-15256]">
<correction node-pathval "Fix prototype pollution [CVE-2020-7751]">
<correction okular "Fix code execution via action link [CVE-2020-9359]">
<correction openjdk-11 "New upstream release; fix JVM crash">
<correction partman-auto "Increase /boot sizes in most recipes to between 512 and 768M, to better handle kernel ABI changes and larger initramfses; cap RAM size as used for swap partition calculations, resolving issues on machines with more RAM than disk space">
<correction pcaudiolib "Cap cancellation latency to 10ms">
<correction plinth "Apache: Disable mod_status [CVE-2020-25073]">
<correction puma "Fix HTTP injection and HTTP smuggling issues [CVE-2020-5247 CVE-2020-5249 CVE-2020-11076 CVE-2020-11077]">
<correction ros-ros-comm "Fix integer overflow [CVE-2020-16124]">
<correction ruby2.5 "Fix potential HTTP request smuggling vulnerability in WEBrick [CVE-2020-25613]">
<correction sleuthkit "Fix stack buffer overflow in yaffsfs_istat [CVE-2020-10232]">
<correction sqlite3 "Fix division by zero [CVE-2019-16168], NULL pointer dereference [CVE-2019-19923], mishandling of NULL pathname during an update of a ZIP archive [CVE-2019-19925], mishandling of embedded NULs in filenames [CVE-2019-19959], possible crash (unwinding WITH stack) [CVE-2019-20218], integer overflow [CVE-2020-13434], segmentation fault [CVE-2020-13435], use-after-free issue [CVE-2020-13630], NULL pointer dereference [CVE-2020-13632], heap overflow [CVE-2020-15358]">
<correction systemd "Basic/cap-list: parse/print numerical capabilities; recognise new capabilities from Linux kernel 5.8; networkd: do not generate MAC for bridge device">
<correction tbsync "New upstream release, compatible with newer Thunderbird versions">
<correction tcpdump "Fix untrusted input issue in the PPP printer [CVE-2020-8037]">
<correction tigervnc "Properly store certificate exceptions in native and java VNC viewer [CVE-2020-26117]">
<correction tor "New upstream stable release; multiple security, usability, portability, and reliability fixes">
<correction transmission "Fix memory leak">
<correction tzdata "New upstream release">
<correction ublock-origin "New upstream version; split plugin to browser-specific packages">
<correction vips "Fix use of uninitialised variable [CVE-2020-20739]">
</table>


<h2>보안 업데이트</h2>


<p>이 리비전은 아래 보안 업데이트를 안정 릴리스에 추가합니다.
보안 팀은 각 업데이트의 권고를 이미 릴리스 했습니다:</p>

<table border=0>
<tr><th>권고 ID</th>  <th>패키지</th></tr>
<dsa 2020 4766 rails>
<dsa 2020 4767 mediawiki>
<dsa 2020 4768 firefox-esr>
<dsa 2020 4769 xen>
<dsa 2020 4770 thunderbird>
<dsa 2020 4771 spice>
<dsa 2020 4772 httpcomponents-client>
<dsa 2020 4773 yaws>
<dsa 2020 4774 linux-latest>
<dsa 2020 4774 linux-signed-amd64>
<dsa 2020 4774 linux-signed-arm64>
<dsa 2020 4774 linux-signed-i386>
<dsa 2020 4774 linux>
<dsa 2020 4775 python-flask-cors>
<dsa 2020 4776 mariadb-10.3>
<dsa 2020 4777 freetype>
<dsa 2020 4778 firefox-esr>
<dsa 2020 4779 openjdk-11>
<dsa 2020 4780 thunderbird>
<dsa 2020 4781 blueman>
<dsa 2020 4782 openldap>
<dsa 2020 4783 sddm>
<dsa 2020 4784 wordpress>
<dsa 2020 4785 raptor2>
<dsa 2020 4786 libexif>
<dsa 2020 4787 moin>
<dsa 2020 4788 firefox-esr>
<dsa 2020 4789 codemirror-js>
<dsa 2020 4790 thunderbird>
<dsa 2020 4791 pacemaker>
<dsa 2020 4792 openldap>
<dsa 2020 4793 firefox-esr>
<dsa 2020 4794 mupdf>
<dsa 2020 4795 krb5>
<dsa 2020 4796 thunderbird>
<dsa 2020 4798 spip>
<dsa 2020 4799 x11vnc>
<dsa 2020 4800 libproxy>
</table>


<h2>삭제된 패키지</h2>

<p>다음 패키지는 우리 통제 범위를 넘는 환경으로 삭제되었습니다:</p>

<table border=0>
<tr><th>패키지</th>               <th>이유</th></tr>
<correction freshplayerplugin "Unsupported by browsers; discontinued upstream">
<correction nostalgy "Incompatible with newer Thunderbird versions">
<correction sieve-extension "Incompatible with newer Thunderbird versions">

</table>

<h2>데비안 설치관리자</h2>
<p>The installer has been updated to include the fixes incorporated
into stable by the point release.</p>

<h2>URLs</h2>

<p>The complete lists of packages that have changed with this revision:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>현재 안정 배포:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Proposed updates to the stable distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>안정 배포 정보 (release notes, errata etc.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>보안 알림 및 정보:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>데비안은</h2>

<p>The Debian Project is an association of Free Software developers who
volunteer their time and effort in order to produce the completely
free operating system Debian.</p>

<h2>연락 정보</h2>

<p>For further information, please visit the Debian web pages at
<a href="$(HOME)/">https://www.debian.org/</a>, send mail to
&lt;press@debian.org&gt;, or contact the stable release team at
&lt;debian-release@lists.debian.org&gt;.</p>
