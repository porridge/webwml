#use wml::debian::template title="Debian 11 -- Erratas" BARETITLE=true
#use wml::debian::toc
#use wml::debian::translation-check translation="1b53acd1a1236a10ef76b706c36cf6495d8ebfcf"

#include "$(ENGLISHDIR)/releases/info"

<toc-display/>


# <toc-add-entry name="known_probs">Problemas conocidos</toc-add-entry>
<toc-add-entry name="security">Problemas de seguridad</toc-add-entry>

<p>El equipo de seguridad de Debian publica actualizaciones de paquetes de la versión «estable»
en los cuales ha identificado problemas relacionados con la seguridad. Consulte las
<a href="$(HOME)/security/">páginas de seguridad</a> para información sobre
cualquier problema de seguridad identificado en <q>bullseye</q>.</p>

<p>Si usa APT, agregue la siguiente línea en <tt>/etc/apt/sources.list</tt>
para tener acceso a las últimas actualizaciones de seguridad:</p>

<pre>
  deb http://security.debian.org/ bullseye-security main contrib non-free
</pre>

<p>Después, ejecute <kbd>apt update</kbd> seguido de
<kbd>apt upgrade</kbd>.</p>


<toc-add-entry name="pointrelease">Versiones</toc-add-entry>

<p>A veces, en el caso de varios problemas críticos o actualizaciones de seguridad, la
distribución publicada se actualiza. Generalmente, esto se indica con un nuevo número de versión («point
release» en inglés).</p>

<!-- <ul>
  <li>La primera versión, 11.1, se publicó el
      <a href="$(HOME)/News/2017/FIXME">FIXME</a>.</li>
</ul> -->

<ifeq <current_release_bullseye> 11.0 "

<p>Aún no hay versiones posteriores de Debian 11.</p>" "

<p>Vea el <a
href="http://http.us.debian.org/debian/dists/bullseye/ChangeLog">\
registro de cambios («ChangeLog»)</a>
para detalles sobre los cambios entre la versión 11 y la <current_release_bullseye/>.</p>"/>


<p>Las correcciones para la distribución «estable» publicada pasan un
período de pruebas extendido antes de ser aceptadas en el archivo.
Sin embargo, estas correcciones están disponibles en el directorio
<a href="http://ftp.debian.org/debian/dists/bullseye-proposed-updates/">\
dists/bullseye-proposed-updates</a> de cualquier réplica del archivo de
Debian.</p>

<p>Si usa APT para actualizar los paquetes, puede instalar
las actualizaciones propuestas agregando la siguiente línea en
<tt>/etc/apt/sources.list</tt>:</p>

<pre>
  \# adiciones propuestas para una versión de Debian 11
  deb http://deb.debian.org/debian bullseye-proposed-updates main contrib non-free
</pre>

<p>Después, ejecute <kbd>apt update</kbd> seguido de
<kbd>apt upgrade</kbd>.</p>


<toc-add-entry name="installer">Sistema de instalación</toc-add-entry>

<p>
Para información sobre erratas y actualizaciones del sistema de instalación, vea
la página de <a href="debian-installer/">información de instalación</a>.
</p>
