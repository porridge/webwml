#use wml::debian::translation-check translation="cc3aa11466129a6224ab33a305a554cb8d65f63c"
<define-tag pagetitle>Debian 10 actualizado: publicada la versión 10.6</define-tag>
<define-tag release_date>2020-09-26</define-tag>
#use wml::debian::news

<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.6</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>El proyecto Debian se complace en anunciar la sexta actualización de la
distribución estable Debian <release> (nombre en clave <q><codename></q>). 
Esta versión añade, principalmente, correcciones de problemas de seguridad
junto con unos pocos ajustes para problemas graves. Los avisos de seguridad
se han publicado de forma independiente y aquí hacemos referencia a ellos donde corresponde.</p>

<p>Tenga en cuenta que esta actualización no constituye una nueva versión de Debian
<release> sino que actualiza alguno de los paquetes que vienen incluidos.  No es necesario deshacerse de los viejos medios de instalación de <q><codename></q>. Tras la instalación, los paquetes instalados pueden pasarse a las nuevas versiones utilizando una réplica de Debian actualizada.</p>

<p>Quienes instalen actualizaciones desde security.debian.org con frecuencia no necesitarán actualizar muchos paquetes ya que la mayoría de dichas actualizaciones están incluidas en esta versión.</p>

<p>Pronto habrá disponibles nuevas imágenes de instalación en los sitios habituales.</p>

<p>Puede actualizar una instalación ya existente a esta versión conectando el sistema de gestión de paquetes a una de las múltiples réplicas HTTP de Debian.
En la siguiente dirección puede encontrar el listado completo de réplicas:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>

<h2>Corrección de fallos varios</h2>

<p>Esta versión «estable» añade correcciones importantes a los siguientes paquetes.</p>

<p>Tenga en cuenta que, debido a problemas de la compilación, las actualizaciones para los paquetes cargo, rustc y rustc-bindgen no están disponibles actualmente para la arquitectura <q>armel</q>.
Estas actualizaciones se añadirán en un futuro si se resuelven esos problemas.</p>

<table border=0>
<tr><th>Paquete</th>               <th>Motivo</th></tr>
<correction arch-test "Corrige fallos de detección ocasionales de S390x">
<correction asterisk "Corrige caída en la negociación de T.38 con una transmisión rechazada («declined stream») [CVE-2019-15297], <q>Una petición SIP puede cambiar la dirección de un par SIP</q> [CVE-2019-18790], <q>Un usuario de la AMI podría ejecutar órdenes del sistema</q> [CVE-2019-18610], violación de acceso en «pjsip show history» con pares IPv6">
<correction bacula "Corrige <q>cadena de datos sobredimensionada permite a un cliente malicioso causar un desbordamiento de la memora dinámica («heap») del director</q> [CVE-2020-11061]">
<correction base-files "Actualiza /etc/debian_version a la versión actual">
<correction calamares-settings-debian "Inhabilita el módulo displaymanager">
<correction cargo "Nueva versión del proyecto original para dar soporte a próximas versiones de Firefox ESR">
<correction chocolate-doom "Corrige la validación ausente [CVE-2020-14983]">
<correction chrony "Previene una carrera de enlace simbólico cuando se escribe en el archivo PID [CVE-2020-14367]; corrige la lectura de temperatura">
<correction debian-installer "Actualiza la ABI de Linux a la versión 4.19.0-11">
<correction debian-installer-netboot-images "Recompilado contra proposed-updates">
<correction diaspora-installer "Usa la opción --frozen en «bundle install» para que utilice el Gemfile.lock del proyecto original; no excluye Gemfile.lock en las actualizaciones; no sobrescribe config/oidc_key.pem en las actualizaciones; convierte config/schedule.yml en un archivo escribible">
<correction dojo "Corrige contaminación del prototipo en el método deepCopy [CVE-2020-5258] y en el método jqMix [CVE-2020-5259]">
<correction dovecot "Corrige regresión de sincronización de filtros sieve con dsync; corrige el manejo del resultado de getpwent en userdb-passwd">
<correction facter "Cambia el extremo de los metadatos de Google GCE de <q>v1beta1</q> a <q>v1</q>">
<correction gnome-maps "Corrige un problema con el renderizado de capas de forma desalineadas">
<correction gnome-shell "Diálogo de inicio de sesión: al cambiar de VT, inicializa los campos para introducir las credenciales antes de mostrar el diálogo [CVE-2020-17489]">
<correction gnome-weather "Previene caída cuando el conjunto de localizaciones configurado no es válido">
<correction grunt "Emplea carga segura («safeLoad») cuando se cargan archivos YAML [CVE-2020-7729]">
<correction gssdp "Nueva versión «estable» del proyecto original">
<correction gupnp "Nueva versión «estable» del proyecto original; previene el ataque de <q>CallStranger</q> [CVE-2020-12695]; necesita GSSDP 1.0.5">
<correction haproxy "logrotate.conf: emplea un script auxiliar de rsyslog en lugar del script de inicio SysV; rechaza mensajes en los que no está presente <q>chunked</q> en «Transfer-Encoding»[CVE-2019-18277]">
<correction icinga2 "Corrige el ataque de enlace simbólico [CVE-2020-14004]">
<correction incron "Corrige la limpieza de procesos zombies">
<correction inetutils "Corrige problema de ejecución de código remoto [CVE-2020-10188]">
<correction libcommons-compress-java "Corrige problema de denegación de servicio [CVE-2019-12402]">
<correction libdbi-perl "Corrige la corrupción de memoria en funciones XS cuando se reasigna una pila de Perl [CVE-2020-14392]; corrige el desbordamiento de memoria en un nombre de clase DBD muy largo [CVE-2020-14393]; corrige una desreferencia de perfil NULL en dbi_profile() [CVE-2019-20919]">
<correction libvncserver "libvncclient: cancela el intento de conexión si el nombre del socket UNIX es demasiado largo [CVE-2019-20839]; corrige problema del aliasing/alineamiento del puntero [CVE-2020-14399]; limita el tamaño del chat de texto [CVE-2020-14405]; libvncserver: añade comprobación de puntero nulo faltante [CVE-2020-14397]; corrige problema del aliasing/alineamiento del puntero [CVE-2020-14400]; scale: convierte a 64 bits antes de desplazar [CVE-2020-14401]; previene accesos OOB [CVE-2020-14402 CVE-2020-14403 CVE-2020-14404]">
<correction libx11 "Corrige desbordamiento de números enteros [CVE-2020-14344 CVE-2020-14363]">
<correction lighttpd "Adapta correcciones de usabilidad y seguridad para versiones anteriores">
<correction linux "Nueva versión «estable» del proyecto original; aumenta ABI a 11">
<correction linux-latest "Actualización para la ABI del núcleo -11">
<correction linux-signed-amd64 "Nueva versión «estable» del proyecto original">
<correction linux-signed-arm64 "Nueva versión «estable» del proyecto original">
<correction linux-signed-i386 "Nueva versión «estable» del proyecto original">
<correction llvm-toolchain-7 "Nueva versión del proyecto original para dar soporte a versiones próximas de Firefox ESR; corrige fallos que afectan al compilado rustc">
<correction lucene-solr "Corrige un problema de seguridad en el manejo de la configuración DataImportHandler [CVE-2019-0193]">
<correction milkytracker "Corrige el desbordamiento de memoria dinámica «heap» [CVE-2019-14464], desbordamiento de pila [CVE-2019-14496], desbordamiento de memoria dinámica «heap» [CVE-2019-14497] y «uso tras liberar» [CVE-2020-15569]">
<correction node-bl "Corrige vulnerabilidad de lectura fuera de límites [CVE-2020-8244]">
<correction node-elliptic "Prevención de maleabilidad y desbordamientos [CVE-2020-13822]">
<correction node-mysql "Añade la opción localInfile para controlar LOAD DATA LOCAL INFILE [CVE-2019-14939]">
<correction node-url-parse "Corrige la validación insuficiente y el saneado en la entrada de usuario [CVE-2020-8124]">
<correction npm "Oculta el password en los registros [CVE-2020-15095]">
<correction orocos-kdl "Elimina inclusión explícita de la ruta «include» predeterminada, corrigiendo problemas con cmake &lt; 3.16">
<correction postgresql-11 "Nueva versión «estable» del proyecto original; configura un search_path seguro en la replicación lógica de walsenders y apply workers [CVE-2020-14349]; hace más seguros los scripts de instalación de los módulos de contribuidores [CVE-2020-14350]">
<correction postgresql-common "No deja caer plpgsql antes de probar las extensiones">
<correction pyzmq "Asyncio: espera a POLLOUT en el emisor en can_connect">
<correction qt4-x11 "Corrige desbordamiento de memoria en el intérprete XBM [CVE-2020-17507]">
<correction qtbase-opensource-src "Corrige desbordamiento de memoria en el intérprete XBM [CVE-2020-17507]; corrige la ruptura del portapapeles cuando el temporizador completa su ciclo y vuelve al principio tras 50 días">
<correction ros-actionlib "Carga YAML de forma segura [CVE-2020-10289]">
<correction rustc "Nueva versión del proyecto original para dar soporte a versiones próximas de Firefox ESR">
<correction rust-cbindgen "Nueva versión del proyecto original para dar soporte a versiones próximas de Firefox ESR">
<correction ruby-ronn "Corrige el manejo de contenido UTF-8 en las páginas del manual">
<correction s390-tools "Definición interna de dependencias de perl en lugar de usar ${perl:Depends}, corrige la instalación bajo debootstrap">
</table>


<h2>Actualizaciones de seguridad</h2>


<p>Esta versión añade las siguientes actualizaciones de seguridad a la versión «estable».
El equipo de seguridad ya ha publicado un aviso para cada una de estas actualizaciones:</p>

<table border=0>
<tr><th>ID del aviso</th>  <th>Paquete</th></tr>
<dsa 2020 4662 openjdk-11>
<dsa 2020 4734 openjdk-11>
<dsa 2020 4736 firefox-esr>
<dsa 2020 4737 xrdp>
<dsa 2020 4738 ark>
<dsa 2020 4739 webkit2gtk>
<dsa 2020 4740 thunderbird>
<dsa 2020 4741 json-c>
<dsa 2020 4742 firejail>
<dsa 2020 4743 ruby-kramdown>
<dsa 2020 4744 roundcube>
<dsa 2020 4745 dovecot>
<dsa 2020 4746 net-snmp>
<dsa 2020 4747 icingaweb2>
<dsa 2020 4748 ghostscript>
<dsa 2020 4749 firefox-esr>
<dsa 2020 4750 nginx>
<dsa 2020 4751 squid>
<dsa 2020 4752 bind9>
<dsa 2020 4753 mupdf>
<dsa 2020 4754 thunderbird>
<dsa 2020 4755 openexr>
<dsa 2020 4756 lilypond>
<dsa 2020 4757 apache2>
<dsa 2020 4758 xorg-server>
<dsa 2020 4759 ark>
<dsa 2020 4760 qemu>
<dsa 2020 4761 zeromq3>
<dsa 2020 4762 lemonldap-ng>
<dsa 2020 4763 teeworlds>
<dsa 2020 4764 inspircd>
<dsa 2020 4765 modsecurity>
</table>



<h2>Instalador de Debian</h2>
<p>Se ha actualizado para incluir las correcciones incorporadas por
esta nueva versión «estable».</p>

<h2>URLs</h2>

<p>Las listas completas de paquetes que han cambiado en esta versión:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>La distribución «estable» actual:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Actualizaciones propuestas a la distribución «estable»:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>Información sobre la distribución «estable» (notas de publicación, erratas, etc.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Información y anuncios de seguridad:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Acerca de Debian</h2>

<p>El proyecto Debian es una asociación de desarrolladores de software libre
que aportan de forma voluntaria su tiempo y esfuerzo para producir el sistema operativo Debian, un sistema operativo completamente libre.</p>

<h2>Información de contacto</h2>

<p>Para más información, visite las páginas web de Debian en
<a href="$(HOME)/">https://www.debian.org/</a>, envíe un correo a
&lt;press@debian.org&gt;, o contacte con el equipo responsable de la publicación en
&lt;debian-release@lists.debian.org&gt;.</p>
