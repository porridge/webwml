<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>In phpMyAdmin 4 before 4.9.4 and 5 before 5.0.1, SQL injection exists in the
user accounts page. A malicious user could inject custom SQL in place of their
own username when creating queries to this page. An attacker must have a valid
MySQL account to access the server.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
4:4.2.12-2+deb8u8.</p>

<p>We recommend that you upgrade your phpmyadmin packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2060.data"
# $Id: $
