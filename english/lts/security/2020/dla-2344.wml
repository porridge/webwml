<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A denial of service vulnerability was discovered in mongodb, an
object/document-oriented database, whereby a user authorized to perform
database queries may issue specially crafted queries, which violate an
invariant in the query subsystem's support for geoNear.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1:3.2.11-2+deb9u2.</p>

<p>We recommend that you upgrade your mongodb packages.</p>

<p>For the detailed security status of mongodb please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/mongodb">https://security-tracker.debian.org/tracker/mongodb</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2344.data"
# $Id: $
