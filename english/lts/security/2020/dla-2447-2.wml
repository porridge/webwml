<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The update of pacemaker released as DLA-2447-1 caused a regression when the
communication between the Corosync cluster engine and pacemaker takes place. A
permission problem prevents IPC requests between cluster nodes. The patch for
<a href="https://security-tracker.debian.org/tracker/CVE-2020-25654">CVE-2020-25654</a> has been reverted until a better solution can be found.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1.1.16-1+deb9u2.</p>

<p>We recommend that you upgrade your pacemaker packages.</p>

<p>For the detailed security status of pacemaker please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/pacemaker">https://security-tracker.debian.org/tracker/pacemaker</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2447-2.data"
# $Id: $
