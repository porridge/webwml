<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The update of proftpd-dfsg released as DLA-2338-1 incorrectly
destroyed the memory pool in function sftp_kex_handle in
contrib/mod_sftp/kex.c which may cause a segmentation fault and thus
prevent sftp connections.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1.3.5e+r1.3.5b-4+deb9u2.</p>

<p>We recommend that you upgrade your proftpd-dfsg packages.</p>

<p>For the detailed security status of proftpd-dfsg please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/proftpd-dfsg">https://security-tracker.debian.org/tracker/proftpd-dfsg</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2338-2.data"
# $Id: $
