<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several security vulnerabilities have been discovered in puma, highly
concurrent HTTP server for Ruby/Rack applications.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11076">CVE-2020-11076</a>

    <p>By using an invalid transfer-encoding header, an attacker could smuggle
    an HTTP response.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11077">CVE-2020-11077</a>

    <p>client could smuggle a request through a proxy, causing the proxy to
    send a response back to another unknown client. If the proxy uses
    persistent connections and the client adds another request in via HTTP
    pipelining, the proxy may mistake it as the first request's body. Puma,
    however, would see it as two requests, and when processing the second
    request, send back a response that the proxy does not expect. If the
    proxy has reused the persistent connection to Puma to send another
    request for a different client, the second response from the first
    client will be sent to the second client.</p></li>

</ul>

<p>For Debian 9 stretch, this problem has been fixed in version
3.6.0-1+deb9u1.</p>

<p>We recommend that you upgrade your puma packages.</p>

<p>For the detailed security status of puma please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/puma">https://security-tracker.debian.org/tracker/puma</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2398.data"
# $Id: $
