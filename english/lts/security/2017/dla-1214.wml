<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Marcin Noga discovered two vulnerabilities in LibreOffice, which could
result in the execution of arbitrary code if a malformed PPT or DOC
document is opened.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1:3.5.4+dfsg2-0+deb7u10.</p>

<p>We recommend that you upgrade your libreoffice packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1214.data"
# $Id: $
