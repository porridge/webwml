<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Some memory corruption bugs were discovered in libraw, a raw image
decoder library, which could be triggered via maliciously crafted
input files to cause denial of service or other unspecified impact.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.14.6-2+deb7u2.</p>

<p>We recommend that you upgrade your libraw packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1057.data"
# $Id: $
