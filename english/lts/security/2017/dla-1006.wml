<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple denial of services vulnerabilities have been identified in 
libarchive when manipulating specially crafted archives.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10209">CVE-2016-10209</a>

    <p>NULL pointer dereference and application crash in the
    archive_wstring_append_from_mbs() function.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10349">CVE-2016-10349</a>

    <p>Heap-based buffer over-read and application crash in the
    archive_le32dec() function.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10350">CVE-2016-10350</a>

    <p>Heap-based buffer over-read and application crash in the
    archive_read_format_cab_read_header() function.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
3.0.4-3+wheezy6.</p>

<p>We recommend that you upgrade your libarchive packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1006.data"
# $Id: $
