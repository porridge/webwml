<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Dominik Penner discovered a flaw in how KConfig interpreted shell
commands in desktop files and other configuration files. An attacker may
trick users into installing specially crafted files which could then be
used to execute arbitrary code, e.g. a file manager trying to find out
the icon for a file or any application using KConfig. Thus the entire
feature of supporting shell commands in KConfig entries has been
removed.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
4:4.14.2-5+deb8u3.</p>

<p>We recommend that you upgrade your kde4libs packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1890.data"
# $Id: $
