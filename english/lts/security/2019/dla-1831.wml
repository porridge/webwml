<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>More Polymorphic Typing issues were discovered in jackson-databind. When
Default Typing is enabled (either globally or for a specific property)
for an externally exposed JSON endpoint and the service has JDOM 1.x or
2.x or logback-core jar in the classpath, an attacker can send a
specifically crafted JSON message that allows them to read arbitrary
local files on the server.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
2.4.2-2+deb8u7.</p>

<p>We recommend that you upgrade your jackson-databind packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1831.data"
# $Id: $
