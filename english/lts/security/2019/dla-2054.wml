<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Multiple buffer overflows have been fixed in jhead, a program to manipulate the non-image part of Exif compliant JPEG files.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16554">CVE-2018-16554</a></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-17088">CVE-2018-17088</a></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1010301">CVE-2019-1010301</a>

    <p>Overflows in gpsinfo</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1010302">CVE-2019-1010302</a>

    <p>Overflow in iptc</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1:2.97-1+deb8u2.</p>

<p>We recommend that you upgrade your jhead packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-2054.data"
# $Id: $
