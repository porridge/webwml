<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that multitouch devices were not being disabled
by the xtrlock screen locking utility.</p>

<p>xtrlock did not block multitouch events so an attacker could still
input and thus control various programs such as Chromium, etc. via
so-called "multitouch" events including pan scrolling, "pinch and
zoom" or even being able to provide regular mouse clicks by
depressing the touchpad once and then clicking with a secondary
finger.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10894">CVE-2016-10894</a>

    <p>xtrlock through 2.10 does not block multitouch events. Consequently, an
    attacker at a locked screen can send input to (and thus control) various
    programs such as Chromium via events such as pan scrolling, "pinch and zoom"
    gestures, or even regular mouse clicks (by depressing the touchpad once and
    then clicking with a different finger).</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
2.6+deb8u1. However, this fix does not the situation where an
attacker plugs in a multitouch device <em>after</em> the screen has been
locked (<a href="https://bugs.debian.org/830726#115">more info</a>).</p>

<p>We recommend that you upgrade your xtrlock packages pending a
deeper fix.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1959.data"
# $Id: $
