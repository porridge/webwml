<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>cURL, an URL transfer library, contains a heap buffer overflow in the
function tftp_receive_packet() that receives data from a TFTP server.
It calls recvfrom() with the default size for the buffer rather than
with the size that was used to allocate it. Thus, the content that
might overwrite the heap memory is entirely controlled by the server.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
7.38.0-4+deb8u15.</p>

<p>We recommend that you upgrade your curl packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1804.data"
# $Id: $
