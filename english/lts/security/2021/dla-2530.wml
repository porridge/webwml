<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Drupal identified a vulnerability in the verson of the Archive_Tar
library it bundles (<a href="https://security-tracker.debian.org/tracker/CVE-2020-36193">CVE-2020-36193</a>), which allows out-of-path
extraction vulnerabilities, granting it the Drupal Security Advisory
ID SA-CORE-2021-001:</p>

    <p>https://www.drupal.org/sa-core-2021-001</p>

<p>For Debian 9 <q>Stretch</q>, the fix to this issue was backported in
version 7.52-2+deb9u14.</p>

<p>We recommend you upgrade your drupal7 package.</p>

<p>For detailed security status of drupal7, please refer to its security
tracker page:</p>

    <p>https://security-tracker.debian.org/tracker/source-package/drupal7</p>

<p>Further information about Debian LTS security advisories, how to
apply these updates to your system, and other frequently asked
questions can be found at:</p>

    <p>https://wiki.debian.org/LTS</p>

<p>For Debian 6 <q>Squeeze</q>, these issues have been fixed in drupal7 version 7.52-2+deb9u14</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2530.data"
# $Id: $
