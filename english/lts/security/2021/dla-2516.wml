<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was an issue in the gssproxy privilege
separation caused by <em>gssproxy</em> not unlocking <pre>cond_mutex</pre>
prior to calling <pre>pthread_exit</pre>.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12658">CVE-2020-12658</a>

    <p>gssproxy (aka gss-proxy) before 0.8.3 does not unlock cond_mutex before
    pthread exit in gp_worker_main() in gp_workers.c.</p></li>

</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
0.5.1-2+deb9u1.</p>

<p>We recommend that you upgrade your gssproxy packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2516.data"
# $Id: $
