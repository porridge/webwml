<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that libcurl's implementation of the printf()
functions triggers a buffer overflow when doing a large floating point
output. The bug occurs when the conversion outputs more than 255 bytes.</p>

<p>The flaw happens because the floating point conversion is using system
functions without the correct boundary checks.</p>

<p>If there are any application that accepts a format string from the
outside without necessary input filtering, it could allow remote
attacks.</p>

<p>This flaw does not exist in the command line tool.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
7.26.0-1+wheezy18.</p>

<p>We recommend that you upgrade your curl packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-767.data"
# $Id: $
