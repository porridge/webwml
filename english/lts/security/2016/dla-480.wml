<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>This security update fixes serious security issues in NSS including
arbitrary code execution and remote denial service attacks.</p>

<p>For Debian 7 <q>wheezy</q>, these problems have been fixed in
3.14.5-1+deb7u6. We recommend you upgrade your nss packages as soon as
possible.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-7181">CVE-2015-7181</a>

    <p>The sec_asn1d_parse_leaf function improperly restricts access to
    an unspecified data structure.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-7182">CVE-2015-7182</a>

    <p>Heap-based buffer overflow in the ASN.1 decoder.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-1938">CVE-2016-1938</a>

    <p>The s_mp_div function in lib/freebl/mpi/mpi.c in improperly
    divides numbers, which might make it easier for remote attackers
    to defeat cryptographic protection mechanisms.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-1950">CVE-2016-1950</a>

    <p>Heap-based buffer overflow allows remote attackers to execute
    arbitrary code via crafted ASN.1 data in an X.509 certificate.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-1978">CVE-2016-1978</a>

    <p>Use-after-free vulnerability in the ssl3_HandleECDHServerKeyExchange
    function allows remote attackers to cause a denial of service or
    possibly have unspecified other impact by making an SSL (1) DHE or
    (2) ECDHE handshake at a time of high memory consumption.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-1979">CVE-2016-1979</a>

    <p>Use-after-free vulnerability in the PK11_ImportDERPrivateKeyInfoAndReturnKey
    function allows remote attackers to cause a denial of service or
    possibly have unspecified other impact via crafted key data with
    DER encoding.</p></li>

</ul>

<p>Further information about Debian LTS security Advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-480.data"
# $Id: $
