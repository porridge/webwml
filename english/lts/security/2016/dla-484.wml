<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several security vulnerabilities were discovered in graphicsmagick a
tool to manipulate image files.</p>

<p>GraphicsMagick is a fork of ImageMagick and also affected by
vulnerabilities collectively known as ImageTragick, that are the
consequence of lack of sanitization of untrusted input. An attacker
with control on the image input could, with the privileges of the user
running the application, 
execute code 
(<a href="https://security-tracker.debian.org/tracker/CVE-2016-3714">CVE-2016-3714</a>), 
make HTTP GET or FTP requests 
(<a href="https://security-tracker.debian.org/tracker/CVE-2016-3718">CVE-2016-3718</a>), 
or delete 
(<a href="https://security-tracker.debian.org/tracker/CVE-2016-3715">CVE-2016-3715</a>), 
move
(<a href="https://security-tracker.debian.org/tracker/CVE-2016-3716">CVE-2016-3716</a>), 
or read 
(<a href="https://security-tracker.debian.org/tracker/CVE-2016-3717">CVE-2016-3717</a>), 
local files.</p>

<p>To address these concerns the following changes have been made:</p>

<ol>

<li>Remove automatic detection/execution of MVG based on file header or
   file extension.</li>

<li>Remove the ability to cause an input file to be deleted based on a
   filename specification.</li>

<li>Improve the safety of delegates.mgk by removing gnuplot support,
   removing manual page support, and by adding -dSAFER to all
   ghostscript invocations.</li>

<li>Sanity check the MVG image primitive filename argument to assure
   that "magick:" prefix strings will not be interpreted.  Please note
   that this patch will break intentional uses of magick prefix
   strings in MVG and so some MVG scripts may fail. We will search
   for a more flexible solution.</li>

</ol>

<p>In addition the following issues have been fixed:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8808">CVE-2015-8808</a>

    <p>Assure that GIF decoder does not use unitialized data and cause an
    out-of-bound read.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2317">CVE-2016-2317</a> and
    <a href="https://security-tracker.debian.org/tracker/CVE-2016-2318">CVE-2016-2318</a>

    <p>Vulnerabilities that allow to read or write outside memory bounds
    (heap, stack) as well as some null-pointer derreferences to cause a
    denial of service when parsing SVG files.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.3.16-1.1+deb7u1.</p>

<p>We recommend that you upgrade your graphicsmagick packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-484.data"
# $Id: $
