#use wml::debian::template title="Dokumentacja przestarzała"
#include "$(ENGLISHDIR)/doc/manuals.defs"
#include "$(ENGLISHDIR)/doc/obsolete.defs"
#use wml::debian::translation-check translation="534d1b782cfb92f46dc41fd064f779fffc329b12"

<h1 id="historical">Dokumenty historyczne</h1>

<p>
  Dokumenty podane poniżej były albo napisane dawno temu i nie są już
  aktualne albo były napisane dla poprzednich wersji Debiana i nie
  zostały zaktualizowane do obecnej wersji. Informacje znajdujące się w
  nich są nieaktualne, jednakże być może nadal ktoś znajdzie w
  nich coś, czego szuka.
</p>

<h2 id="user">Dokumentacja dla użytkowników</h2>


<document "<code>dselect</code> Documentation for Beginners" "dselect">
<div class="centerblock">
<p>
  Dokument ten jest przeznaczony dla użytkowników, którzy pierwszy raz
  posługują się programem <code>dselect</code>, a jego celem jest
  pomoc w pomyślnym zainstalowaniu Debiana. Nie tłumaczy on
  wszystkiego, więc gdy po raz pierwszy zetkniesz się
  z <code>dselect</code>, korzystaj też z ekranów pomocy.
</p>
<doctable>
  <authors "Stéphane Bortzmeyer" />
  <maintainer "(?)" />
  <status>
    rozwój zatrzymany: <a href="https://packages.debian.org/aptitude"><code>aptitude</code></a>
    zastępuje <code>dselect</code> jako standardowy program do
    zarządzania pakietami w Debianie
  </status>
  <availability>
  <inddpvcs name="dselect-beginner" formats="html txt pdf ps"
            langs="ca cs da de en es fr hr it ja pl pt ru sk" />
  </availability>
</doctable>
</div>

<hr />

<div class="centerblock">
<p>
  Jest to po prostu przeredagowany <q>Progeny User's
  Guide</q>. Zawartość jest dostosowana do standardowego systemu
  Debian.
</p>
<p>
  Ponad 300 stron z dobrymi samouczkami pomagającymi rozpocząć używanie
  systemu Debian w trybie graficznym i z wiersza poleceń powłoki.
</p>
<doctable>
  <authors "Progeny Linux Systems, Inc." />
  <maintainer "Osamu Aoki (&#38738;&#26408; &#20462;)" />
  <status>
    użyteczny jako samouczek; napisany dla wydania woody, więc powoli
    staje się nieaktualny
  </status>
  <availability>
    # langs="en" isn't redundant, it adds the needed language suffix to the link
    <inddpvcs name="users-guide" index="users-guide" langs="en" formats="html txt pdf" />
  </availability>
</doctable>
</div>

<hr />

<document "Debian Tutorial" "tutorial" />
<div class="centerblock">
<p>
  Podręcznik przeznaczony dla nowych użytkowników Linuksa, pozwalający
  zapoznać się z zainstalowanym już systemem lub dla użytkowników
  komputera z Linuksem, którego administratorem jest ktoś inny.
</p>
<doctable>
  <authors "Havoc Pennington, Oliver Elphick, Ole Tetlie, James Treacy,
  Craig Sawyer, Ivan E. Moore II" />
  <editors "Havoc Pennington" />
  <maintainer "(?)" />
  <status>
    nie rozwija się; niekompletny; prawdopodobnie zastąpiony
    przez <a href="#quick-reference">Debian Reference</a>
  </status>
  <availability>
    jeszcze nie skończony
  <inddpvcs name="debian-tutorial" vcsname="tutorial" vcstype="attic">
  </availability>
</doctable>
</div>

<hr />

<document "Debian GNU/Linux: Guide to Installation and Usage" "guide" />
<div class="centerblock">
<p>
  Podręcznik dla użytkowników końcowych.
</p>
<doctable>
  <authors "John Goerzen, Ossama Othman" />
  <editors "John Goerzen" />
  <status>
    skończony (ale dla wersji potato)
  </status>
  <availability>
  <inoldpackage "debian-guide" />
  </availability>
</doctable>
</div>

<hr />

<document "Debian User Reference Manual" "userref" />
<div class="centerblock">
<p>
  Podręcznik ten daje użytkownikowi przynajmniej zarys tego
  wszystkiego, co powinien on wiedzieć o swoim systemie Debian
  GNU/Linux (np. ustawianie X-ów, jak skonfigurować sieć, dostęp do
  stacji dyskietek, itd.). Celem <q>Debian User Reference Manual</q>
  jest wypełnienie luki pomiędzy <q>Debian Tutorial</q>, a szczegółową
  dokumentacją dostarczaną wraz z pakietami.
</p>
<p>
  W założeniach jest to także wprowadzenie do zagadnienia, jak połączyć
  komendy według generalnej zasady Uniksa: <em>zawsze jest więcej niż jedna
  możliwość, aby to zrobić</em>.
</p>
<doctable>
  <authors "Ardo van Rangelrooij, Jason D. Waterman, Havoc Pennington,
      Oliver Elphick, Bruce Evry, Karl-Heinz Zimmer" />
  <editors "Thalia L. Hooker, Oliver Elphick" />
  <maintainer "(?)" />
  <status>
    rozwój zatrzymany i nieco niekompletny; prawdopodobnie zastąpiony
    przez <a href="#quick-reference">Debian Reference</a>
  </status>
  <availability>
  <inddpvcs name="user" />
  </availability>
</doctable>
</div>

<hr />


<document "Debian System Administrator's Manual" "system" />
<div class="centerblock">
<p>
  Wzmianka o tym dokumencie znajduje się w podręczniku Policy.
  Zawiera on wszystkie informacje na temat administracji systemem
  Debian.
</p>
<doctable>
  <authors "Tapio Lehtonen" />
  <maintainer "(?)" />
  <status>
    rozwój zatrzymany; nieskończony;
    prawdopodobnie zastąpiony przez <a href="#quick-reference">Debian Reference</a>
  </status>
  <availability>
    jeszcze niedostępny
  <inddpvcs name="system-administrator" />
  </availability>
</doctable>
</div>

<hr />

<document "Debian Network Administrator's Manual" "network" />
<div class="centerblock">
<p>
  Podręcznik ten porusza wszystkie aspekty sieciowego administrowania
  systemem Debian. 
</p>
<doctable>
  <authors "Ardo van Rangelrooij, Oliver Elphick, Duncan C. Thomson,
  Ivan E. Moore II" />
  <maintainer "(?)" />
  <status>
    rozwój zatrzymany; nieskończony; prawdopodobnie zastąpiony
    przez <a href="#quick-reference">Debian Reference</a>
  </status>
  <availability>
    jeszcze niedostępny
    <inddpvcs name="network-administrator" />
  </availability>
</doctable>
</div>

<hr />

# Add this to books, there's a revised edition (2nd) @ Amazon
<document "The Linux Cookbook" "linuxcookbook" />
<div class="centerblock">
<p>
  Praktyczny przewodnik po systemie Debian GNU/Linux. W ponad 1500
  <i>przepisach</i> książka pokazuje jak go używać w codziennych
  czynnościach &mdash; od pracy z tekstem, grafiką i dźwiękiem, po
  zagadnienia związane z wydajnością i pracą w sieci. Podobnie jak
  opisywane oprogramowanie, książka jest rozprowadzana na
  zasad <i>copyleft</i>.
</p>
<doctable>
  <authors "Michael Stutz" />
  <status>
    wydany; napisany dla woody - powoli się dezaktualizuje
  </status>
  <availability>
    <inoldpackage "linuxcookbook" /><br />
    <a href="http://dsl.org/cookbook/">na stronie autora</a>
  </availability>
</doctable>
</div>

<hr />

<document "APT HOWTO" "apt-howto">
<div class="centerblock">
<p>
Podręcznik ten jest szybkim, lecz kompletnym źródłem informacji na temat
systemu APT i jego zagadnień. Zawiera dużo informacji o zastosowaniu APT, a
także wiele przykładów.
</p>
<doctable>
  <authors "Gustavo Noronha Silva">
  <maintainer "Gustavo Noronha Silva">
  <status>gotowy</status>
  <availability>
    <inoldpackage "apt-howto">
    <inddpvcs name="apt-howto" langs="en ca cs de es el fr it ja pl pt-br ru uk tr zh-tw zh-cn"
      formats="html txt pdf ps" naming="locale" />
  </availability>
</doctable>
</div>

<hr/>

<document "Euro support in Debian GNU/Linux" "euro-support">

<div class="centerblock">
<p>
Dokument ten opisuje problemy związane z obsługą Euro w systemie operacyjnym
Debian GNU/Linux, i zawiera wskazówki jak w tym celu poprawnie skonfigurować
system i aplikacje.
</p>
<doctable>
  <authors "Javier Fernandez-Sanguino Pena">
  <status>gotowy</status>
  <availability>
    <inpackage "euro-support">
    <inddpvcs name="debian-euro-support" langs="en fr it" cvsname="euro-support">
  </availability>
</doctable>
</div>


<h2 id="devel">Dokumentacja dla rozwijających</h2>

<document "Dpkg Internals Manual" "dpkgint">

<div class="centerblock">
<p>
  Dokumentacja techniczna nt. budowy wewnętrznej <code>dpkg</code>
  oraz o tym, jak pisać nowe narzędzia dla systemu zarządzania
  pakietami używając bibliotek <code>dpkg</code>. Dokument ten
  jest użyteczny tylko dla ograniczonego kręgu czytelników.
</p>
<doctable>
  <authors "Klee Dienes" />
  <status>
    rozwój zatrzymany
  </status>
  <availability>
    <inoldpackage "dpkg-doc" />
    <a href="packaging-manuals/dpkg-internals/">HTML online</a>
  </availability>
</doctable>
</div>

<hr />

<document "Introduction: Making a Debian Package" "makeadeb">

<div class="centerblock">
<p>
  Wprowadzenie do tego, jak tworzyć pakiety DEB, przy
  użyciu <strong>debmake</strong>.
</p>
<doctable>
  <authors "Jaldhar H. Vyas" />
  <status>
    rozwój zatrzymany; zastąpiony
    przez <a href="devel-manuals#maint-guide">New Maintainers'
    Guide</a>
  </status>
  <availability>
  <a href="https://people.debian.org/~jaldhar/">HTML online</a>
  </availability>
</doctable>
</div>

<hr />

<document "Debian Programmers' Manual" "programmers">

<div class="centerblock">
<p>
  Pomaga nowym deweloperom tworzyć pakiety dla dystrybucji Debian
  GNU/Linux.
</p>
<doctable>
  <authors "Igor Grobman" />
  <status>
    zastąpiony przez <a href="devel-manuals#maint-guide">New
    Maintainers' Guide</a>
  </status>
  <availability>
  <inddpvcs name="programmer" />
  </availability>
</doctable>
</div>

<hr />

<document "Debian Packaging Manual" "packman">

<div class="centerblock">
<p>
Ten podręcznik opisuje techniczne aspekty tworzenia
pakietów binarnych i źródłowych dla Debiana.
Dodatkowo opisuje interfejs pomiędzy dselect i jego 
skryptami. Nie jest zgodny z wytycznymi Projektu Debiana 
i zakłada znajomość z funkcjami administracyjnymi programu dpkg.

<doctable>
  <authors "Ian Jackson, Klee Dienes, David A. Morris, Christian Schwarz">
  <status>
  Fragmenty, które były zgodne z wymaganiami zostały dołączone do
  <a href="devel-manuals#policy">debian-policy</a>.
  </status>
  <availability>
  <inoldpackage "packaging-manual">
  </availability>
</doctable>
</div>

<hr>

<document "Wprowadzenie do i18n" "i18n">

<div class="centerblock">
<p>
  Dokument ten opisuje podstawowe koncepcje dotyczące l10n
  (lokalizacji), i18n (internacjonalizacji) oraz m17n (wielojęzyczności) i
  jest skierowany do programistów oraz opiekunów pakietów.

  <p>Założeniem tego dokumentu jest to, aby więcej pakietów obsługiwało i18n
  oraz aby Debian był bardziej zinternacjonalizowaną dystrybucją.
  Wszelkie wsparcie jest mile widziane, a ze względu na to, iż językiem
  ojczystym autora jest język japoński to w razie braku wsparcia
  dokument będzie dotyczył wyłącznie wsparcia języka japońskiego.
 
<doctable>
  <authors "Tomohiro KUBOTA (&#20037;&#20445;&#30000;&#26234;&#24195;)">
  <maintainer "Tomohiro KUBOTA (&#20037;&#20445;&#30000;&#26234;&#24195;)">
  <status>
  w trakcie rozwoju
  </status>
  <availability>
  jeszcze nie skończone
  <inddpvcs-intro-i18n>
  </availability>
</doctable>
</div>

<hr>

<document "Jak producenci oprogramowania mogą dystrybuować własne oprogramowanie
bezpośrednio w formacie .deb" "swprod">

<div class="centerblock">
<p>
  Dokument ten jest punktem wyjściowym dla producentów oprogramowania
  wyjaśniającym w jaki
  sposób mogą integrować własne produkty z Debianem oraz uwidaczniającym
  różne sytuacje, jakie mogą powstać wokół licencji produktów i decyzji
  producentów oraz istniejących możliwości wynikających z tego.
  Dokument ten nie wyjaśnia sposobu tworzenia pakietów, ale odsyła do
  dokumentów dotyczących tego.

  <p>Należy przeczytać ten dokument jeśli nie zna się ogólnej koncepcji
  tworzenia i dystrybuowania pakietów Debiana i opcjonalnie dołączenia
  ich do dystrybucji Debiana.

<doctable>
  <authors "Aaron Isotton">
  <maintainer "Aaron Isotton">
  <status>
  skończony (?)
  </status>
  <availability>
  <inddpvcs-distribute-deb>
  </availability>
</doctable>
</div>

<hr>

<document "Debian SGML/XML HOWTO" "sgml-howto">

<div class="centerblock">
<p>
  This HOWTO contains practical information about the use of SGML and XML
  on a Debian operating system.

<doctable>
  <authors "Stephane Bortzmeyer">
  <maintainer "Stephane Bortzmeyer">
  <status>
  stalled, obsolete
  </status>
  <availability>

# English only using index.html, so langs set.
  <inddpvcs name="sgml-howto"
            formats="html"
            srctype="SGML"
            vcstype="attic"
/>
  </availability>
</doctable>
</div>

<hr>

<document "Polityka Debiana dotycząca XML/SGML" "xml-sgml-policy">

<div class="centerblock">
<p>
  Wymogi, które muszą spełnić pakiety Debiana wspierające lub wykorzystujące
  zasoby XML lub SGML.

<doctable>
  <authors "Mark Johnson, Ardo van Rangelrooij, Adam Di Carlo">
  <status>
  rozpoczęcie, migracja z <tt>sgml-base-doc</tt> oraz nowych materiałów
  dotyczących zarządzania katalogiem XML do obecnej polityki dotyczącej
  SGML
  </status>
  <availability>
  <inddpvcs-xml-sgml-policy>
  </availability>
</doctable>
</div>

<hr>

<document "DebianDoc-SGML Markup Manual" "markup">

<div class="centerblock">
<p>
  Dokumentacja systemu <strong>debiandoc-sgml</strong>, włączając
  w to wskazówki i opisy najlepszych praktyk dla opiekunów. Przyszłe
  wersje powinny zawierać wskazówki jak łatwiej zarządzać i budować dokumentację
  w pakietach Debiana, jak organizować tłumaczenia dokumentów oraz wiele
  innych, przydatnych informacji.
  Patrz też <a href="https://bugs.debian.org/43718">błąd #43718</a>.

<doctable>
  <authors "Ian Jackson, Ardo van Rangelrooij">
  <maintainer "Ardo van Rangelrooij">
  <status>
  gotowe
  </status>
  <availability>
  <inpackage "debiandoc-sgml-doc">
  <inddpvcs-debiandoc-sgml-doc>
  </availability>
</doctable>
</div>

<hr>

<h2 id="misc">Rozmaita dokumentacja</h2>

<document "Debian META Manual" "meta" />
<div class="centerblock">
<p>
  Ten podręcznik pokaże, gdzie znaleźć dokument, który zawiera szukane
  przez nas informacje.
</p>
<doctable>
  <authors "Ardo van Rangelrooij, Oliver Elphick" />
  <maintainer "(?)" />
  <status>
    planowany
  </status>
  <availability>
    jeszcze nie gotowy; wygląda na to, że brakuje opiekuna
    <inddpvcs name="meta" />
  </availability>
</doctable>
</div>

<hr />

<document "Debian Book Suggestions" "books">

<div class="centerblock">
<p>
  Lista książek godnych uwagi dla użytkowników i opiekunów pakietów
  Debiana.
</p>
<doctable>
  <authors "Ardo van Rangelrooij, Oliver Elphick, Tapio Lehtonen" />
  <maintainer "?" />
  <status>
    planowany
  </status>
  <availability>
    jeszcze nie gotowy
  <inddpvcs name="book-suggestions" />
  </availability>
</doctable>
</div>

<hr />

<document "Debian Dictionary" "dict">

<div class="centerblock">
<p>
  <q>Myślałem przez długi czas, że potrzebujemy słownika
  Debiana. Projekt z pewnością ma swój specyficzny
  żargon. Prawdopodobnie możemy postawić gdzieś stronę WWW, żeby
  ludzie mogli podawać nowe terminy i ich definicje gdy tylko się z
  nimi spotkają.</q>
</p>
<doctable>
  <authors "Ardo van Rangelrooij, Oliver Elphick" />
  <maintainer "?" /> 
  <status>
    planowany
  </status>
  <availability>
    nie gotowy
    <inddpvcs name="dictionary" />
    <p>
      Pakiet <a href="https://packages.debian.org/dict-vera">dict-vera</a>
      oraz <a href="https://packages.debian.org/dict-foldoc">dict-foldoc</a>
      zawierają definicje wielu wyrażeń i skrótów związanych z Debianem.
    </p>
  </availability>
</doctable>
</div>

<hr />

<dl>
  <dt><strong><a href="../releases/potato/installguide/">Potato installation HOWTO</a></strong></dt>
  <dd>
    Nieoficjalna instrukcja instalacji dla Debiana 2.2 (nazwa kodowa: Potato)
  </dd>
</dl>

<hr />

<document "Debian Repository HOWTO" "repo">

<div class="centerblock">
<p>
 Ten dokument opisuje jak działają repozytoria Debiana, jak je tworzyć
 i jak poprawnie dodawać je do pliku <tt>sources.list</tt>.
</p>
<doctable>
  <authors "Aaron Isotton">
  <maintainer "Aaron Isotton">
  <status>gotowy (?)</status>
  <availability>
    <inddpvcs name="repository-howto" index="repository-howto"
    formats="html" langs="en fr de uk ta">
  </availability>
</doctable>
</div>

# Translated by Karol Ossowski
