#use wml::debian::template title="A filozófiánk: hogyan csináljuk és miért csináljuk"
#include "$(ENGLISHDIR)/releases/info"

# translators: some text is taken from /intro/about.wml

#use wml::debian::translation-check translation="8d625100deb403dba223e3cecbac44a283fe02ff" maintainer="Szabolcs Siebenhofer"

<ul class="toc">
<li><a href="#what">MI a Debian?</a>
<li><a href="#free">Teljesen ingyenes?</a>
<li><a href="#how">Hogy tud a közösség úgy működni, mint egy projekt?</a>
<li><a href="#history">Mindez hogyan kezdődött?</a>
</ul>

<h2><a name="what">Mi a Debian?</a></h2>

<p>A <a href="$(HOME)/">Debian Project</a> egyének egyesülete, akik közös egy ügyet 
hoztak létre a <a href="free">szabad</a> operációs rendszer megalkotására.
Az operációs rendszert, amit létrehoztunk, <strong>Debian</strong>-nak hívják.

<p>Az operációs rendszer alavető porgramok és segédprogramok gyűjteménye, amik
használható teszik a számítógépedet.
Az operációs rendszer magja a kernel.
A kernel a legalapvetőbb program a számítógépeden, alapvető háztartási munkákat
végez és lehetővé teszi más programok elindítását.

<p>A Debian jelenleg a <a href="https://www.kernel.org/">Linux</a> kernelt vagy a
<a href="https://www.freebsd.org/">FreeBSD</a> kernelt használja. A Linux egy
szoftver, amit <a href="https://en.wikipedia.org/wiki/Linus_Torvalds">
Linus Torvalds</a> kezdett el és programozók ezrei támogatják szerte a világon.
A FreeBSD egy operációs rendszer, ami tartalmazza a kernelt és más szoftvereket.

<p>Azonban, dolgozunk azon, hogy más kernelekkel is kínáljuk a Debiant, 
elsősorban a <a href="https://www.gnu.org/software/hurd/hurd.html">Hurd</a>-el. A Hurd 
szerverek gyűjteménye, amik a mikrokernelen futnak (mint a Mach), hogy megvalósítsanak 
különböző funkciókat. A Hurd szabad szoftver, amit a <a href="https://www.gnu.org/">
GNU projekt</a> hozott létre.

<p>Az alapvető eszközök nagy része, ami kitölti az operációs rendszert, a 
<a href="https://www.gnu.org/">GNU projekttől</a> származik; ennél fogva a neveik:
GNU/Linux, GNU/kFreeBSD, és GNU/Hurd.
Ezek az eszközök mind szabad szoftverek.

<p>Természetesen, felhasználói programokat szeretnének:
programok, amik segítenek nekik abban, hogy megtegyék, amit meg kell tenniük,
dokumentumok szerkesztésétől kezdve, az üzlet működtetésén és játékokon át,
egészen további szoftverek megírásáig. A Debian több, mint <packages_in_stable>
<a href="$(DISTRIB)/packages">csomaggal</a> (előre lefordított program, tetszetős
köntösbe csomagolva, hogy könnyen telepíthesd a számítógépeden), csomagkezelővel (APT) és
más segédprogramokkal érkezik, amik lehetővé teszik, hogy több ezer csomagot kezelhessünk
több ezer számítógépen, olyan könnyen, mint ahogy egy alklamazást telepítünk. És ez 
mind <a href="free">szabad</a>.
</p>

<p>Ez kicsit olyan, mint egy torony. A kernel az alap.
Azon vannak azok a szoftverek, amik működtetik a számítógéped.
A torony csúcsán pedig a Debian &mdash gondosan szervezve és összehangolva,
szóval ez az egész együtt működik.

<h2>És ez mind <a href="free" name="free">szabad?</a></h2>

<p>Ha a "szabad" szót használjuk, akkor a szoftverek <strong>szabadságára</strong>
utalunk. Még többet olvashatsz arról, <a href="free">mire gondolunk a "szabad 
szoftverrel"</a> és <a href="https://www.gnu.org/philosophy/free-sw">mit mond a 
Free Software Alapítvány</a> erről a témáról.

<p>Talán csodálkozol: miért töltenek emeberek órákat a saját idejükből azzal, hogy programokat
írjanak, gondosan csomagolják őket és aztán <EM>tovább adják</EM> az egészet?
A válaszok annyira sokfélék, mint az emberek, akik közreműködnek. Több és több ember keresi
a módját, hogy ne kelljen kifizetnie a szoftverek felfújt árát. Sokan programokat írnak,
hogy többet tudjanak a számítógépekről. Közreműködök egyre növekvő tömege köszönetként teszi az
összes nagyszerű szabad szoftverért, amit másoktól kapott. Sokan az egyetemeken készítenek 
szabad szoftvereket, hogy a kutatásukban széleskörű segítséget kaphassanak. Vállalkozások
segítenek karbantartani szabad szoftvereket, így beleszólhatnak a fejlődésükbe. --
nincs annál gyorsabb módja egy új funkció megalkotásának, minthogy megcsinálod magad!
Természetesen, sokunk nagyon szórakoztatónak tartja.

<p>A Debian nagyon elkötelezett a szabad szoftver iránt és azt gondoltuk, hasznos lenne, ha ez
az elkötelezettség irásos dokumentumba lenne foglalva. Ígyhát, a 
<a href="$(HOME)/social_contract">Társadalmi Szerződésünk</a> megszületett.

<p>Habár a Debian hisz a szabad szoftverben, vannak helyzetek, amikor az emberek szerenének vagy
muszáj, hogy nem-szabad szoftvert tegyenek a számítógépükre. Amikor csak lehetséges,
a Debian támogatja ezt. Egyre több olyan csomag van, melynek egyetlen feladata nem-szabad
szoftverek telepítése a Debian rendszerbe.

<h2><a name="how">Hogy tud a közösség úgy működni, mint egy projekt?</a></h2>

<p>A Debian <a href="$(DEVEL)/developers.loc">szerte a világon</a> több ezer, a saját idejét 
feláldozó önkéntes fejlesztő közreműködésével jött létre. Néhányan a fejlesztők közül már 
találkoztak személyesen. A kommunikáció elsődlegesen e-mail-en keresztül 
(a levelező listák megtalálhatóak a lists.debian.org oldalon) és IRC-n (#debian csatorna
az irc.debian.org-on) zajlik</p>

<p>A Debian Projekt gondosan <a href="organization">szervezett struktúrával</a> rendelkezik.
A Debian belső felépítésével kapcsolatos további inforációkért ne habozz felkeresni 
a <a href="$(DEVEL)/">fejlesztők sarkát</a>.

</p>

<p>
A közösség működését bemutató legfőbb dokumentumok a következők:
<ul>
<li><a href="$(DEVEL)/constitution">A Debian Alkotmány</a></li>
<li><a href="../social_contract">A Társadalmi szerződés és a Szabad Szoftver irányelvek</a></li>
<li><a href="diversity">A Sokszínűségi Nyilatkozat</a></li>
<li><a href="../code_of_conduct">Az Etikai Kódex</a></li>
<li><a href="../doc/developers-reference/">A Fejlesztők Referenciája</a></li>
<li><a href="../doc/debian-policy/">A Debian Politika</a></li>
</ul>

<h2><a name="history">Mindez hogyan kezdődött?</a></h2>

<p>A Debian-t 1993-ban Ian Murdock indította, mint egy olyan új disztribúció, ami szabadon 
készül, a Linux és a GNU szellemében. A Debian-t gondosan és lelkiismeretesen szerették 
volna összerakni és hasonló módon karbantartni és támogatni. Szabad szoftver fejlesztők
 egy kicsi és jól szerevett csoportjaként indult és folyamatosan nőve fejlődött
 a fejlesztők és felhasználók jól szervezett közösségévé. Itt megtalálod  
<a href="$(DOC)/manuals/project-history/">a részletes történetet.</a>

<p>Mivel sokan kérdezték, a Debian így kell kiejteni: /&#712;de.bi.&#601;n/. A Debian 
létrehozójának, Ian Murdock-nak és feleségének, Debra-nak a nevéből jött létre.
